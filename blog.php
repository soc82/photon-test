<?php
/*
  Template Name: Blog Page
 */

get_header();
get_template_part("templates/breadcrumb");
?> 
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8 col-sm-8">
                    <div class="content-bar">
                        <?php
                        $tf_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_type' => array('post'),
                            'paged' => $tf_paged,
                            'post_status' => array('publish')
                        );

// The Query
                        $query = new WP_Query($args);

// The Loop
                        if ($query->have_posts()) {
                            while ($query->have_posts()) {
                                $query->the_post();
                                /**
                                 * Include the Post Template
                                 */
                                get_template_part('content');
                                // do something
                            }
                        }


// Restore original Post Data
                        wp_reset_postdata();
                        ?>
                        <div class="clear"></div>
                        <?php traffica_pagination($query->max_num_pages); ?> 
                        <div class="clear"></div>
                    </div>
                </div>    
                <div class="col-md-4 col-sm-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>