<?php
/**
 * Defining Relative and Absolute Path of Theme's root directory by Constant
 * Relative Path - TRAFFICA_DIR
 * Absolute Path - TRAFFICA_DIR_URI
 */
if (!defined('TRAFFICA_DIR')) {
    define('TRAFFICA_DIR', get_template_directory() . '/');
}
if (!defined('TRAFFICA_DIR_URI')) {
    define('TRAFFICA_DIR_URI', get_template_directory_uri() . '/');
}

include_once TRAFFICA_DIR . 'functions/inkthemes-functions.php';

/**
 * Include Inkoptions FrameWork
 */
//require_once (TRAFFICA_DIR . 'includes/inkoption-framework/load.php');


/* These files build out the options interface.  Likely won't need to edit these. */
require_once (TRAFFICA_DIR . 'includes/shortcodes.php');
require_once (TRAFFICA_DIR . 'includes/dynamic-image.php');
require_once (TRAFFICA_DIR . '/includes/customizer.php');


/**
 * Include preview demo
 */
require get_parent_theme_file_path('includes/preview/demo-preview.php');

/**
 * Include preview demo
 */
require_once TRAFFICA_DIR . 'includes/features/feature-about-page.php';

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if (!isset($content_width)) {
    $content_width = 700;
}
/**
 * traffica functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package traffica
 */
if (!function_exists('traffica_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function traffica_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on traffica, use a find and replace
         * to change 'traffica' to the name of your theme in all the template files.
         */
        load_theme_textdomain('traffica', TRAFFICA_DIR . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
            
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');
         /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */

        add_theme_support('post-thumbnails');
		/*
        * WooCommcerce Support
        */
        add_theme_support( 'woocommerce' );
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );

		// This theme uses wp_nav_menu() in one location.
        register_nav_menu('custom_menu', __('Main Menu', 'traffica'));
        
         // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        
              // Define and register starter content to showcase the theme on new sites.
//        $starter_content = apply_filters('hustler_starter_content', $starter_content);

        add_theme_support('starter-content', array(
            // Set up nav menus for the theme.
		'nav_menus' => array(
			// Assign a menu to the "menu-1" location.
			'custom_menu' => array(
				'name' => __( 'Top Menu', 'traffica' ),
				'items' => array(
                    'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
        
					
				),
			),
                    ),
            
            'widgets' => array(
                // Place three core-defined widgets in the sidebar area.
                'primary-widget-area' => array(
                    'search',
                    'categories',
                    'recent-posts',
                ),
                'footer-widget-area1' => array(
                    'my_html1' => array(
                        // Widget $id -> set when creating a Widget Class
                        'custom_html',
                        // Widget $instance -> settings 
                        array(
                            'title' => __( 'Contact', 'traffica' ),
                            'content' => '<div class="full"><img src="' . esc_url(get_template_directory_uri()) . '/assets/images/traffica_foot_logo.png" alt="Image" /></div>',
             
                        ),
                    ),
                    'my_text' => array(
                        // Widget $id -> set when creating a Widget Class
                        'text',
                        // Widget $instance -> settings 
                        array(
                            'title' => '',
                            'text' => 'Footer is widgetized. To setup the footer, drag the required Widgets in Appearance -> Widgets Tab in the First, Second, Third, Fourth and Five Footer Widget Areas.',
             
                        ),
                    ),
                    'my_html2' => array(
                        // Widget $id -> set when creating a Widget Class
                        'custom_html',
                        // Widget $instance -> settings 
                        array(
                            'title' => '',
                            'content' => '<div class="details_icon_add"><i class="fa fa-phone fa-1x"></i> +1-415-479-4125 or +1-224-280-4110<br/><i class="fa fa-paper-plane-o"></i> 3529 Harley Brook Lane Saxton, PA 16678<br /><i class="fa fa-envelope"></i> MichaelGKnight@teleworm.us</div>',
             
                        ),
                    ),
                    
                     
                ),
                // Add the core-defined business info widget to the footer 1 area.
                'footer-widget-area2' => array(
                    'my_html3' => array(
                        // Widget $id -> set when creating a Widget Class
                        'custom_html',
                        // Widget $instance -> settings 
                        array(
                            'title' => 'Our Pages',
                            'content' => join( '', array(
                                "<ul>"
                                .'<li><a href="#">'.__('Default template', 'traffica').'</a></li>'
                                .'<li><a href="#">'.__('Full-width template', 'traffica').'</a></li>'
                                .'<li><a href="#">'.__('Home template', 'traffica').'</a></li>'
                                .'<li><a href="#">'.__('Custom Logo Fevicon', 'traffica').'</a></li>'
                                ."</ul>"

                            ) ),
      
                        
             
                        ),
                    ),
                ),
                // Put two core-defined widgets in the footer 2 area.
                'footer-widget-area3' => array(
                    'search' => array( 'search', array(
                        'title' => __( 'Search', 'traffica' ),
                    ) ),
                    'my_html4' => array(
                        // Widget $id -> set when creating a Widget Class
                        'custom_html',
                        // Widget $instance -> settings 
                        array(
                            'title' => '',
                            'content' => '<p>'.__("I decided to try this out on my Diamondback Bikes Reviews site and even if it ain't pretty it's so fricken.", "traffica"). '</p>',
             
                        ),
                    ),
                ),

                'footer-widget-area4' => array(
                    'my_html4' => array(
                        // Widget $id -> set when creating a Widget Class
                        'custom_html',
                        // Widget $instance -> settings 
                        array(
                            'title' => __( 'Inkthemes Theme', 'traffica' ),
                            'content' => '<p>'.__('Premium WordPress Themes with Single Click Installation, Just a Click and your website is ready for use. Your Site is faster to built, easy to use.', 'traffica'). '</p>',
             
                        ),
                    ),
                ),
               
            ),
                )
        );
    }

endif;
add_action('after_setup_theme', 'traffica_setup');


function traffica_panels_js() {

           wp_enqueue_style('fontawesome', TRAFFICA_DIR_URI . "assets/font-Awesome/css/font-awesome.css", '', '', 'all');
}
add_action( 'customize_controls_enqueue_scripts', 'traffica_panels_js' );


function traffica_styles() {

    /**
     * Bootstrap Stylesheet
     */
    wp_enqueue_style('traffica-bootstrap', TRAFFICA_DIR_URI . 'assets/css/bootstrap.css');

    /**
     * Stylesheet to reset the previous styles.
     */
    wp_enqueue_style('traffica-reset', TRAFFICA_DIR_URI . 'assets/css/reset.css');

    /**
     * Font-Awesome Library 
     */
    wp_enqueue_style('traffica-font-awesome', TRAFFICA_DIR_URI . 'assets/font-Awesome/css/font-awesome.css');

    /**
     * Fonts used in the theme
     */
    wp_enqueue_style('traffica-open-sans-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800');

    /**
     * Main Stylesheet - Style.css
     */
    wp_enqueue_style('traffica-style', get_stylesheet_uri());

    /**
     * Shortcode Stylesheet
     */
    wp_enqueue_style('traffica-shortcode', TRAFFICA_DIR_URI . 'assets/css/shortcode.css');

    /**
     * Stylesheet of Pretty Photo
     */
    wp_enqueue_style('traffica-prettyphoto', TRAFFICA_DIR_URI . 'assets/css/prettyPhoto.css');

    /**
     * Animate.css
     */
    wp_enqueue_style('traffica-animate', TRAFFICA_DIR_URI . 'assets/css/animate.css');

    /**
     * Stylesheet of Mean Menu(Mobile Menu) 
     */
    wp_enqueue_style('traffica-mean-menu', TRAFFICA_DIR_URI . 'assets/css/mean-menu.css');

    /**
     *   Color template stylesheet
     */
//    if (!is_admin()) {
//        if (traffica_get_option('traffica_altstylesheet') != 'orange') {
//            wp_enqueue_style('traffica-coloroptions', TRAFFICA_DIR_URI . "assets/css/color/" . traffica_get_option('traffica_altstylesheet') . ".css", '', '', 'all');
//        }
//        if (traffica_get_option('traffica_lanstylesheet') != 'Default') {
//            wp_enqueue_style('traffica_lanstylesheet', TRAFFICA_DIR_URI . "assets/css/color/" . traffica_get_option('traffica_lanstylesheet') . ".css", '', '', 'all');
//        }
//        wp_enqueue_style('traffica-shortcodes', TRAFFICA_DIR_URI . "assets/css/shortcode.css", '', '', 'all');
//    }

    /**
     * Responsive Stylesheet
     */
    wp_enqueue_style('traffica-responsive', TRAFFICA_DIR_URI . 'assets/css/responsive.css');

    /**
     * woocommerce
     *  Stylesheet
     */
        wp_enqueue_style('traffica-woocommerce', TRAFFICA_DIR_URI . 'assets/css/woocommerce.css');
}

/**
 * Enqueuing/adding all the stylesheets in the theme
 */
add_action('wp_enqueue_scripts', 'traffica_styles');

/**
 * jQuery Enqueue
 */
function traffica_wp_enqueue_scripts() {
    if (!is_admin()) {

        /**
         * DDSmooth Menu (Navigation Menu) JS File
         * Dependency : jQuery
         */
        wp_enqueue_script('traffica-ddsmoothmenu', TRAFFICA_DIR_URI . 'assets/js/ddsmoothmenu.js', array('jquery'));


        /**
         * Bootstrap JS File
         */
        wp_enqueue_script('traffica-bootstrap', TRAFFICA_DIR_URI . 'assets/js/bootstrap.js', array('jquery'));

        /**
         * Flex Slider JS File
         * Dependency : jQuery 
         */
        wp_enqueue_script('traffica-flexslider', TRAFFICA_DIR_URI . 'assets/js/jquery.flexslider-min.js', array('jquery'));

        /**
         * Layered Slider
         */
        wp_enqueue_script('traffica-carau', TRAFFICA_DIR_URI . 'assets/js/jquery.carouFredSel-6.2.1.js', array('jquery'));

        /**
         * Pretty Photo JS File
         * Dependency : jQuery
         */
        wp_enqueue_script('traffica-jquery.prettyPhoto', TRAFFICA_DIR_URI . 'assets/js/jquery.prettyPhoto.js', array('jquery'));

        /**
         * Mean Menu (Mobile Menu) JS File
         * Dependency : jQuery
         */
//		wp_enqueue_script( 'traffica-responsive-menu-2', TRAFFICA_DIR_URI . 'assets/js/jquery.meanmenu.js', array( 'jquery' ) );

        wp_enqueue_script('traffica-carousal', TRAFFICA_DIR_URI . 'assets/js/jcarousellite_1.0.1.min.js', array('jquery'));
        wp_enqueue_script('traffica-carousal-2', TRAFFICA_DIR_URI . '/assets/js/jquery.jcarousel.min.js', array('jquery'));
        wp_enqueue_script('traffica-accordian', TRAFFICA_DIR_URI . 'assets/js/jquery.akordeon.js', array('jquery'));
    
        /**
         * Custom JS File 
         * Dependency : jQuery
         */
        wp_enqueue_script('traffica-custom', TRAFFICA_DIR_URI . '/assets/js/custom.js', array('jquery'), false, true);

        /**
         * jQuery Validate JS File 
         * Dependency : jQuery
         */
        wp_enqueue_script('traffica-validate', TRAFFICA_DIR_URI . 'assets/js/jquery.validate.min.js', array('jquery'));

        if (is_singular() and get_site_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }
    /**
     * Mean Menu (Mobile Menu) JS File
     * Dependency : jQuery
     */
    wp_enqueue_script('traffica-responsive-menu-2', TRAFFICA_DIR_URI . 'assets/js/jquery.meanmenu.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'traffica_wp_enqueue_scripts');

/*
* Enqueue scripts for install/activate colorway sites plugin
*/
function my_enqueue() {

   // wp_register_script('colorway-admin-settings', get_template_directory_uri() . '/includes/colorway-admin-settings/js/colorway-admin-menu-settings.js', array('jquery', 'wp-util', 'updates'));
    wp_register_script('colorway-admin-settings', get_template_directory_uri() . '/assets/js/colorway-admin-menu-settings.js', array('jquery', 'wp-util', 'updates'));
    wp_enqueue_script('colorway-admin-settings');

    $localize = array(
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'btnActivating' => __('Activating Importer Plugin ', 'traffica') . '&hellip;',
        'colorwaySitesLink' => admin_url('themes.php?page=colorway-sites'),
        'colorwaySitesLinkTitle' => __('Start Building With Elementor »', 'traffica'),
    );
    wp_localize_script('colorway-admin-settings', 'colorway', apply_filters('colorway_theme_js_localize', $localize));
    
}
if(is_admin()){
add_action('admin_enqueue_scripts', 'my_enqueue');
}

/*
* Redirect to about us page.
*/
if (is_admin() && isset($_GET['activated']) && $pagenow == "themes.php")
    wp_redirect('themes.php?page=traffica-welcome');


//traffica data update from lite to pro
$tarffica_options = array('traffica_logo' => 'traffica_logo',
    'traffica_favicon' => 'traffica_favicon',
    'inkthemes_contact_number' => 'traffica_contact_number',
    'inkthemes_nav' => 'traffica_nav',
    'inkthemes_image1' => 'traffica_image1',
    'inkthemes_slider_heading1' => 'traffica_slider_heading1',
    'inkthemes_slider_des1' => 'traffica_slider_des1',
    'inkthemes_slider_btnt1' => 'traffica_slider_btnt1',
    'inkthemes_slider_btnlink1' => 'traffica_slider_btnlink1',
    'inkthemes_image2' => 'traffica_image2',
    'inkthemes_slider_heading2' => 'traffica_slider_heading2',
    'inkthemes_slider_des2' => 'traffica_slider_des2',
    'inkthemes_slider_btnt2' => 'traffica_slider_btnt2',
    'inkthemes_slider_btnlink2' => 'traffica_slider_btnlink2',
    'inkthemes_blog_heading' => 'traffica_blog_heading',
    'inkthemes_page_main_heading' => 'traffica_page_main_heading',
    'inkthemes_page_sub_heading' => 'traffica_page_sub_heading',
    'inkthemes_fimg1' => 'traffica_fimg1',
    'inkthemes_ficon1' => 'traffica_ficon1',
    'inkthemes_firsthead' => 'traffica_firsthead',
    'inkthemes_firstdesc' => 'traffica_firstdesc',
    'inkthemes_feature_link1' => 'traffica_feature_link1',
    'inkthemes_fimg2' => 'traffica_fimg2',
    'inkthemes_ficon2' => 'traffica_ficon2',
    'inkthemes_headline2' => 'traffica_headline2',
    'inkthemes_seconddesc' => 'traffica_seconddesc',
    'inkthemes_feature_link2' => 'traffica_feature_link2',
    'inkthemes_fimg3' => 'traffica_fimg3',
    'inkthemes_ficon3' => 'traffica_ficon3',
    'inkthemes_headline3' => 'traffica_headline3',
    'inkthemes_thirddesc' => 'traffica_thirddesc',
    'inkthemes_feature_link3' => 'traffica_feature_link3',
    'inkthemes_fimg4' => 'traffica_fimg4',
    'inkthemes_ficon4' => 'traffica_ficon4',
    'inkthemes_headline4' => 'traffica_headline4',
    'inkthemes_fourthdesc' => 'traffica_fourthdesc',
    'inkthemes_feature_link4' => 'traffica_feature_link4',
    'inkthemes_about_head' => 'traffica_about_head',
    'inkthemes_about_desc' => 'traffica_about_desc',
    'inkthemes_customcss' => 'traffica_customcss',
    'inkthemes_lanstylesheet' => 'traffica_lanstylesheet'
);
$inkthemes_backup_data = get_option('inkthemes_lite_theme_data');
if (empty($inkthemes_backup_data)) {
    $traffica_lite_options = get_option('traffica_options');
    $traffica_pro_options = get_option('traffica_options');
    foreach ($tarffica_options as $key => $val) {
        $previous_data = isset($traffica_pro_options[$key]) ? $traffica_pro_options[$key] : '';
        if (empty($previous_data)) {
            traffica_update_option($key, $traffica_lite_options[$val]);
        }
    }
    update_option('inkthemes_lite_theme_data', '1');
}
function traffica_get_option($name) {
    $options = get_option('traffica_options');
    if (isset($options[$name]))
        return $options[$name];
}
//
function traffica_update_option($name, $value) {
    $options = get_option('traffica_options');
    $options[$name] = $value;
    return update_option('traffica_options', $options);
}
//
function traffica_delete_option($name) {
    $options = get_option('traffica_options');
    unset($options[$name]);
    return update_option('traffica_options', $options);
}



//Lite To Pro Data Loss Prevention Procedure
$traffica_lite_theme_option = array(
    'traffica_logo' => 'traffica_logo',
    'traffica_favicon' => 'traffica_favicon',
    'inkthemes_contact_number' => 'traffica_contact_number',
    'inkthemes_nav' => 'traffica_nav',
    'inkthemes_image1' => 'traffica_image1',
    'inkthemes_slider_heading1' => 'traffica_slider_heading1',
    'inkthemes_slider_btnt1' => 'traffica_slider_btnt1',
    'inkthemes_slider_btnlink1' => 'traffica_slider_btnlink1',
    'inkthemes_image2' => 'traffica_image2',
    'inkthemes_slider_heading2' => 'traffica_slider_heading2',
    'inkthemes_slider_des2' => 'traffica_slider_des2',
    'inkthemes_slider_btnt2' => 'traffica_slider_btnt2',
    'inkthemes_slider_btnlink2' => 'traffica_slider_btnlink2',
    'inkthemes_blog_heading' => 'traffica_blog_heading',
    'inkthemes_page_main_heading' => 'traffica_page_main_heading',
    'inkthemes_page_sub_heading' => 'traffica_page_sub_heading',
    'inkthemes_fimg1' => 'traffica_fimg1',
    'inkthemes_ficon1' => 'traffica_ficon1',
    'inkthemes_firsthead' => 'traffica_firsthead',
    'inkthemes_firstdesc' => 'traffica_firstdesc',
    'inkthemes_feature_link1' => 'traffica_feature_link1',
    'inkthemes_fimg2' => 'traffica_fimg2',
    'inkthemes_ficon2' => 'traffica_ficon2',
    'inkthemes_seconddesc' => 'traffica_seconddesc',
    'inkthemes_feature_link2' => 'traffica_feature_link2',
    'inkthemes_fimg3' => 'traffica_fimg3',
    'inkthemes_ficon3' => 'traffica_ficon3',
    'inkthemes_headline3' => 'traffica_headline3',
    'inkthemes_thirddesc' => 'traffica_thirddesc',
    'inkthemes_feature_link3' => 'traffica_feature_link3',
    'inkthemes_fimg4' => 'traffica_fimg4',
    'inkthemes_ficon4' => 'traffica_ficon4',
    'inkthemes_headline4' => 'traffica_headline4',
    'inkthemes_fourthdesc' => 'traffica_fourthdesc',
    'inkthemes_feature_link4' => 'traffica_feature_link4',
    'inkthemes_about_head' => 'traffica_about_head',
    'inkthemes_about_desc' => 'traffica_about_desc',
    'inkthemes_lanstylesheet' => 'traffica_lanstylesheet'
);
$inkthemes_litetopro_data_update = get_option('inkthemes_litetopro_data_update');
if (empty($inkthemes_litetopro_data_update)) {
    $options = get_option('traffica_options');
    if (!empty($options)) {
        foreach ($options as $key => $val) {
            $key = array_search($key, $traffica_lite_theme_option);
            if ($val != '') {
                traffica_update_option($key, $val);
            }
        }
        update_option('inkthemes_litetopro_data_update', '1');
    }
}

function traffica_body_background() {
    $bg_url = traffica_get_option('traffica_bodybg', TRAFFICA_DIR_URI . 'assets/images/bodybg.png');
    ?>
    <style type="text/css">
        body{ background:url('<?php echo esc_url($bg_url); ?>'); }		
    </style>
    <?php
}

add_action('wp_head', 'traffica_body_background');

/**
 * function to sanitize the html strings
 * Allowed only the WordPress allowed html tags and iframe html tags
 * @param string $value
 * @return string
 */
function traffica_sanitize_html($value) {

    $array = wp_kses_allowed_html('post');
    $allowedtags = array(
        'iframe' => array(
            'width' => array(),
            'height' => array(),
            'frameborder' => array(),
            'scrolling' => array(),
            'src' => array(),
            'marginwidth' => array(),
            'marginheight' => array(),
        ),
        'script' => array(
            'type' => array(),
            'src' => array()
        ),
        'i' => array(
            'class' => array()
        )
    );
    $data = array_merge($allowedtags, $array);
    $value = wp_kses($value, $data);
    return $value;
}


// Sanitize html/iframe shortcodes
//function traffica_sanitize_html($value) {
//    $array = wp_kses_allowed_html('post');
//    $allowedtags = array(
//        'iframe' => array(
//            'width' => array(),
//            'height' => array(),
//            'frameborder' => array(),
//            'scrolling' => array(),
//            'src' => array(),
//            'marginwidth' => array(),
//            'marginheight' => array()
//        )
//    );
//    $data = array_merge($allowedtags, $array);
//    $value = wp_kses($value, $data);
//    return do_shortcode($value);
//}



// Add Shortcode
function traffica_feature_box_option_generator_shortcode($atts) {
    $atts1 = shortcode_atts($atts, $content = 'null');
    if (traffica_get_option('feature_col-struct') != '4') {
        $column_class = "col-md-4";
    } else {
        $column_class = "col-md-3 col-sm-6";
    }
    ?>
    <div class = "<?php echo $column_class; ?>">
        <div class="flip-container" ontouchstart="this.classList.toggle('hover');" >
            <div class = "feature_inner_box">
                <div class="flipper">
                    <div class="front">
                        <?php if ($atts['featureimg'] != '') {
                            ?>
                            <a href="<?php echo esc_url(( $atts['featurelink'])); ?>"><img src="<?php echo $atts['featureimg']; ?>" alt="<?php __('Feature image', 'traffica'); ?>" /></a>
                        <?php } else if ($atts['featureicon'] != '') { ?>
                            <a href="<?php echo esc_url($atts['featurelink']); ?>"><i class="fa <?php echo $atts['featureicon']; ?>"></i></a>
                        <?php } else {
                            ?>
                            <a href="<?php echo esc_url($atts['featurelink']); ?>"><i class="fa fa-television"></i></a>
                            <?php
                        }
                        if ($atts['featuretitle'] != '') {
                            ?>
                            <h6 class="feature_title"><a href="<?php
                                if ($atts['featurelink'] != '') {
                                    echo esc_url($atts['featurelink']);
                                }
                                ?>"><?php echo stripslashes($atts['featuretitle']); ?></a></h6>
                            <?php } else { ?>
                            <h6><a href="#"><?php _e('Quickly With Plugins', 'traffica'); ?></a></h6>
                            <?php
                        }
                        if ($atts['featuredesc'] != '') {
                            ?>
                            <p><?php echo stripslashes($atts['featuredesc']); ?></p>
                        <?php } else { ?>
                            <p><?php _e('Site speed is one of the few ranking that Google has officially confirmed.', 'traffica'); ?></p>
                        <?php } ?>
                    </div>
                    <div class="back">
                        <?php
                        if ($atts['featureback'] != '') {
                            echo '<p>' . $atts['featureback'] . '</p>';
                        } else {
                            ?>
                            <p><?php _e('This is the demo text. This is the back of the feature box', 'traffica'); ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <?php
}

//add_shortcode('add_feature_box', 'traffica_feature_box_option_generator_shortcode');

function traffica_custom_excerpt_length($length) {
    if (!is_home()) {
        return $length;
    } else {
        return 20;
    }
}

add_filter('excerpt_length', 'traffica_custom_excerpt_length', 999);


/* ----------------------------------------------------------------------------------- */
/* Custom Menus Function
  /*----------------------------------------------------------------------------------- */

// Add CLASS attributes to the first <ul> occurence in wp_page_menu
function traffica_add_menuclass($ulclass) {
    return preg_replace('/<ul>/', '<ul class="ddsmoothmenu">', $ulclass, 1);
}

add_filter('wp_page_menu', 'traffica_add_menuclass');

function traffica_nav() {
    if (function_exists('wp_nav_menu'))
        wp_nav_menu(array('theme_location' => 'custom_menu', 'container_id' => 'menu', 'menu_class' => 'ddsmoothmenu', 'fallback_cb' => 'traffica_nav_fallback'));
    else
        traffica_nav_fallback();
}

function traffica_nav_fallback() {
    ?>
    <div id="menu">
        <ul class="ddsmoothmenu">
            <?php
            wp_list_pages('title_li=&show_home=1&sort_column=menu_order');
            ?>
        </ul>
    </div>
    <?php
}

function traffica_nav_menu_items($items) {
    if (is_home()) {
        $homelink = '<li class="current_page_item">' . '<a href="' . home_url('/') . '">' . __('Home', 'traffica') . '</a></li>';
    } else {
        $homelink = '<li>' . '<a href="' . home_url('/') . '">' . __('Home', 'traffica') . '</a></li>';
    }
    $items = $homelink . $items;
    return $items;
}

add_filter('wp_list_pages', 'traffica_nav_menu_items');
/* ----------------------------------------------------------------------------------- */
/* Breadcrumbs Plugin
  /*----------------------------------------------------------------------------------- */

function traffica_breadcrumbs() {
    $delimiter = '/';
    $home = 'Home'; // text for the 'Home' link
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb
    echo '<div id="crumbs">';
    global $post;
    $homeLink = home_url();
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
    if (is_category()) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0)
            echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo $before . __('Archive by category "', 'traffica') . single_cat_title('', false) . '"' . $after;
    }
    elseif (is_day()) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('d') . $after;
    } elseif (is_month()) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('F') . $after;
    } elseif (is_year()) {
        echo $before . get_the_time('Y') . $after;
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo $before . get_the_title() . $after;
        }
    } elseif (!is_single() && !is_page() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        //echo $before . $post_type->labels->singular_name . $after;
        echo $before . ___('Search results for "', 'traffica') . get_search_query() . '"' . $after;
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        if (!empty($cat))
            echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_page() && !$post->post_parent) {
        echo $before . get_the_title() . $after;
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb)
            echo $crumb . ' ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_search()) {
        echo $before . __('Search results for "', 'traffica') . get_search_query() . '"' . $after;
    } elseif (is_tag()) {
        echo $before . __('Posts tagged "', 'traffica') . single_tag_title('', false) . '"' . $after;
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . __('Articles posted by&nbsp;', 'traffica') . $userdata->display_name . $after;
    } elseif (is_404()) {
        echo $before . __('Error 404', 'traffica') . $after;
    }
    if (get_query_var('paged')) {
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
            echo ' (';
        echo __('Page', 'traffica') . ' ' . get_query_var('paged');
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
            echo ')';
    }
    echo '</div>';
}

//* ----------------------------------------------------------------------------------- */
/* Function to call first uploaded image in functions file
  /*----------------------------------------------------------------------------------- */

/**
 * This function thumbnail id and
 * returns thumbnail image
 * @param type $iw
 * @param type $ih 
 */
function traffica_get_thumbnail($iw, $ih) {
    $permalink = get_permalink();
    $thumb = get_post_thumbnail_id();
    $image = traffica_thumbnail_resize($thumb, '', $iw, $ih, true, 90);
    if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) {
        print "<a href='$permalink'><img class='postimg bb' src='" . $image['url'] . "' width='" . $image['width'] . "' height='" . $image['height'] . "' /></a>";
    }
}

/**
 * This function gets image width and height and
 * Prints attached images from the post        
 */
function traffica_get_image($width, $height) {
    $w = $width;
    $h = $height;
    global $post, $posts;
//This is required to set to Null
    $img_source = '';
    $permalink = get_permalink();
//	ob_start();
//	ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    if (isset($matches [1] [0])) {
        $img_source = $matches [1] [0];
    }
    $img_path = traffica_image_resize($img_source, $w, $h);
    if (!empty($img_path['url'])) {
        print "<a href='$permalink'><img src='" . $img_path['url'] . "' class='postimg' alt='" . __('Post Image', 'traffica') . "'/></a>";
    }
}

/* ----------------------------------------------------------------------------------- */
/* Attachment Page Design
  /*----------------------------------------------------------------------------------- */

//For Attachment Page
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 */
function traffica_posted_in() {
// Retrieves tag list of current post, separated by commas.
    $tag_list = get_the_tag_list('', ', ');
    if ($tag_list) {
        $posted_in = __('This entry was posted in %1$s.', 'traffica') . ' .' . __('and tagged %2$s.', 'traffica') . __('Bookmark the', 'traffica') . ' <a href="%3$s" title="Permalink to %4$s" rel="bookmark">' . __('permalink', 'traffica') . '</a>.';
    } elseif (is_object_in_taxonomy(get_post_type(), 'category')) {
        $posted_in = __('This entry was posted in %1$s.', 'traffica') . __('Bookmark the', 'traffica') . ' <a href="%3$s" title="Permalink to %4$s" rel="bookmark">' . __('permalink', 'traffica') . '</a>.';
    } else {
        $posted_in = __('Bookmark the', 'traffica') . '<a href="%3$s" title="Permalink to %4$s" rel="bookmark">' . __('permalink', 'traffica') . '</a>.';
    }
// Prints the string, replacing the placeholders.
    printf(
            $posted_in, get_the_category_list(', '), $tag_list, get_permalink(), the_title_attribute('echo=0')
    );
}

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @uses register_sidebar
 */
function traffica_widgets_init() {
// Area 1, located at the top of the sidebar.
    register_sidebar(array(
        'name' => __('Primary Widget Area', 'traffica'),
        'id' => 'primary-widget-area',
        'description' => __('The primary widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<span class="widget-h3">',
        'after_title' => '</span>',
    ));
// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
    register_sidebar(array(
        'name' => __('Secondary Widget Area', 'traffica'),
        'id' => 'secondary-widget-area',
        'description' => __('The secondary widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<span class="widget-h3">',
        'after_title' => '</span>',
    ));
// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
    register_sidebar(array(
        'name' => __('Home Page Sidebar Feature widget area', 'traffica'),
        'id' => 'home-page-widget-area',
        'description' => __('The Home Page Sidebar Feature widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<span class="widget-h3">',
        'after_title' => '</span>',
    ));
    // Area 3, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('First Footer Widget Area', 'traffica'),
        'id' => 'footer-widget-area1',
        'description' => __('The first footer widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    // Area 4, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Second Footer Widget Area', 'traffica'),
        'id' => 'footer-widget-area2',
        'description' => __('The second footer widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    // Area 5, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Third Footer Widget Area', 'traffica'),
        'id' => 'footer-widget-area3',
        'description' => __('The third footer widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    // Area 6, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Fourth Footer Widget Area', 'traffica'),
        'id' => 'footer-widget-area4',
        'description' => __('The fourth footer widget area', 'traffica'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s widget-seperator-div">',
        'after_widget' => '</aside>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
}

/** Register sidebars by running inkthemes_widgets_init() on the widgets_init hook. */
add_action('widgets_init', 'traffica_widgets_init');

/**
 * Pagination
 *
 */
function traffica_pagination($pages = '', $range = 2) {
    $showitems = ($range * 2) + 1;
//    global $paged;
    if (empty($tf_paged))
        $tf_paged = 1;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }
    if (1 != $pages) {
        echo "<ul class='paging'>";
        if ($tf_paged > 2 && $tf_paged > $range + 1 && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link(1) . "'>&laquo;</a></li>";
        if ($tf_paged > 1 && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link($tf_paged - 1) . "'>&lsaquo;</a></li>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $tf_paged + $range + 1 || $i <= $tf_paged - $range - 1) || $pages <= $showitems )) {
                echo ($tf_paged == $i) ? "<li><a href='" . get_pagenum_link($i) . "' class='current' >" . $i . "</a></li>" : "<li><a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a></li>";
            }
        }
        if ($tf_paged < $pages && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link($tf_paged + 1) . "'>&rsaquo;</a></li>";
        if ($tf_paged < $pages - 1 && $tf_paged + $range - 1 < $pages && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link($pages) . "'>&raquo;</a></li>";
        echo "</ul>\n";
    }
}

/////////Theme Options

/* ----------------------------------------------------------------------------------- */
/* Show analytics code in footer */
/* ----------------------------------------------------------------------------------- */

function traffica_childtheme_analytics() {
    $output = traffica_get_option('traffica_analytics');
    if ($output <> "")
        traffica_sanitize_html(stripslashes($output));
        // var_dump(traffica_get_option('traffica_analytics'));
}

add_action('wp_head', 'traffica_childtheme_analytics');
/* ----------------------------------------------------------------------------------- */
/* Custom CSS Styles */
/* ----------------------------------------------------------------------------------- */

function traffica_of_head_css() {
    $output = '';
    $custom_css = traffica_get_option('inkthemes_customcss');
    if ($custom_css <> '') {
        $output .= $custom_css . "\n";
    }
// Output styles
    if ($output <> '') {
        $output = "<!-- Custom Styling -->\n<style type=\"text/css\">\n" . $output . "</style>\n";
        echo $output;
    }
}

add_action('wp_head', 'traffica_of_head_css');

// activate support for thumbnails
function get_category_id($cat_name) {
    $term = get_term_by('name', $cat_name, 'category');
    return $term->term_id;
}

//Trm excerpt
function custom_trim_excerpt($length) {
    global $post;
    $explicit_excerpt = $post->post_excerpt;
    if ('' == $explicit_excerpt) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
    } else {
        $text = apply_filters('the_content', $explicit_excerpt);
    }
    $text = strip_shortcodes($text); // optional
    $text = strip_tags($text);
    $excerpt_length = $length;
    $words = explode(' ', $text, $excerpt_length + 1);
    if (count($words) > $excerpt_length) {
        array_pop($words);
        array_push($words, '...');
        $text = implode(' ', $words);
        $text = apply_filters('the_excerpt', $text);
    }
    return $text;
}

function traffica_image_post($post_id) {
    add_post_meta($post_id, 'img_key', 'on');
}

//Trm post title
function the_titlesmall($before = '', $after = '', $echo = true, $length = false) {
    $title = get_the_title();
    if ($length && is_numeric($length)) {
        $title = substr($title, 0, $length);
    }
    if (strlen($title) > 0) {
        $title = apply_filters('the_titlesmall', $before . $title . $after, $before, $after);
        if ($echo)
            echo $title;
        else
            return $title;
    }
}

/**
 * The Gallery shortcode.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 *
 * @since 2.5.0
 *
 * @param array $attr Attributes of the shortcode.
 * @return string HTML content to display gallery.
 */
//remove_shortcode('gallery');
//add_shortcode('gallery', 'traffica_gallery_shortcode');

function traffica_gallery_shortcode($attr) {
    $post = get_post();
    static $instance = 0;
    $instance++;

    if (!empty($attr['ids'])) {
        // 'ids' is explicitly ordered, unless you specify otherwise.
        if (empty($attr['orderby']))
            $attr['orderby'] = 'post__in';
        $attr['include'] = $attr['ids'];
    }

    // Allow plugins/themes to override the default gallery template.
    $output = apply_filters('post_gallery', '', $attr);
    if ($output != '')
        return $output;

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
                    ), $attr));
    $id = intval($id);
    if ('RAND' == $order)
        $orderby = 'none';

    if (!empty($include)) {
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif (!empty($exclude)) {
        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    } else {
        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    }
    if (empty($attachments))
        return '';
    if (is_feed()) {
        $output = "\n";
        foreach ($attachments as $att_id => $attachment)
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }
    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100 / $columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";
    ?>
    <script>
        //Gallery
        //Prety Photo	  
        jQuery.noConflict();
        jQuery(document).ready(function () {
            jQuery(".<?php echo $selector ?> a[rel^='prettyPhoto']").prettyPhoto({animation_speed: 'normal', theme: 'light_square', slideshow: 8000, autoplay_slideshow: true});
        });
    </script>
    <?php
    $gallery_style = $gallery_div = '';
    if (apply_filters('use_default_gallery_style', true))
        $gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
		</style>
		<!-- see gallery_shortcode() in wp-includes/media.php -->";
    $size_class = sanitize_html_class($size);
    $gallery_div = "<div id='$selector' class='$selector gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
    $gallery_ul = "<div class='row thumbnail col-{$columns}'>";
    $output = apply_filters('gallery_style', $gallery_style . "\n\t\t" . $gallery_div . $gallery_ul);
    $i = 0;
    ?>
    <?php
    foreach ($attachments as $gallery_image) {
        $class_name = '';
        $i++;
        switch ($columns) {
            case 1:
                $class_name = "col-md-12 col-sm-12 col-xs-12";
                break;
            case 2:
                $class_name = "col-md-6 col-sm-6 col-xs-12";
                break;
            case 3:
                $class_name = "col-md-4 col-sm-6 col-xs-12";
                break;
            case 4:
                $class_name = "col-md-3 col-sm-6 col-xs-12";
                break;
            case 5:
                $class_name = "col-5";
                break;
            case 6:
                $class_name = "col-md-2 col-sm-4 col-xs-12";
                break;
            case 7:
                $class_name = "col-7";
                break;
            case 8:
                $class_name = "col-8";
                break;
            case 9:
                $class_name = "col-9";
                break;
        }
        $attachment_img = wp_get_attachment_url($gallery_image->ID);
        $img_source = traffica_image_resize($attachment_img, 350, 245);
        $output .= "<div class='$class_name animated'>";
        $output .= '<a rel="prettyPhoto[gallery2]" alt="' . $gallery_image->post_excerpt . '" href="' . $attachment_img . '">';
        $output .= '<span>';
        $output .= '</span>';
        $output .= '<img src="' . $img_source['url'] . '" alt=""/>';
        $output .= '</a>';
        $output .= '<h2><a alt="' . $gallery_image->post_excerpt . '" <a class="gall-content"  href="' . '?attachment_id=' . $gallery_image->ID . '">';
        $output .= $gallery_image->post_excerpt;
        $output .= '</a></h2>';
        $output .= "</div>";
    }
    $output .= "
	<br style='clear: both;' />			
	</div>\n
	</div>"
    ;
    return $output;
}

/* ----------------------------------------------------------------------------------- */
/* // Register 'Recent Custom Posts' widget
  /*----------------------------------------------------------------------------------- */
add_action('widgets_init', 'init_rcp_recent_posts');

function init_rcp_recent_posts() {
    return register_widget('rcp_recent_posts');
}

class rcp_recent_posts extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'rcp_recent_posts',
            'description' => 'Recent Rated Posts',
        );
        parent::__construct('recent_custom_posts', 'Recent Rated Posts', $widget_ops);
    }

    /**
     * This is our Widget
     * */
    function widget($args, $instance) {
        global $post;
        $post_type = 'post';
        extract($args);
        // Widget options
        $title = apply_filters('widget_title', $instance['title']); // Title		
        $cpt = $instance['types']; // Post type(s) 		
        $types = explode(',', $cpt); // Let's turn this into an array we can work with.
        $number = $instance['number']; // Number of posts to show
        // Output
        echo $before_widget;

        if ($title)
            echo $before_title . $title . $after_title;
        ?>
        <ul class="ratting_widget">
            <?php
            query_posts('showposts=' . $number . '&post_type=' . $post_type);
            $wp_query->is_archive = true;
            $wp_query->is_home = false;
            if (have_posts()) : while (have_posts()) : the_post();
                    ?>
                    <li>
                        <div class="widget_thumb">
                            <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>
                                <?php the_post_thumbnail('post_thumbnail_1', array('class' => 'postimg')); ?>
                            <?php } else { ?>
                                <?php traffica_get_image(66, 60); ?> 
                                <?php
                            }
                            ?></div>
                        <div class="widget_content">
                            <h6><a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID() ); ?>">		<?php
                                    $tit = the_title('', '', FALSE);
                                    echo substr($tit, 0, 30);
                                    if (strlen($tit) > 30)
                                        echo "...";
                                    ?></a></h6>
                            <h6 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">

                                </a></h6>
                            <?php echo custom_trim_excerpt(7); ?>
                        </div>
                    </li>
                    <div class="clear"></div>
                    <?php
                endwhile;
                wp_reset_query();
            endif;
            ?>
        </ul>
        <?php
        // echo widget closing tag
        echo $after_widget;
    }

    /** Widget control update */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;

        //Let's turn that array into something the WordPress database can store
        $types = implode(',', (array) $new_instance['types']);

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['types'] = $types;
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }

    /**
     * Widget settings
     * */
    function form($instance) {

        // instance exist? if not set defaults
        if ($instance) {
            $title = $instance['title'];
            $types = $instance['types'];
            $number = $instance['number'];
        } else {
            //These are our defaults
            $title = '';
            $types = 'post';
            $number = '5';
        }

        //Let's turn $types into an array
        $types = explode(',', $types);

        //Count number of post types for select box sizing
        $cpt_types = get_post_types(array('public' => true), 'names');
        foreach ($cpt_types as $cpt) {
            $cpt_ar[] = $cpt;
        }
        $n = count($cpt_ar);
        if ($n > 10) {
            $n = 10;
        }

        // The widget form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'traffica'); ?></label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:', 'traffica'); ?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
        <?php
    }

}



/**
* Theme options to customizer migration function
*/
function theme_options_to_customizer(){ 
    $customizer_data = array();

    /*
     *Fetch all the theme options ids 
    */
    $theme_options_data = get_option('inkthemes_options');

    /*
    *To display theme options 
    */
    // echo "<pre>===";
    // print_r($theme_options_data);
    // echo "</pre>";
    
    /*
    * Replacing inkthemes string with theme name
    */  
    foreach ($theme_options_data as $key => $value) {      
        if (stripos("'.$key.'", 'inkthemes') !== false) {
            $key = str_replace("inkthemes", "traffica", $key);
            $customizer_data[$key] = $value;
        }
        else {
            $customizer_data[$key] = $value;
        }                      
    }

    /*
    *To display theme customizer options 
    */
    // echo "<pre>";
    // print_r($customizer_data);
    // echo "</pre>";
    
    /**
     * Updating the customizer options
     */
    update_option('traffica_options', $customizer_data); 
}

 /**
 *Checking data migrating status (Theme options to Customizer) 
 */
$customizer_data_status = get_option('theme_options_to_customizer_status');
if ($customizer_data_status != 'completed' && $customizer_data_status != '' && $customizer_data_status != null) {
    add_action('after_setup_theme', 'theme_options_to_customizer');
    update_option('theme_options_to_customizer_status', 'completed');
}




function traffica_tracking_admin_notice() {
   global $current_user;
   $user_id = $current_user->ID;
   $my_theme = wp_get_theme();    
   /* Check that the user hasn't already clicked to ignore the message */
   if (!get_user_meta($user_id, 'wp_email_tracking_ignore_notice')) {
       
       ?>
       <div class="updated um-admin-notice"><p><?php _e('Allow Traffica theme to send you setup guide? Opt-in to our newsletter and we will immediately e-mail you a setup guide along with 20% discount which you can use to purchase any theme.', 'traffica'); ?></p><p><a href="<?php echo get_template_directory_uri() . '/includes/smtp.php?wp_email_tracking=email_smtp_allow_tracking'; ?>" class="button button-primary"><?php _e('Allow Sending', 'traffica'); ?></a>&nbsp;<a href="<?php echo get_template_directory_uri() . '/includes/smtp.php?wp_email_tracking=email_smtp_hide_tracking'; ?>" class="button-secondary"><?php _e('Do not allow', 'traffica'); ?></a></p></div>
       <?php
   }
}


add_action('admin_notices', 'traffica_tracking_admin_notice');

/*
 * * Enqueue Google Fonts
 */
//function traffica_gfonts_scripts() {
//    wp_enqueue_style('traffica-google-fonts', traffica_google_fonts());
//    add_action('wp_head', 'traffica_typography');
//}
//add_action('wp_enqueue_scripts', 'traffica_gfonts_scripts');
//
//$fonts = array('ABeeZee', 'Abel', 'Abril Fatface', 'Aclonica', 'Acme', 'Actor', 'Adamina', 'Advent Pro', 'Aguafina Script', 'Akronim', 'Aladin', 'Aldrich', 'Alef', 'Alegreya', 'Alegreya SC', 'Alex Brush', 'Alfa Slab One', 'Alice', 'Alike', 'Alike Angular', 'Allan', 'Allerta', 'Allerta Stencil', 'Allura', 'Almendra', 'Almendra Display', 'Almendra SC', 'Amarante', 'Amaranth', 'Amatic SC', 'Amethysta', 'Anaheim', 'Andada', 'Andika', 'Angkor', 'Annie Use Your Telescope', 'Anonymous Pro', 'Antic', 'Antic Didone', 'Antic Slab', 'Anton', 'Arapey', 'Arbutus', 'Arbutus Slab', 'Architects Daughter', 'Archivo Black', 'Archivo Narrow', 'Arial Black', 'Arial Narrow', 'Arimo', 'Arizonia', 'Armata', 'Artifika', 'Arvo', 'Asap', 'Asset', 'Astloch', 'Asul', 'Atomic Age', 'Aubrey', 'Audiowide', 'Autour One', 'Average', 'Average Sans', 'Averia Gruesa Libre', 'Averia Libre', 'Averia Sans Libre', 'Averia Serif Libre', 'Bad Script', 'Balthazar', 'Bangers', 'Basic', 'Battambang', 'Baumans', 'Bayon', 'Belgrano', 'Bell MT', 'Bell MT Alt', 'Belleza', 'BenchNine', 'Bentham', 'Berkshire Swash', 'Bevan', 'Bigelow Rules', 'Bigshot One', 'Bilbo', 'Bilbo Swash Caps', 'Bitter', 'Black Ops One', 'Bodoni', 'Bokor', 'Bonbon', 'Boogaloo', 'Bowlby One', 'Bowlby One SC', 'Brawler', 'Bree Serif', 'Bubblegum Sans', 'Bubbler One', 'Buenard', 'Butcherman', 'Butcherman Caps', 'Butterfly Kids', 'Cabin', 'Cabin Condensed', 'Cabin Sketch', 'Caesar Dressing', 'Cagliostro', 'Calibri', 'Calligraffitti', 'Cambo', 'Cambria', 'Candal', 'Cantarell', 'Cantata One', 'Cantora One', 'Capriola', 'Cardo', 'Carme', 'Carrois Gothic', 'Carrois Gothic SC', 'Carter One', 'Caudex', 'Cedarville Cursive', 'Ceviche One', 'Changa One', 'Chango', 'Chau Philomene One', 'Chela One', 'Chelsea Market', 'Chenla', 'Cherry Cream Soda', 'Cherry Swash', 'Chewy', 'Chicle', 'Chivo', 'Cinzel', 'Cinzel Decorative', 'Clara', 'Clicker Script', 'Coda', 'Codystar', 'Combo', 'Comfortaa', 'Coming Soon', 'Concert One', 'Condiment', 'Consolas', 'Content', 'Contrail One', 'Convergence', 'Cookie', 'Copse', 'Corben', 'Corsiva', 'Courgette', 'Courier New', 'Cousine', 'Coustard', 'Covered By Your Grace', 'Crafty Girls', 'Creepster', 'Creepster Caps', 'Crete Round', 'Crimson Text', 'Croissant One', 'Crushed', 'Cuprum', 'Cutive', 'Cutive Mono', 'Damion', 'Dancing Script', 'Dangrek', 'Dawning of a New Day', 'Days One', 'Delius', 'Delius Swash Caps', 'Delius Unicase', 'Della Respira', 'Denk One', 'Devonshire', 'Dhyana', 'Didact Gothic', 'Diplomata', 'Diplomata SC', 'Domine', 'Donegal One', 'Doppio One', 'Dorsa', 'Dosis', 'Dr Sugiyama', 'Droid Arabic Kufi', 'Droid Arabic Naskh', 'Droid Sans', 'Droid Sans Mono', 'Droid Sans TV', 'Droid Serif', 'Duru Sans', 'Dynalight', 'EB Garamond', 'Eagle Lake', 'Eater', 'Eater Caps', 'Economica', 'Electrolize', 'Elsie', 'Elsie Swash Caps', 'Emblema One', 'Emilys Candy', 'Engagement', 'Englebert', 'Enriqueta', 'Erica One', 'Esteban', 'Euphoria Script', 'Ewert', 'Exo', 'Expletus Sans', 'Fanwood Text', 'Fascinate', 'Fascinate Inline', 'Faster One', 'Fasthand', 'Fauna One', 'Federant', 'Federo', 'Felipa', 'Fenix', 'Finger Paint', 'Fjalla One', 'Fjord One', 'Flamenco', 'Flavors', 'Fondamento', 'Fontdiner Swanky', 'Forum', 'Francois One', 'Freckle Face', 'Fredericka the Great', 'Fredoka One', 'Freehand', 'Fresca', 'Frijole', 'Fruktur', 'Fugaz One', 'GFS Didot', 'GFS Neohellenic', 'Gabriela', 'Gafata', 'Galdeano', 'Galindo', 'Garamond', 'Gentium Basic', 'Gentium Book Basic', 'Geo', 'Geostar', 'Geostar Fill', 'Germania One', 'Gilda Display', 'Give You Glory', 'Glass Antiqua', 'Glegoo', 'Gloria Hallelujah', 'Goblin One', 'Gochi Hand', 'Gorditas', 'Goudy Bookletter 1911', 'Graduate', 'Grand Hotel', 'Gravitas One', 'Great Vibes', 'Griffy', 'Gruppo', 'Gudea', 'Habibi', 'Hammersmith One', 'Hanalei', 'Hanalei Fill', 'Handlee', 'Hanuman', 'Happy Monkey', 'Headland One', 'Helvetica Neue', 'Henny Penny', 'Herr Von Muellerhoff', 'Holtwood One SC', 'Homemade Apple', 'Homenaje', 'IM Fell DW Pica', 'IM Fell DW Pica SC', 'IM Fell Double Pica', 'IM Fell Double Pica SC', 'IM Fell English', 'IM Fell English SC', 'IM Fell French Canon', 'IM Fell French Canon SC', 'IM Fell Great Primer', 'IM Fell Great Primer SC', 'Iceberg', 'Iceland', 'Imprima', 'Inconsolata', 'Inder', 'Indie Flower', 'Inika', 'Irish Grover', 'Irish Growler', 'Istok Web', 'Italiana', 'Italianno', 'Jacques Francois', 'Jacques Francois Shadow', 'Jim Nightshade', 'Jockey One', 'Jolly Lodger', 'Josefin Sans', 'Josefin Sans Std Light', 'Josefin Slab', 'Joti One', 'Judson', 'Julee', 'Julius Sans One', 'Junge', 'Jura', 'Just Another Hand', 'Just Me Again Down Here', 'Kameron', 'Karla', 'Kaushan Script', 'Kavoon', 'Keania One', 'Kelly Slab', 'Kenia', 'Khmer', 'Kite One', 'Knewave', 'Kotta One', 'Koulen', 'Kranky', 'Kreon', 'Kristi', 'Krona One', 'La Belle Aurore', 'Lancelot', 'Lateef', 'Lato', 'League Script', 'Leckerli One', 'Ledger', 'Lekton', 'Lemon', 'Lemon One', 'Libre Baskerville', 'Life Savers', 'Lilita One', 'Lily Script One', 'Limelight', 'Linden Hill', 'Lobster', 'Lobster Two', 'Lohit Bengali', 'Lohit Devanagari', 'Lohit Tamil', 'Londrina Outline', 'Londrina Shadow', 'Londrina Sketch', 'Londrina Solid', 'Lora', 'Love Ya Like A Sister', 'Loved by the King', 'Lovers Quarrel', 'Luckiest Guy', 'Lusitana', 'Lustria', 'Macondo', 'Macondo Swash Caps', 'Magra', 'Maiden Orange', 'Mako', 'Marcellus', 'Marcellus SC', 'Marck Script', 'Margarine', 'Marko One', 'Marmelad', 'Marvel', 'Mate', 'Mate SC', 'Maven Pro', 'McLaren', 'Meddon', 'MedievalSharp', 'Medula One', 'Megrim', 'Meie Script', 'Merienda', 'Merienda One', 'Merriweather', 'Merriweather Sans', 'Metal', 'Metal Mania', 'Metamorphous', 'Metrophobic', 'Michroma', 'Milonga', 'Miltonian', 'Miltonian Tattoo', 'Miniver', 'Miss Fajardose', 'Miss Saint Delafield', 'Modern Antiqua', 'Molengo', 'Monda', 'Monofett', 'Monoton', 'Monsieur La Doulaise', 'Montaga', 'Montez', 'Montserrat', 'Montserrat Alternates', 'Montserrat Subrayada', 'Moul', 'Moulpali', 'Mountains of Christmas', 'Mouse Memoirs', 'Mr Bedford', 'Mr Bedfort', 'Mr Dafoe', 'Mr De Haviland', 'Mrs Saint Delafield', 'Mrs Sheppards', 'Muli', 'Mystery Quest', 'Neucha', 'Neuton', 'New Rocker', 'News Cycle', 'Niconne', 'Nixie One', 'Nobile', 'Nokora', 'Norican', 'Nosifer', 'Nosifer Caps', 'Nothing You Could Do', 'Noticia Text', 'Noto Sans', 'Noto Sans UI', 'Noto Serif', 'Nova Cut', 'Nova Flat', 'Nova Mono', 'Nova Oval', 'Nova Round', 'Nova Script', 'Nova Slim', 'Nova Square', 'Numans', 'Nunito', 'OFL Sorts Mill Goudy TT', 'Odor Mean Chey', 'Offside', 'Old Standard TT', 'Oldenburg', 'Oleo Script', 'Oleo Script Swash Caps', 'Open Sans', 'Oranienbaum', 'Orbitron', 'Oregano', 'Orienta', 'Original Surfer', 'Oswald', 'Over the Rainbow', 'Overlock', 'Overlock SC', 'Ovo', 'Oxygen', 'Oxygen Mono', 'PT Mono', 'PT Sans', 'PT Sans Caption', 'PT Sans Narrow', 'PT Serif', 'PT Serif Caption', 'Pacifico', 'Paprika', 'Parisienne', 'Passero One', 'Passion One', 'Pathway Gothic One', 'Patrick Hand', 'Patrick Hand SC', 'Patua One', 'Paytone One', 'Peralta', 'Permanent Marker', 'Petit Formal Script', 'Petrona', 'Philosopher', 'Piedra', 'Pinyon Script', 'Pirata One', 'Plaster', 'Play', 'Playball', 'Playfair Display', 'Playfair Display SC', 'Podkova', 'Poiret One', 'Poller One', 'Poly', 'Pompiere', 'Pontano Sans', 'Port Lligat Sans', 'Port Lligat Slab', 'Prata', 'Preahvihear', 'Press Start 2P', 'Princess Sofia', 'Prociono', 'Prosto One', 'Proxima Nova', 'Proxima Nova Tabular Figures', 'Puritan', 'Purple Purse', 'Quando', 'Quantico', 'Quattrocento', 'Quattrocento Sans', 'Questrial', 'Quicksand', 'Quintessential', 'Qwigley', 'Racing Sans One', 'Radley', 'Raleway', 'Raleway Dots', 'Rambla', 'Rammetto One', 'Ranchers', 'Rancho', 'Rationale', 'Redressed', 'Reenie Beanie', 'Revalia', 'Ribeye', 'Ribeye Marrow', 'Righteous', 'Risque', 'Roboto', 'Roboto Condensed', 'Roboto Slab', 'Rochester', 'Rock Salt', 'Rokkitt', 'Romanesco', 'Ropa Sans', 'Rosario', 'Rosarivo', 'Rouge Script', 'Ruda', 'Rufina', 'Ruge Boogie', 'Ruluko', 'Rum Raisin', 'Ruslan Display', 'Russo One', 'Ruthie', 'Rye', 'Sacramento', 'Sail', 'Salsa', 'Sanchez', 'Sancreek', 'Sansita One','Sans Serif', 'Sarina', 'Satisfy', 'Scada', 'Scheherazade', 'Schoolbell', 'Seaweed Script', 'Sevillana', 'Seymour One', 'Shadows Into Light', 'Shadows Into Light Two', 'Shanti', 'Share', 'Share Tech', 'Share Tech Mono', 'Shojumaru', 'Short Stack', 'Siamreap', 'Siemreap', 'Sigmar One', 'Signika', 'Signika Negative', 'Simonetta', 'Sintony', 'Sirin Stencil', 'Six Caps', 'Skranji', 'Slackey', 'Smokum', 'Smythe', 'Snippet', 'Snowburst One', 'Sofadi One', 'Sofia', 'Sonsie One', 'Sorts Mill Goudy', 'Source Code Pro', 'Source Sans Pro', 'Special Elite', 'Spicy Rice', 'Spinnaker', 'Spirax', 'Squada One', 'Stalemate', 'Stalin One', 'Stalinist One', 'Stardos Stencil', 'Stint Ultra Condensed', 'Stint Ultra Expanded', 'Stoke', 'Strait', 'Sue Ellen Francisco', 'Sunshiney', 'Supermercado One', 'Suwannaphum', 'Swanky and Moo Moo', 'Syncopate', 'Tahoma', 'Tangerine', 'Taprom', 'Tauri', 'Telex', 'Tenor Sans', 'Terminal Dosis', 'Terminal Dosis Light', 'Text Me One', 'Thabit', 'The Girl Next Door', 'Tienne', 'Tinos', 'Titan One', 'Titillium Web', 'Trade Winds', 'Trocchi', 'Trochut', 'Trykker', 'Tulpen One', 'Ubuntu', 'Ubuntu Condensed', 'Ubuntu Mono', 'Ultra', 'Uncial Antiqua', 'Underdog', 'Unica One', 'UnifrakturMaguntia', 'Unkempt', 'Unlock', 'Unna', 'VT323', 'Vampiro One', 'Varela', 'Varela Round', 'Vast Shadow', 'Vibur', 'Vidaloka', 'Viga', 'Voces', 'Volkhov', 'Vollkorn', 'Voltaire', 'Waiting for the Sunrise', 'Wallpoet', 'Walter Turncoat', 'Warnes', 'Wellfleet', 'Wendy One', 'Wire One', 'Yanone Kaffeesatz', 'Yellowtail', 'Yeseva One', 'Yesteryear', 'Zeyada', 'jsMath cmbx10', 'jsMath cmex10', 'jsMath cmmi10', 'jsMath cmr10', 'jsMath cmsy10', 'jsMath cmti10');
//
///* String of Google fonts */
//function traffica_google_fonts() {
//    global $fonts;
//    $fonts_collection = add_query_arg(array(
//        "family" => urlencode(implode("|", $fonts)),
//        "subset" => "latin"
//            ), 'https://fonts.googleapis.com/css');
//    return $fonts_collection;
//}
//
/////* typography style CSS code function */
//function traffica_typography() {
//     global $fonts;
//
//    $index = traffica_get_option('typography_logo_family');
//        $index1 = traffica_get_option('typography_nav_family');
//
//if (traffica_get_option('typography_logo_family')!=''){
//    echo "<style type='text/css'> .logo{ font-family: " . $fonts[$index] . ", sans-serif!important; font-weight:500; }</style>";
//}
//
//if (traffica_get_option('typography_nav_family')!=''){
//    echo "<style type='text/css'> body{ font-family: " . $fonts[$index1] . ", sans-serif!important; font-weight:500; }</style>";
//
//}
//}


/*
 * Colorway sites plugin activation function
 * **/
add_action('wp_ajax_colorway-sites-plugin-activate', 'required_plugin_activate1');
add_action('wp_ajax_nopriv_colorway-sites-plugin-activate', 'required_plugin_activate1');

function required_plugin_activate1() {
            
           
            if (!current_user_can('install_plugins') || !isset($_POST['init']) || !sanitize_key(wp_unslash($_POST['init']))) {
                wp_send_json_error(
                        array(
                            'success' => false,
                            'message' => __('No plugin specified', 'traffica'),
                        )
                );
            }

            $plugin_init = ( isset($_POST['init']) ) ? esc_attr(wp_unslash($_POST['init'])) : '';
                       
            $activate = activate_plugin($plugin_init, '', false, true);

            if (is_wp_error($activate)) {
                wp_send_json_error(
                        array(
                            'success' => false,
                            'message' => $activate->get_error_message(),
                        )
                );
            }

            wp_send_json_success(
                    array(
                        'success' => true,
                        'message' => __('Plugin Successfully Activated', 'traffica'),
                        'redirect_url' => admin_url('themes.php?page=colorway-sites'),
                    )
            );
        }

// define the customize_preview_init callback 
function action_customize_preview_init( $array ) { 
    wp_enqueue_script('inkthemes_customizer_js', get_template_directory_uri().'/functions/js/customizer.js', array( 'jquery','customize-preview' ),false, true);

}; 

add_action( 'customize_preview_init', 'action_customize_preview_init', 10, 1 ); 