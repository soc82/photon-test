<div class="row">
    <div class="col-md-3 col-sm-3">
        <div class="footer_widget first">
            <?php
            if (is_active_sidebar('footer-widget-area1')){
                dynamic_sidebar('footer-widget-area1');
            }
            
            
                ?>
                
       
    </div>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer_widget">
            <?php
            if (is_active_sidebar('footer-widget-area2')) 
            {dynamic_sidebar('footer-widget-area2');}
          
                ?> 
                
        </div>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer_widget">
            <?php
            if (is_active_sidebar('footer-widget-area3')) {
                dynamic_sidebar('footer-widget-area3');
            }
                
            
                ?>
               
        </div>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer_widget last">
            <?php if (is_active_sidebar('footer-widget-area4')) {
                dynamic_sidebar('footer-widget-area4');
            } ?>
                
        </div>
    </div>
</div>
