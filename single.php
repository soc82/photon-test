<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage infoway
 * @since infoway 1.0
 */
get_header();
get_template_part("templates/breadcrumb");
?>
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8 col-sm-8">
                    <div class="content-bar">
                        <!-- Start the Loop. -->
                        <?php if (have_posts()) : while (have_posts()) : the_post(); 
                        echo 'single'?>
                                <!--Start post-->
                                <div class="post single">
                                    <div class="post_heading_wrapper">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2">
                                                <div class="post_date">
                                                    <ul class="date">
                                                        <li class="day"><?php echo get_the_time('d') ?></li>
                                                        <li class="month"><?php echo get_the_time('M') ?></li>
                                                        <li class="year"><?php echo get_the_time('Y') ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-sm-10">
                                                <h1 class="post_title"><?php the_title(); ?></h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <ul class="post_meta">
                                        <li class="posted_by"><span><?php esc_html_e('Posted By', 'traffica'); ?></span><?php the_author_posts_link(); ?></li>
                                        <li class="post_category"><span><?php esc_html_e('In', 'traffica'); ?></span><?php the_category(', '); ?></li>

                                        <?php if (get_comments_number($post->ID) > 0 && !post_password_required()) {
                                            ?>
                                            <li class="post_comment"><span></span><?php comments_popup_link(__('No Comments.', 'traffica'), __('1 Comment.', 'traffica'), __('% Comments.', 'traffica')); ?></li>
                                        <?php } ?>


                                    </ul>
                                    <div class="post_content"><?php
                                        the_content();
                                        wp_link_pages(array(
                                            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'traffica') . '</span>',
                                            'after' => '</div>',
                                            'link_before' => '<span>',
                                            'link_after' => '</span>',
                                            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'traffica') . ' </span>%',
                                            'separator' => '<span class="screen-reader-text">, </span>',
                                        ));
                                        ?></div>
                                    <div class="clear"></div>
                                    <?php if (has_tag()) { ?>
                                        <div class="tag">
                                            <?php
                                            /**
                                             * Include the post tags
                                             */
                                            the_tags(__('Post Tagged with ', 'traffica'), ', ', '');
                                            ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <!--End post-->
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                        <div class="clear"></div>
                        <!--End post-->
                        <nav id="nav-single"> <span class="nav-previous">
                                <?php previous_post_link('%link', '<span class="meta-nav">&larr;</span>' . __(' Previous Post', 'traffica')); ?>
                            </span> <span class="nav-next">
                                <?php next_post_link('%link', __('Next Post ', 'traffica') . ' <span class="meta-nav">&rarr;</span>'); ?>
                            </span> </nav>
                        <div class="clear"></div>
                        <?php wp_link_pages(array('before' => '<div class="clear"></div><div class="page-link"><span>' . __('Pages:', 'traffica') . '</span>', 'after' => '</div>')); ?>
                        <!--Start Comment box-->
                        <?php comment_form(); ?>
                        <!--End Comment box-->
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>