<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 */
get_header();
get_template_part("templates/breadcrumb");
?>  
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8 col-sm-8">
                    <div class="content-bar">
                        <?php // the_content();   ?>	
                        <header class="entry-header">
                            <p>
                                <?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching, or one of the links below, can help.', 'traffica'); ?>
                            </p>
                            <?php
                            get_search_form();
                            the_widget('WP_Widget_Recent_Posts', array('number' => 10), array('widget_id' => '404'));
                            ?>
                            <div class="widget">
                                <h2 class="widgettitle">
                                    <?php esc_html_e('Most Used Categories', 'traffica'); ?>
                                </h2>
                                <ul>
                                    <?php wp_list_categories(array('orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10)); ?>
                                </ul>
                            </div>
                            <?php
                            /* translators: %1$s: smilie */
                            $archive_content = '<p>' . sprintf(__('Try looking in the monthly archives. %1$s', 'traffica'), convert_smilies(':)')) . '</p>';
                            the_widget('WP_Widget_Archives', array('count' => 0, 'dropdown' => 1), array('after_title' => '</h2>' . $archive_content));

                            the_widget('WP_Widget_Tag_Cloud');
                            ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>