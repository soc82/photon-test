var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS    = require('gulp-minify-css');
var browserSync  = require('browser-sync');
var httpProxy    = require('http-proxy');
var php          = require('gulp-connect-php');
const minify     = require('gulp-minify');
let cleanCSS     = require('gulp-clean-css');
const cssScss    = require('gulp-css-scss');
var rename       = require('gulp-rename');

const reload     = browserSync.reload;


gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://yourlocalenvironment.local:8888" // Change to your local url for development
    });
});

gulp.task('css-scss', () => {
  return gulp.src('style.css')
    .pipe(cssScss())
    .pipe(gulp.dest('scss'));
});



gulp.task('sass', function(){
  gulp.watch('scss/**/*.scss', ['sass']);
  return gulp.src('scss/main.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('css/'));
});



gulp.task('minify-css', () => {
    gulp.watch(['css/*.css', '!css/*.min.css'], ['minify-css']);
    return gulp.src(['css/*.css', '!css/*.min.css'])
      .pipe(rename({
          suffix: '.min'
      }))
      .pipe(cleanCSS({debug: true}, (details) => {
        console.log(`${details.name}: ${details.stats.originalSize}`);
        console.log(`${details.name}: ${details.stats.minifiedSize}`);
      }))
    .pipe(gulp.dest('css'));
});


gulp.task('js', function(){
    gulp.watch('js/*.js', ['js']);
    browserSync.reload()
});

gulp.task('compile', ['sass', 'minify-css','browser-sync']); 
gulp.task('convert', ['css-scss']); 