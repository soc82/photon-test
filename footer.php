 
<div class="footer-container">
    <?php
    $footer_widget_content = traffica_get_option('footer_widget_on_off');
    $footer_widget_content_on = "on";
    if ($footer_widget_content == $footer_widget_content_on || $footer_widget_content != 'off') {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer">
                            <?php
                            /* A sidebar in the footer? Yep. You can can customize
                             * your footer with four columns of widgets.
                             */
                            get_sidebar('footer');
                            ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
<?php } ?>
</div>

<div class="footer-line-container">
    <div class="footer-line">
    </div>
</div>
<div class="clear"></div>
<div class="bottom-footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bottom_footer_content row traffica_footertext">        
                    <div class="col-md-6 col-sm-6">        
                        <div class="bottom-inner-footer copyrightinfo"><div>
                                <div class="copyrightinfo">
                                    <p class="copyright"><?php esc_html_e('Traffica Theme by', 'traffica'); ?> <a rel="nofollow" href="<?php esc_url('http://www.inkthemes.com'); ?>"><?php esc_html_e('InkThemes.com', 'traffica'); ?></a></p>
                                </div>
                            </div> </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="social_logos social_icon_1">			
                            <?php
                        $social_icon_array = array('social_icon_1' => 'social_link_1', 'social_icon_2' => 'social_link_2', 'social_icon_3' => 'social_link_3', 'social_icon_4' => 'social_link_4');
                        $i = 1;                       
                        foreach ($social_icon_array as $icon => $slink) {
                            if ( traffica_get_option($icon) != '') {
                                ?>
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-<?php echo traffica_get_option("social_icon_" .$i); ?>"></i>
                                    </a> 
                                </li>
                                <?php
                            }
                            $i++;
                        }
                        ?> 
                        </ul>    
                    </div>                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
