<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class traffica_Customizer {

    public static function traffica_Register($wp_customize) {

        self::traffica_Sections($wp_customize);

        self::traffica_Controls($wp_customize);
    }

    public static function traffica_Sections($wp_customize) {

        /**
         * Custom Logo
         */
        $wp_customize->add_section('title_tagline', array(
            'title' => __('Site Title & logo', 'traffica'),
            'description' => __('Allows you to setup site title and tagline for Traffica Theme.', 'traffica'),
            'priority' => '1',
            'panel' => '',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Remove control for site icon
         */
        $wp_customize->remove_control('site_icon');

        /**
         * Logo and favicon section
         */
//        $wp_customize->add_section('logo_fevi_setting', array(
//            'title' => __('Logo & Favicon', 'traffica'),
//            'description' => __('Allows you to customize header logo, favicon settings for Traffica Theme.', 'traffica'), //Descriptive tooltip
//            'panel' => '',
//            'priority' => '2',
//            'capability' => 'edit_theme_options'
//                )
//        );

        /**
         * Background Image Section
         */
        $wp_customize->add_section('background_image', array(
            'title' => __('Background Image setting', 'traffica'),
            'description' => __('Allows you to change background image for Traffica Theme. This will overright the background color property', 'traffica'),
            'priority' => '3',
            'panel' => '',
            'capability' => 'edit_theme_options'
        ));
        
                /**
         * Header Layout Section
         */
        $wp_customize->add_section('header_layout_section', array(
            'title' => __('Header Layout', 'traffica'),
            'priority' => '3',
            'description' => __('Allows you to setup Header Layout for Traffica Theme.', 'traffica'),
            //'panel' => 'layout_setting_panel'
        ));

        /**
         * Typography
         * 
         */
        $wp_customize->add_section('typography_section', array(
            'title' => __('Typography', 'traffica'),
            /* translators: %s: search term */
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Typography!</h3> Allows you to set Font Family for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'priority' => '4',
            'panel' => ''
        ));


        /**
         * Google Analytics Tracking Code Section
         */
        $wp_customize->add_section('tracking_code_setting', array(
            'title' => __('Google Analytics Tracking Code', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Google Analytics Tracking Code!</h3> Paste your Google Analytics (or other) Tracking Code here.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'panel' => '',
            'priority' => '13',
            'capability' => 'edit_theme_options'
                )
        );


        /**
         * Mobile Navigation Menu
         */
        $wp_customize->add_section('mobile_navigation_menu', array(
            'title' => __('Mobile Navigation Menu', 'traffica'),
            'description' => __('Allows you to Navigation Menu for Traffica Theme.', 'traffica'), //Descriptive tooltip
            'panel' => '',
            'priority' => '16',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Front Page On/Off
         */
//        $wp_customize->add_section('dummy_data_setting', array(
//            'title' => __('Front Page On/Off', 'traffica'),
//            'description' => __('Allows you to enable or disable Front Page (On/Off) for Traffica Theme.', 'traffica'), //Descriptive tooltip
//            'panel' => '',
//            'priority' => '17',
//            'capability' => 'edit_theme_options'
//                )
//        );

        /**
         * Contact Number For Tap To Call Feature
         */
        $wp_customize->add_section('call_feature_setting', array(
            'title' => __('Tap To Call Feature', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Tap to call feature!</h3> Allows you to set tap to call for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'panel' => '',
            'priority' => '10',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Add panel for Bottom footer settings
         */
        $wp_customize->add_panel('footer_settings_panel', array(
            'title' => __('Bottom Footer Widget Settings', 'traffica'),
            'description' => __("Allows you to setup Footer area for traffica Theme.", 'traffica'),
            'priority' => '12',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Widget Area Settings Setting
         */
        $wp_customize->add_section('widget_area_section', array(
            'title' => __('Widget Area Section', 'traffica'),
            'description' => sprintf(__("<b>You Can Customize Your Widget Area By Following These Steps</b><br/><br/><ul><li>Go To Dashboard->Appearance->Widget.</li><br/><li> Place your desired Widgets under the Footer Widget Areas (First, Second, Third, etc.) they will appear on the footer of your site. </li><br/><li> If you want to hide the footer widget contents then, just place a Blank Text Widget under Footer Widget Areas. </li><br/><li> You can also refer this tutorial link: <a href='https://www.inkthemes.com/how-to-use-widgets-in-wordpress/'>How To Use Widgets In Wordpress</a></li><br/> <h3>Check out the <a href='%s' target='_blank'>PRO version </a>for full control over Footer Widget On/Off ! </h3><br/> Allows you to turn on and off Footer Area Widgets for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/')),
            'priority' => '',
            'panel' => 'footer_settings_panel'
        ));

        /**
         * Footer text Setting
         */
        $wp_customize->add_section('footer_section_copyright', array(
            'title' => __('Copyright Footer Text', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Copyright Footer Text!</h3> ", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'priority' => '',
            'panel' => 'footer_settings_panel'
        ));

        /**
         * Home page slider panel
         */
        $wp_customize->add_panel('home_page_slider_panel', array(
            'title' => __('Slider Settings', 'traffica'),
            'description' => __('Allows you to setup home page slider for Traffica Theme.', 'traffica'),
            'priority' => '5',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Slider controle section
         */
        $wp_customize->add_section('home_page_slider_control', array(
            'title' => __('Slider Control', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control to Change home page Slider type as per your requirement.!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Slider Speed Section
         */
        $wp_customize->add_section('home_page_slider_speed', array(
            'title' => __('Slider Speed', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control to Change home page Slider Speed as per your requirement.!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * First Slider section
         */
        $wp_customize->add_section('home_page_slider_1', array(
            'title' => __('First Slider', 'traffica'),
            'description' => __('Allows you to setup first slider for Traffica Theme.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Second Slider section
         */
        $wp_customize->add_section('home_page_slider_2', array(
            'title' => __('Second Slider', 'traffica'),
            'description' => __('Allows you to setup second slider for Traffica Theme.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Third Slider section
         */
        $wp_customize->add_section('home_page_slider_3', array(
            'title' => __('Third Slider', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control Over Slider!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Fourth Slider section
         */
        $wp_customize->add_section('home_page_slider_4', array(
            'title' => __('Fourth Slider', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control Over Slider!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Fifth Slider section
         */
        $wp_customize->add_section('home_page_slider_5', array(
            'title' => __('Fifth Slider', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control Over Slider!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Sixth Slider section
         */
        $wp_customize->add_section('home_page_slider_6', array(
            'title' => __('Sixth Slider', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control Over Slider!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_page_slider_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page Feature area panel
         */
        $wp_customize->add_panel('home_feature_area_panel', array(
            'title' => __('Feature Area Settings', 'traffica'),
            'description' => __('Allows you to setup home page feature area section for Traffica Theme.', 'traffica'),
            'priority' => '6',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Home page main heading
         */
        $wp_customize->add_section('home_page_main_heading', array(
            'title' => __('Main Heading Setting', 'traffica'),
            'description' => __('Allows you to setup main heading for Traffica Theme.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Select Feature Column Structure
         */
        $wp_customize->add_section('home_page_column_structure', array(
            'title' => __('Feature Column Structure', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control Over Column Structure!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );




        /**
         * Home Page feature area 1
         */
        $wp_customize->add_section('home_feature_area_1', array(
            'title' => __('First Feature Area', 'traffica'),
            'description' => __('Allows you to setup first feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page feature area 2
         */
        $wp_customize->add_section('home_feature_area_2', array(
            'title' => __('Second Feature Area', 'traffica'),
            'description' => __('Allows you to setup second feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page feature area 3
         */
        $wp_customize->add_section('home_feature_area_3', array(
            'title' => __('Third Feature Area', 'traffica'),
            'description' => __('Allows you to setup third feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page feature area 4
         */
        $wp_customize->add_section('home_feature_area_4', array(
            'title' => __('Fourth Feature Area', 'traffica'),
            'description' => __('Allows you to setup fourth feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page Feature Box
         */
        $wp_customize->add_section('home_feature_box', array(
            'title' => __('Add Unlimited Feature Box', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control to add Unlimited Feature Box!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_feature_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page blog post Feature area panel
         */
        $wp_customize->add_panel('home_feature_blog_panel', array(
            'title' => __('Blog Area Settings', 'traffica'),
            'description' => __('Allows you to setup home page blog area section for Traffica Theme.', 'traffica'),
            'priority' => '7',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Home page blog heading
         */
        $wp_customize->add_section('home_page_blog_heading', array(
            'title' => __('Home Page Blog Heading Text', 'traffica'),
            'description' => __('Allows you to display the recent blog posts with images.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_feature_blog_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home page blog number
         */
        $wp_customize->add_section('home_page_blog_number', array(
            'title' => __('Home Page Number of Blogs', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control to add limited no of blog section of home page!</h3>", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'), //Descriptive tooltip
            'panel' => 'home_feature_blog_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );


        /**
         * Home page blog post On/Off
         */
        $wp_customize->add_section('home_page_blogpost_onoff', array(
            'title' => __('Home Page Blog post On/Off', 'traffica'),
            'description' => __('Allows you to Turn on or off the home page blog post as per your requirement.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_feature_blog_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page About Area Feature panel
         */
        $wp_customize->add_panel('home_about_area_panel', array(
            'title' => __('About Area Settings', 'traffica'),
            'description' => __('Allows you to setup home page About area section for Traffica Theme.', 'traffica'),
            'priority' => '8',
            'capability' => 'edit_theme_options'
        ));


        /**
         * Home page About post On/Off
         */
        $wp_customize->add_section('home_page_aboutpost_onoff', array(
            'title' => __('Home Page About post On/Off', 'traffica'),
            'description' => __('Allows you to Turn on or off the home page About post as per your requirement.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Testimonial Section On/Off
         */
        $wp_customize->add_section('home_page_testimonial_onoff', array(
            'title' => __('Home Page Testimonial On/Off', 'traffica'),
            'description' => __('Allows you to Turn on or off the Testimonial Section of About post as per your requirement.', 'traffica'), //Descriptive tooltip
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );


        /**
         * Home Page Bottom left Feature Start From Here
         */
        $wp_customize->add_section('home_feature_bootom_left', array(
            'title' => __('Bottom left Feature', 'traffica'),
            'description' => __('Allows you to setup bottom left feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page Accordian Feature Start From Here
         */
        $wp_customize->add_section('home_accordian_feature', array(
            'title' => __('Accordian Feature', 'traffica'),
            'description' => __('Allows you to setup Accordian feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Home Page Accordian Feature Settings Start From Here
         */
        $wp_customize->add_section('home_accordian_feature_settings', array(
            'title' => __('Accordian Feature Settings', 'traffica'),
            'description' => __('Allows you to setup Accordian feature area Settings for Traffica Theme.', 'traffica'),
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );
        /**
         * Home Page Testimonial Feature Start From Here
         */
        $wp_customize->add_section('home_testimonial_feature', array(
            'title' => __('Testimonial Feature Settings', 'traffica'),
            'description' => __('Allows you to setup Testimonial feature area section for Traffica Theme.', 'traffica'),
            'panel' => 'home_about_area_panel',
            'priority' => '',
            'capability' => 'edit_theme_options'
                )
        );

        /**
         * Add panel for Styling Options
         */
        $wp_customize->add_panel('styling_options', array(
            'title' => __('Styling Options', 'traffica'),
            'description' => __('Allows you to setup Styling Options for Traffica Theme.', 'traffica'),
            'priority' => '11',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Theme Stylesheet
         */
        $wp_customize->add_section('theme_stylesheet', array(
            'title' => __('Theme Stylesheet', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Theme Stylesheet!</h3>Allows you to Change Theme Color for Traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'priority' => '',
            'panel' => 'styling_options'
        ));

        /**
         * Theme Language
         */
        $wp_customize->add_section('theme_language', array(
            'title' => __('Theme Language', 'traffica'),
            'description' => __('Allows you to change the theme content display from left-right to right-left i.e. switching it to RTL.', 'traffica'),
            'priority' => '',
            'panel' => 'styling_options'
        ));
        /**
         * Social Icons
         */
        $wp_customize->add_section('traffica_social_icons', array(
            'title' => __('Social Icons', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Social Icon section!</h3>Allows you to setup Social Icon for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'priority' => '9',
            'panel' => ''
        ));

//        /**
//         * Footer Setting
//         */
//        $wp_customize->add_section('traffica_footer_text', array(
//            'title' => __('Bottom Footer Settings', 'traffica'),
//            'description' => __('Allows you to set footer text for Traffica Theme.', 'traffica'),
//            'priority' => '14',
//            'panel' => ''
//        ));



        /**
         * Contact Page Map
         */
        $wp_customize->add_section('contact_page_map', array(
            'title' => __('Contact Page Map', 'traffica'),
            'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over Contact Map Page!</h3>Allows you to setup Contact Map Page for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
            'priority' => '15',
            'panel' => ''
        ));

        /**
         * Add panel for SEO Options
         */
        $wp_customize->add_panel('seo_options_panel', array(
            'title' => __('SEO Options', 'traffica'),
            'description' => __('Allows you to setup SEO Options section for Traffica Theme.', 'traffica'),
            'priority' => '14',
            'capability' => 'edit_theme_options'
        ));

        /**
         * Meta Keywords (comma separated)
         */
        $wp_customize->add_section('meta_keyword', array(
            'title' => __('Meta Keywords (comma separated)', 'traffica'),
            'description' => __('Allows you to setupMeta Keywords (comma separated) for Traffica Theme.', 'traffica'),
            'priority' => '',
            'panel' => 'seo_options_panel'
        ));

        /**
         * Meta Description
         */
        $wp_customize->add_section('meta_desc', array(
            'title' => __('Meta Description', 'traffica'),
            'description' => __('Allows you to setup Meta Description for Traffica Theme.', 'traffica'),
            'priority' => '',
            'panel' => 'seo_options_panel'
        ));

        /**
         * Meta Author Name
         */
        $wp_customize->add_section('meta_author_name', array(
            'title' => __('Meta Author Name', 'traffica'),
            'description' => __('Allows you to setup Meta Author Name for Traffica Theme.', 'traffica'),
            'priority' => '',
            'panel' => 'seo_options_panel'
        ));
    }

    public static function traffica_Section_Content() {

        $section_content = array(
            'title_tagline' => array(
                'traffica_logo',
                'traffica_favicon'
            ),
            'background_image' => array(
                'traffica_bodybg'
            ),
            'call_feature_setting' => array(
                'traffica_contact_number'
            ),
            'tracking_code_setting' => array(
                'traffica_analytics'
            ),
            'mobile_navigation_menu' => array(
                'traffica_nav'
            ),
//            'dummy_data_setting' => array(
//                're_nm'
//            ),
            'home_page_slider_control' => array(
                'slider_select'
            ),
            'home_page_slider_speed' => array(
                'traffica_slide_speed'
            ),
            'home_page_slider_1' => array(
                'traffica_image1',
                'traffica_slider_heading1',
                'traffica_slider_des1',
                'traffica_slider_btnt1',
                'traffica_slider_btnlink1'
            ),
            'home_page_slider_2' => array(
                'traffica_image2',
                'traffica_slider_heading2',
                'traffica_slider_des2',
                'traffica_slider_btnt2',
                'traffica_slider_btnlink2'
            ),
            'home_page_slider_3' => array(
                'traffica_image3',
                'traffica_slider_heading3',
                'traffica_slider_des3',
                'traffica_slider_btnt3',
                'traffica_slider_btnlink3'
            ),
            'home_page_slider_4' => array(
                'traffica_image4',
                'traffica_slider_heading4',
                'traffica_slider_des4',
                'traffica_slider_btnt4',
                'traffica_slider_btnlink4'
            ),
            'home_page_slider_5' => array(
                'traffica_image5',
                'traffica_slider_heading5',
                'traffica_slider_des5',
                'traffica_slider_btnt5',
                'traffica_slider_btnlink5'
            ),
            'home_page_slider_6' => array(
                'traffica_image6',
                'traffica_slider_heading6',
                'traffica_slider_des6',
                'traffica_slider_btnt6',
                'traffica_slider_btnlink6'
            ),
            'home_page_main_heading' => array(
                'traffica_page_main_heading',
                'traffica_page_sub_heading'
            ),
            'home_page_column_structure' => array(
                'feature_col-struct'
            ),
            'home_feature_area_1' => array(
                'traffica_fimg1',
                'traffica_ficon1',
                'traffica_firsthead',
                'traffica_firstdesc',
                'traffica_feature_link1',
                'traffica_feature_back1'
            ),
            'home_feature_area_2' => array(
                'traffica_fimg2',
                'traffica_ficon2',
                'traffica_headline2',
                'traffica_seconddesc',
                'traffica_feature_link2',
                'traffica_feature_back2'
            ),
            'home_feature_area_3' => array(
                'traffica_fimg3',
                'traffica_ficon3',
                'traffica_headline3',
                'traffica_thirddesc',
                'traffica_feature_link3',
                'traffica_feature_back3'
            ),
            'home_feature_area_4' => array(
                'traffica_fimg4',
                'traffica_ficon4',
                'traffica_headline4',
                'traffica_fourthdesc',
                'traffica_feature_link4',
                'traffica_feature_back4'
            ),
            'home_feature_box' => array(
                'traffica_add_feature_box'
            ),
            'home_page_blog_heading' => array(
                'traffica_blog_heading'
            ),
            'home_page_blog_number' => array(
                'traffica_blog_number'
            ),
            'home_page_blogpost_onoff' => array(
                'traffica_home_post_content'
            ),
            'home_page_aboutpost_onoff' => array(
                'traffica_home_about_section'
            ),
            'home_page_testimonial_onoff' => array(
                'traffica_home_about_testimonial_section'
            ),
            'home_accordian_feature_settings' => array(
                'traffica_home_accordian_on_off',
                'traffica_accordian_selector'
            ),
            'home_feature_bootom_left' => array(
                'traffica_about_head',
                'traffica_about_desc'
            ),
            'home_accordian_feature' => array(
                'traffica_service_head',
                'traffica_acc_head1',
                'traffica_acc_desc1',
                'traffica_acc_head2',
                'traffica_acc_desc2',
                'traffica_acc_head3',
                'traffica_acc_desc3'
            ),
            'home_testimonial_feature' => array(
                'traffica_test_head',
                'traffica_test_image',
                'traffica_test_desc',
                'traffica_test_link_text',
                'traffica_test_link',
                'traffica_test_website'
            ),
            'traffica_social_icons' => array(
                'social_icon_1',
                'social_link_1',
                'social_icon_2',
                'social_link_2',
                'social_icon_3',
                'social_link_3',
                'social_icon_4',
                'social_link_4',
            ),
            'theme_stylesheet' => array(
                'traffica_altstylesheet'
            ),
            'theme_language' => array(
                'traffica_lanstylesheet'
            ),
//            'traffica_footer_text' => array(
//                'traffica_footertext',
//            ),
            'contact_page_map' => array(
                'traffica_contact_map'
            ),
            'meta_keyword' => array(
                'traffica_keyword'
            ),
            'meta_desc' => array(
                'traffica_description'
            ),
            'meta_author_name' => array(
                'traffica_author'
            ),
            'typography_section' => array(
                'typography_logo_family',
                'typography_nav_family'
            ),
            'widget_area_section' => array(
                'footer_widget_on_off',
                'footer_widget_area_select'
            ),
            'footer_section_copyright' => array(
                'traffica_footertext',
            ),
        );
        return $section_content;
    }

    public static function traffica_Settings() {

        //Front page on/off
        $file_rename = array("on" => "On", "off" => "Off");
        $home_page_blog_content = array("on" => "On", "off" => "Off");
        $home_page_about_section = array("on" => "On", "off" => "Off");
        $home_page_about_testimonial_section = array("on" => "On", "off" => "Off");
        $home_page_Accordin_section = array("on" => "On", "off" => "Off");
        $home_slider_select = array('normal' => 'Normal Slider', 'layered' => 'Layered Slider');
        $feature_col_struct = array('3' => '3-Column Structure', '4' => '4-column structure');

        $social_icons = array(
            'facebook' => '&#xf09a;',
            'facebook-official' => '&#xf230;',
            'facebook-square' => '&#xf082;',
            'twitter' => '&#xf099;',
            'twitter-square' => '&#xf081;',
            'google' => '&#xf1a0;',
            'google-plus' => '&#xf0d5;',
            'google-plus-official' => '&#xf2b3;',
            'google-plus-square' => '&#xf0d4;',
            'linkedin' => '&#xf0e1;',
            'linkedin-square' => '&#xf08c;',
            'pinterest' => '&#xf0d2;',
            'pinterest-p' => '&#xf231;',
            'pinterest-square' => '&#xf0d3;',
            'behance' => '&#xf1b4;',
            'behance-square' => '&#xf1b5;',
            'tumblr' => '&#xf173;',
            'tumblr-square' => '&#xf174;',
            'reddit' => '&#xf1a1;',
            'reddit-alien' => '&#xf281;',
            'reddit-square' => '&#xf1a2;',
            'dribbble' => '&#xf17d;',
            'vk' => '&#xf189;',
            'skype' => '&#xf17e;',
            'film' => '&#xf008;',
            'youtube-play' => '&#xf16a;',
            'youtube' => '&#xf167;',
            'youtube-square' => '&#xf166;',
            'vimeo-square' => '&#xf194;',
            'soundcloud' => '&#xf1be;',
            'instagram' => '&#xf16d;',
            'flickr' => '&#xf16e;',
            'rss' => '&#xf09e;',
            'rss-square' => '&#xf143;',
            'heart' => '&#xf004;',
            'heart-o' => '&#xf08a;',
            'github' => '&#xf09b;',
            'github-alt' => '&#xf113;',
            'github-square' => '&#xf092;',
            'stack-overflow' => '&#xf16c;',
            'qq' => '&#xf1d6;',
            'weibo' => '&#xf18a;',
            'weixin' => '&#xf1d7;',
            'xing' => '&#xf168;',
            'xing-square' => '&#xf169;',
            'gamepad' => '&#xf11b;',
            'medium' => '&#xf23a;',
            'envelope' => '&#xf0e0;',
            'envelope-o' => '&#xf003;',
            'envelope-square ' => '&#xf199;',
            'cc-paypal' => '&#xf1f4;',
            'credit-card' => '&#xf09d;',
        );


        // Typography Array
        $font_family = array('ABeeZee', 'Abel', 'Abril Fatface', 'Aclonica', 'Acme', 'Actor', 'Adamina', 'Advent Pro', 'Aguafina Script', 'Akronim', 'Aladin', 'Aldrich', 'Alef', 'Alegreya', 'Alegreya SC', 'Alex Brush', 'Alfa Slab One', 'Alice', 'Alike', 'Alike Angular', 'Allan', 'Allerta', 'Allerta Stencil', 'Allura', 'Almendra', 'Almendra Display', 'Almendra SC', 'Amarante', 'Amaranth', 'Amatic SC', 'Amethysta', 'Anaheim', 'Andada', 'Andika', 'Angkor', 'Annie Use Your Telescope', 'Anonymous Pro', 'Antic', 'Antic Didone', 'Antic Slab', 'Anton', 'Arapey', 'Arbutus', 'Arbutus Slab', 'Architects Daughter', 'Archivo Black', 'Archivo Narrow', 'Arial Black', 'Arial Narrow', 'Arimo', 'Arizonia', 'Armata', 'Artifika', 'Arvo', 'Asap', 'Asset', 'Astloch', 'Asul', 'Atomic Age', 'Aubrey', 'Audiowide', 'Autour One', 'Average', 'Average Sans', 'Averia Gruesa Libre', 'Averia Libre', 'Averia Sans Libre', 'Averia Serif Libre', 'Bad Script', 'Balthazar', 'Bangers', 'Basic', 'Battambang', 'Baumans', 'Bayon', 'Belgrano', 'Bell MT', 'Bell MT Alt', 'Belleza', 'BenchNine', 'Bentham', 'Berkshire Swash', 'Bevan', 'Bigelow Rules', 'Bigshot One', 'Bilbo', 'Bilbo Swash Caps', 'Bitter', 'Black Ops One', 'Bodoni', 'Bokor', 'Bonbon', 'Boogaloo', 'Bowlby One', 'Bowlby One SC', 'Brawler', 'Bree Serif', 'Bubblegum Sans', 'Bubbler One', 'Buenard', 'Butcherman', 'Butcherman Caps', 'Butterfly Kids', 'Cabin', 'Cabin Condensed', 'Cabin Sketch', 'Caesar Dressing', 'Cagliostro', 'Calibri', 'Calligraffitti', 'Cambo', 'Cambria', 'Candal', 'Cantarell', 'Cantata One', 'Cantora One', 'Capriola', 'Cardo', 'Carme', 'Carrois Gothic', 'Carrois Gothic SC', 'Carter One', 'Caudex', 'Cedarville Cursive', 'Ceviche One', 'Changa One', 'Chango', 'Chau Philomene One', 'Chela One', 'Chelsea Market', 'Chenla', 'Cherry Cream Soda', 'Cherry Swash', 'Chewy', 'Chicle', 'Chivo', 'Cinzel', 'Cinzel Decorative', 'Clara', 'Clicker Script', 'Coda', 'Codystar', 'Combo', 'Comfortaa', 'Coming Soon', 'Concert One', 'Condiment', 'Consolas', 'Content', 'Contrail One', 'Convergence', 'Cookie', 'Copse', 'Corben', 'Corsiva', 'Courgette', 'Courier New', 'Cousine', 'Coustard', 'Covered By Your Grace', 'Crafty Girls', 'Creepster', 'Creepster Caps', 'Crete Round', 'Crimson Text', 'Croissant One', 'Crushed', 'Cuprum', 'Cutive', 'Cutive Mono', 'Damion', 'Dancing Script', 'Dangrek', 'Dawning of a New Day', 'Days One', 'Delius', 'Delius Swash Caps', 'Delius Unicase', 'Della Respira', 'Denk One', 'Devonshire', 'Dhyana', 'Didact Gothic', 'Diplomata', 'Diplomata SC', 'Domine', 'Donegal One', 'Doppio One', 'Dorsa', 'Dosis', 'Dr Sugiyama', 'Droid Arabic Kufi', 'Droid Arabic Naskh', 'Droid Sans', 'Droid Sans Mono', 'Droid Sans TV', 'Droid Serif', 'Duru Sans', 'Dynalight', 'EB Garamond', 'Eagle Lake', 'Eater', 'Eater Caps', 'Economica', 'Electrolize', 'Elsie', 'Elsie Swash Caps', 'Emblema One', 'Emilys Candy', 'Engagement', 'Englebert', 'Enriqueta', 'Erica One', 'Esteban', 'Euphoria Script', 'Ewert', 'Exo', 'Expletus Sans', 'Fanwood Text', 'Fascinate', 'Fascinate Inline', 'Faster One', 'Fasthand', 'Fauna One', 'Federant', 'Federo', 'Felipa', 'Fenix', 'Finger Paint', 'Fjalla One', 'Fjord One', 'Flamenco', 'Flavors', 'Fondamento', 'Fontdiner Swanky', 'Forum', 'Francois One', 'Freckle Face', 'Fredericka the Great', 'Fredoka One', 'Freehand', 'Fresca', 'Frijole', 'Fruktur', 'Fugaz One', 'GFS Didot', 'GFS Neohellenic', 'Gabriela', 'Gafata', 'Galdeano', 'Galindo', 'Garamond', 'Gentium Basic', 'Gentium Book Basic', 'Geo', 'Geostar', 'Geostar Fill', 'Germania One', 'Gilda Display', 'Give You Glory', 'Glass Antiqua', 'Glegoo', 'Gloria Hallelujah', 'Goblin One', 'Gochi Hand', 'Gorditas', 'Goudy Bookletter 1911', 'Graduate', 'Grand Hotel', 'Gravitas One', 'Great Vibes', 'Griffy', 'Gruppo', 'Gudea', 'Habibi', 'Hammersmith One', 'Hanalei', 'Hanalei Fill', 'Handlee', 'Hanuman', 'Happy Monkey', 'Headland One', 'Helvetica Neue', 'Henny Penny', 'Herr Von Muellerhoff', 'Holtwood One SC', 'Homemade Apple', 'Homenaje', 'IM Fell DW Pica', 'IM Fell DW Pica SC', 'IM Fell Double Pica', 'IM Fell Double Pica SC', 'IM Fell English', 'IM Fell English SC', 'IM Fell French Canon', 'IM Fell French Canon SC', 'IM Fell Great Primer', 'IM Fell Great Primer SC', 'Iceberg', 'Iceland', 'Imprima', 'Inconsolata', 'Inder', 'Indie Flower', 'Inika', 'Irish Grover', 'Irish Growler', 'Istok Web', 'Italiana', 'Italianno', 'Jacques Francois', 'Jacques Francois Shadow', 'Jim Nightshade', 'Jockey One', 'Jolly Lodger', 'Josefin Sans', 'Josefin Sans Std Light', 'Josefin Slab', 'Joti One', 'Judson', 'Julee', 'Julius Sans One', 'Junge', 'Jura', 'Just Another Hand', 'Just Me Again Down Here', 'Kameron', 'Karla', 'Kaushan Script', 'Kavoon', 'Keania One', 'Kelly Slab', 'Kenia', 'Khmer', 'Kite One', 'Knewave', 'Kotta One', 'Koulen', 'Kranky', 'Kreon', 'Kristi', 'Krona One', 'La Belle Aurore', 'Lancelot', 'Lateef', 'Lato', 'League Script', 'Leckerli One', 'Ledger', 'Lekton', 'Lemon', 'Lemon One', 'Libre Baskerville', 'Life Savers', 'Lilita One', 'Lily Script One', 'Limelight', 'Linden Hill', 'Lobster', 'Lobster Two', 'Lohit Bengali', 'Lohit Devanagari', 'Lohit Tamil', 'Londrina Outline', 'Londrina Shadow', 'Londrina Sketch', 'Londrina Solid', 'Lora', 'Love Ya Like A Sister', 'Loved by the King', 'Lovers Quarrel', 'Luckiest Guy', 'Lusitana', 'Lustria', 'Macondo', 'Macondo Swash Caps', 'Magra', 'Maiden Orange', 'Mako', 'Marcellus', 'Marcellus SC', 'Marck Script', 'Margarine', 'Marko One', 'Marmelad', 'Marvel', 'Mate', 'Mate SC', 'Maven Pro', 'McLaren', 'Meddon', 'MedievalSharp', 'Medula One', 'Megrim', 'Meie Script', 'Merienda', 'Merienda One', 'Merriweather', 'Merriweather Sans', 'Metal', 'Metal Mania', 'Metamorphous', 'Metrophobic', 'Michroma', 'Milonga', 'Miltonian', 'Miltonian Tattoo', 'Miniver', 'Miss Fajardose', 'Miss Saint Delafield', 'Modern Antiqua', 'Molengo', 'Monda', 'Monofett', 'Monoton', 'Monsieur La Doulaise', 'Montaga', 'Montez', 'Montserrat', 'Montserrat Alternates', 'Montserrat Subrayada', 'Moul', 'Moulpali', 'Mountains of Christmas', 'Mouse Memoirs', 'Mr Bedford', 'Mr Bedfort', 'Mr Dafoe', 'Mr De Haviland', 'Mrs Saint Delafield', 'Mrs Sheppards', 'Muli', 'Mystery Quest', 'Neucha', 'Neuton', 'New Rocker', 'News Cycle', 'Niconne', 'Nixie One', 'Nobile', 'Nokora', 'Norican', 'Nosifer', 'Nosifer Caps', 'Nothing You Could Do', 'Noticia Text', 'Noto Sans', 'Noto Sans UI', 'Noto Serif', 'Nova Cut', 'Nova Flat', 'Nova Mono', 'Nova Oval', 'Nova Round', 'Nova Script', 'Nova Slim', 'Nova Square', 'Numans', 'Nunito', 'OFL Sorts Mill Goudy TT', 'Odor Mean Chey', 'Offside', 'Old Standard TT', 'Oldenburg', 'Oleo Script', 'Oleo Script Swash Caps', 'Open Sans', 'Oranienbaum', 'Orbitron', 'Oregano', 'Orienta', 'Original Surfer', 'Oswald', 'Over the Rainbow', 'Overlock', 'Overlock SC', 'Ovo', 'Oxygen', 'Oxygen Mono', 'PT Mono', 'PT Sans', 'PT Sans Caption', 'PT Sans Narrow', 'PT Serif', 'PT Serif Caption', 'Pacifico', 'Paprika', 'Parisienne', 'Passero One', 'Passion One', 'Pathway Gothic One', 'Patrick Hand', 'Patrick Hand SC', 'Patua One', 'Paytone One', 'Peralta', 'Permanent Marker', 'Petit Formal Script', 'Petrona', 'Philosopher', 'Piedra', 'Pinyon Script', 'Pirata One', 'Plaster', 'Play', 'Playball', 'Playfair Display', 'Playfair Display SC', 'Podkova', 'Poiret One', 'Poller One', 'Poly', 'Pompiere', 'Pontano Sans', 'Port Lligat Sans', 'Port Lligat Slab', 'Prata', 'Preahvihear', 'Press Start 2P', 'Princess Sofia', 'Prociono', 'Prosto One', 'Proxima Nova', 'Proxima Nova Tabular Figures', 'Puritan', 'Purple Purse', 'Quando', 'Quantico', 'Quattrocento', 'Quattrocento Sans', 'Questrial', 'Quicksand', 'Quintessential', 'Qwigley', 'Racing Sans One', 'Radley', 'Raleway', 'Raleway Dots', 'Rambla', 'Rammetto One', 'Ranchers', 'Rancho', 'Rationale', 'Redressed', 'Reenie Beanie', 'Revalia', 'Ribeye', 'Ribeye Marrow', 'Righteous', 'Risque', 'Roboto', 'Roboto Condensed', 'Roboto Slab', 'Rochester', 'Rock Salt', 'Rokkitt', 'Romanesco', 'Ropa Sans', 'Rosario', 'Rosarivo', 'Rouge Script', 'Ruda', 'Rufina', 'Ruge Boogie', 'Ruluko', 'Rum Raisin', 'Ruslan Display', 'Russo One', 'Ruthie', 'Rye', 'Sacramento', 'Sail', 'Salsa', 'Sanchez', 'Sancreek', 'Sansita One', 'Sans Serif', 'Sarina', 'Satisfy', 'Scada', 'Scheherazade', 'Schoolbell', 'Seaweed Script', 'Sevillana', 'Seymour One', 'Shadows Into Light', 'Shadows Into Light Two', 'Shanti', 'Share', 'Share Tech', 'Share Tech Mono', 'Shojumaru', 'Short Stack', 'Siamreap', 'Siemreap', 'Sigmar One', 'Signika', 'Signika Negative', 'Simonetta', 'Sintony', 'Sirin Stencil', 'Six Caps', 'Skranji', 'Slackey', 'Smokum', 'Smythe', 'Snippet', 'Snowburst One', 'Sofadi One', 'Sofia', 'Sonsie One', 'Sorts Mill Goudy', 'Source Code Pro', 'Source Sans Pro', 'Special Elite', 'Spicy Rice', 'Spinnaker', 'Spirax', 'Squada One', 'Stalemate', 'Stalin One', 'Stalinist One', 'Stardos Stencil', 'Stint Ultra Condensed', 'Stint Ultra Expanded', 'Stoke', 'Strait', 'Sue Ellen Francisco', 'Sunshiney', 'Supermercado One', 'Suwannaphum', 'Swanky and Moo Moo', 'Syncopate', 'Tahoma', 'Tangerine', 'Taprom', 'Tauri', 'Telex', 'Tenor Sans', 'Terminal Dosis', 'Terminal Dosis Light', 'Text Me One', 'Thabit', 'The Girl Next Door', 'Tienne', 'Tinos', 'Titan One', 'Titillium Web', 'Trade Winds', 'Trocchi', 'Trochut', 'Trykker', 'Tulpen One', 'Ubuntu', 'Ubuntu Condensed', 'Ubuntu Mono', 'Ultra', 'Uncial Antiqua', 'Underdog', 'Unica One', 'UnifrakturMaguntia', 'Unkempt', 'Unlock', 'Unna', 'VT323', 'Vampiro One', 'Varela', 'Varela Round', 'Vast Shadow', 'Vibur', 'Vidaloka', 'Viga', 'Voces', 'Volkhov', 'Vollkorn', 'Voltaire', 'Waiting for the Sunrise', 'Wallpoet', 'Walter Turncoat', 'Warnes', 'Wellfleet', 'Wendy One', 'Wire One', 'Yanone Kaffeesatz', 'Yellowtail', 'Yeseva One', 'Yesteryear', 'Zeyada', 'jsMath cmbx10', 'jsMath cmex10', 'jsMath cmmi10', 'jsMath cmr10', 'jsMath cmsy10', 'jsMath cmti10', 'Maracellus');

// Background Defaults
        $background_defaults = array('color' => '', 'image' => '', 'repeat' => 'repeat', 'position' => 'top center', 'attachment' => 'scroll');
//Stylesheet Reader
        $alt_stylesheets = array("orange" => "orange", "green" => "green", "purple" => "purple", "teal-green" => "teal-green", "blue" => "blue", "yellow" => "yellow", "red" => "red", "grey" => "grey");
        $lan_stylesheets = array("Default" => "Default", "rtl" => "rtl");
// Pull all the categories into an array
        $options_categories = array();
        $options_categories_obj = get_categories();
        foreach ($options_categories_obj as $category) {
            $options_categories[$category->cat_ID] = $category->cat_name;
        }

// Populate OptionsFramework option in array for use in theme
        $contact_option = array("on" => "On", "off" => "Off");
        $captcha_option = array("on" => "On", "off" => "Off");
// Pull all the pages into an array
        $options_pages = array();
        $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
        $options_pages[''] = 'Select a page:';
        foreach ($options_pages_obj as $page) {
            $options_pages[$page->ID] = $page->post_title;
        }
        // If using image radio buttons, define a directory path
        $imagepath = get_template_directory_uri() . '/images/';



        $theme_settings = array(
            'traffica_logo' => array(
                'id' => 'traffica_options[traffica_logo]',
                'label' => __('Traffica Logo', 'traffica'),
                'description' => __('Upload a logo for your Website. The recommended size for the logo is 200px width x 50px height.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_favicon' => array(
                'id' => 'traffica_options[traffica_favicon]',
                'label' => __('Custom Favicon', 'traffica'),
                'description' => __('Here you can upload a Favicon for your Website. Specified size is 16px x 16px.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_contact_number' => array(
                'id' => 'traffica_options[traffica_contact_number]',
                'label' => __('Tap To Call Feature', 'traffica'),
                'description' => __('Mention your contact number here through which users can interact to you directly. This feature is called tap to call and this will work when the user will access your website through mobile phones or iPhone.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_analytics' => array(
                'id' => 'traffica_options[traffica_analytics]',
                'label' => __('Tracking Code', 'traffica'),
                'description' => __('Paste your Google Analytics (or other) tracking code here.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_nav' => array(
                'id' => 'traffica_options[traffica_nav]',
                'label' => __("Enter your mobile navigation menu text", 'traffica'),
                'description' => __('Enter your mobile navigation menu text', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_bodybg' => array(
                'id' => 'traffica_options[traffica_bodybg]',
                'label' => __('Background Image', 'traffica'),
                'description' => __('Choose a suitable background image that will complement your website.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
//            're_nm' => array(
//                'id' => 'traffica_options[re_nm]',
//                'label' => __('Front Page On/Off', 'traffica'),
//                'description' => __('If the front page option is active then home page will appear otherwise blog page will display.', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'radio',
//                'default' => 'on',
//                'choices' => $file_rename
//            ),
            'slider_select' => array(
                'id' => 'traffica_options[slider_select]',
                'label' => __('Home Page Slider Types', 'traffica'),
                'description' => __('Select the type of slider you want to display on the home page.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $home_slider_select
            ),
            'traffica_slide_speed' => array(
                'id' => 'traffica_options[traffica_slide_speed]',
                'label' => __('Slider Speed', 'traffica'),
                'description' => __('Set the speed of the slider in milliseconds. For e.g. if you want to set the speed of the slider for 8 seconds then enter 8000 in the field or if you want to set the slider speed for 2.5 seconds then enter 2500 in the field.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_image1' => array(
                'id' => 'traffica_options[traffica_image1]',
                'label' => __('First Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page First Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading1' => array(
                'id' => 'traffica_options[traffica_slider_heading1]',
                'label' => __('First Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page First slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des1' => array(
                'id' => 'traffica_options[traffica_slider_des1]',
                'label' => __('First Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page First Slides Show', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt1' => array(
                'id' => 'traffica_options[traffica_slider_btnt1]',
                'label' => __('First Slider Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page First Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink1' => array(
                'id' => 'traffica_options[traffica_slider_btnlink1]',
                'label' => __('First Slider Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page First Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_image2' => array(
                'id' => 'traffica_options[traffica_image2]',
                'label' => __('Second Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page Second Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading2' => array(
                'id' => 'traffica_options[traffica_slider_heading2]',
                'label' => __('Second Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page Second slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des2' => array(
                'id' => 'traffica_options[traffica_slider_des2]',
                'label' => __('Seocnd Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page Second Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt2' => array(
                'id' => 'traffica_options[traffica_slider_btnt2]',
                'label' => __('Second Slider Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page Second Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink2' => array(
                'id' => 'traffica_options[traffica_slider_btnlink2]',
                'label' => __('Second Slider Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page Second Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_image3' => array(
                'id' => 'traffica_options[traffica_image3]',
                'label' => __('Third Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page Third Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading3' => array(
                'id' => 'traffica_options[traffica_slider_heading3]',
                'label' => __('Third Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page Third slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des3' => array(
                'id' => 'traffica_options[traffica_slider_des3]',
                'label' => __('Third Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page Third Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt3' => array(
                'id' => 'traffica_options[traffica_slider_btnt3]',
                'label' => __('Third Slider Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page Third Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink3' => array(
                'id' => 'traffica_options[traffica_slider_btnlink3]',
                'label' => __('Third Slider Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page Third Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_image4' => array(
                'id' => 'traffica_options[traffica_image4]',
                'label' => __('Fourth Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page Fourth Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading4' => array(
                'id' => 'traffica_options[traffica_slider_heading4]',
                'label' => __('Fourth Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page Fourth slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des4' => array(
                'id' => 'traffica_options[traffica_slider_des4]',
                'label' => __('Fourth Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page Fourth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt4' => array(
                'id' => 'traffica_options[traffica_slider_btnt4]',
                'label' => __('Fourth Slider Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page Fourth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink4' => array(
                'id' => 'traffica_options[traffica_slider_btnlink4]',
                'label' => __('Fourth Slider Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page Fourth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_image5' => array(
                'id' => 'traffica_options[traffica_image5]',
                'label' => __('Fifth Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page Fifth Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading5' => array(
                'id' => 'traffica_options[traffica_slider_heading5]',
                'label' => __('Fifth Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page Fifth slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des5' => array(
                'id' => 'traffica_options[traffica_slider_des5]',
                'label' => __('Fifth Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page Fifth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt5' => array(
                'id' => 'traffica_options[traffica_slider_btnt5]',
                'label' => __('Fifth Slider Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page Fifth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink5' => array(
                'id' => 'traffica_options[traffica_slider_btnlink5]',
                'label' => __('Fifth Slider Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page First Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_image6' => array(
                'id' => 'traffica_options[traffica_image6]',
                'label' => __('Sixth Slider Image', 'traffica'),
                'description' => __('Choose Image for your Home page Sixth Slider. Optimal Size: 1171px x 526px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_slider_heading6' => array(
                'id' => 'traffica_options[traffica_slider_heading6]',
                'label' => __('Sixth Slider Heading', 'traffica'),
                'description' => __('Enter the Heading for Home page Sixth slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_des6' => array(
                'id' => 'traffica_options[traffica_slider_des6]',
                'label' => __('Sixth Slider Description', 'traffica'),
                'description' => __('Enter the Description for Home Page Sixth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_slider_btnt6' => array(
                'id' => 'traffica_options[traffica_slider_btnt6]',
                'label' => __('Fourth Slide Button Text', 'traffica'),
                'description' => __('Enter the Button Text for Home Page Sixth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_slider_btnlink6' => array(
                'id' => 'traffica_options[traffica_slider_btnlink6]',
                'label' => __('Fourth Slide Button Link', 'traffica'),
                'description' => __('Enter the Button Link URL for Home Page Sixth Slider', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_page_main_heading' => array(
                'id' => 'traffica_options[traffica_page_main_heading]',
                'label' => __('Home Page Main Heading', 'traffica'),
                'description' => __('Enter your heading text for home page', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_page_sub_heading' => array(
                'id' => 'traffica_options[traffica_page_sub_heading]',
                'label' => __('Home Page Sub Heading', 'traffica'),
                'description' => __('Enter your sub heading text for home page', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'feature_col-struct' => array(
                'id' => 'traffica_options[feature_col-struct]',
                'label' => __('Home Page feature column structure', 'traffica'),
                'description' => __('Choose whether you want 3-column structure or 4-column structure for feature box.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $feature_col_struct
            ),
            'traffica_fimg1' => array(
                'id' => 'traffica_options[traffica_fimg1]',
                'label' => __('First Feature Image', 'traffica'),
                'description' => __('Choose image for your feature column first. Optimal size 198px x 115px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_ficon1' => array(
                'id' => 'traffica_options[traffica_ficon1]',
                'label' => __('First Feature Icon', 'traffica'),
                'description' => __("Choose icon for your first Feature area. Go to <a href='http://fortawesome.github.io/Font-Awesome/'>Font Awesome</a>. Search Any Icon. Say you want to search an icon for TV.You will find code like &lt;i class=fa fa-television&gt; &lt;/i>. You need to copy that code and paste it here.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_firsthead' => array(
                'id' => 'traffica_options[traffica_firsthead]',
                'label' => __('First Feature Heading', 'traffica'),
                'description' => __('Enter text for first feature area heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_firstdesc' => array(
                'id' => 'traffica_options[traffica_firstdesc]',
                'label' => __('First Feature Description', 'traffica'),
                'description' => __('Enter text for first feature area description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_feature_link1' => array(
                'id' => 'traffica_options[traffica_feature_link1]',
                'label' => __('First feature Link', 'traffica'),
                'description' => __('Enter your text for First feature Link..', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'traffica_feature_back1' => array(
                'id' => 'traffica_options[traffica_feature_back1]',
                'label' => __('First feature Back Text', 'traffica'),
                'description' => __('Enter your text for First feature back portion.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_fimg2' => array(
                'id' => 'traffica_options[traffica_fimg2]',
                'label' => __('Second Feature Image', 'traffica'),
                'description' => __('Choose image for your feature column second. Optimal size 198px x 115px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_ficon2' => array(
                'id' => 'traffica_options[traffica_ficon2]',
                'label' => __('First Feature Icon', 'traffica'),
                'description' => __("Choose icon for your second Feature area. Go to <a href='http://fortawesome.github.io/Font-Awesome/'>Font Awesome</a>. Search Any Icon. Say you want to search an icon for TV.You will find code like &lt;i class=fa fa-television&gt; &lt;/i>. You need to copy that code and paste it here.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_headline2' => array(
                'id' => 'traffica_options[traffica_headline2]',
                'label' => __('Second Feature Heading', 'traffica'),
                'description' => __('Enter text for second feature area heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_seconddesc' => array(
                'id' => 'traffica_options[traffica_seconddesc]',
                'label' => __('Second Feature Description', 'traffica'),
                'description' => __('Enter text for second feature area description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_feature_link2' => array(
                'id' => 'traffica_options[traffica_feature_link2]',
                'label' => __('Second feature Link', 'traffica'),
                'description' => __('Enter link url for second feature area.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'traffica_feature_back2' => array(
                'id' => 'traffica_options[traffica_feature_back2]',
                'label' => __('Second feature Back Text', 'traffica'),
                'description' => __('Enter your text for Second feature back portion.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_fimg3' => array(
                'id' => 'traffica_options[traffica_fimg3]',
                'label' => __('Third Feature Image', 'traffica'),
                'description' => __('Choose image for your feature column third. Optimal size 198px x 115px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_ficon3' => array(
                'id' => 'traffica_options[traffica_ficon3]',
                'label' => __('Third Feature Icon', 'traffica'),
                'description' => __("Choose icon for your third Feature area. Go to <a href='http://fortawesome.github.io/Font-Awesome/'>Font Awesome</a>. Search Any Icon. Say you want to search an icon for TV.You will find code like &lt;i class=fa fa-television&gt; &lt;/i>. You need to copy that code and paste it here.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_headline3' => array(
                'id' => 'traffica_options[traffica_headline3]',
                'label' => __('Third Feature Heading', 'traffica'),
                'description' => __('Enter text for third feature area heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_thirddesc' => array(
                'id' => 'traffica_options[traffica_thirddesc]',
                'label' => __('Third Feature Description', 'traffica'),
                'description' => __('Enter text for third feature area description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_feature_link3' => array(
                'id' => 'traffica_options[traffica_feature_link3]',
                'label' => __('Third feature Link', 'traffica'),
                'description' => __('Enter link url for third feature area.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'traffica_feature_back3' => array(
                'id' => 'traffica_options[traffica_feature_back3]',
                'label' => __('Third feature Back Text', 'traffica'),
                'description' => __('Enter your text for Third feature back portion.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_fimg4' => array(
                'id' => 'traffica_options[traffica_fimg4]',
                'label' => __('Fourth Feature Image', 'traffica'),
                'description' => __('Choose image for your feature column fourth. Optimal size 198px x 115px', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_ficon4' => array(
                'id' => 'traffica_options[traffica_ficon4]',
                'label' => __('Fourth Feature Icon', 'traffica'),
                'description' => __("Choose icon for your fourth Feature area. Go to <a href='http://fortawesome.github.io/Font-Awesome/'>Font Awesome</a>. Search Any Icon. Say you want to search an icon for TV.You will find code like &lt;i class=fa fa-television&gt; &lt;/i>. You need to copy that code and paste it here.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_headline4' => array(
                'id' => 'traffica_options[traffica_headline4]',
                'label' => __('Fourth Feature Heading', 'traffica'),
                'description' => __('Enter text for fourth feature area heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_fourthdesc' => array(
                'id' => 'traffica_options[traffica_fourthdesc]',
                'label' => __('Fourth Feature Description', 'traffica'),
                'description' => __('Enter text for fourth feature area description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_feature_link4' => array(
                'id' => 'traffica_options[traffica_feature_link4]',
                'label' => __('Fourth feature Link', 'traffica'),
                'description' => __('Enter link url for fourth feature area.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'traffica_feature_back4' => array(
                'id' => 'traffica_options[traffica_feature_back4]',
                'label' => __('Fourth feature Back Text', 'traffica'),
                'description' => __('Enter your text for Fourth feature back portion.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_add_feature_box' => array(
                'id' => 'traffica_options[traffica_add_feature_box]',
                'label' => __('Add Unlimited Feature Box', 'traffica'),
                'description' => __("Enter your feature box shortcode to add another feature box. Ex: [add_feature_box featureimg = 'PATH TO THE IMAGE' featureicon = 'ICON CLASS ex: fa-television' featuretitle = 'TITLE OF YOUR FEATURE' featuredesc = 'DESC OF YOUR FEATURE' featurelink = 'LINK OF YOUR FEATURE' featureback = 'BACK TEXT OF YOUR FEATURE']. You can also add multiple shortcodes separated by ',' (comma) to generate more feature box.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => '',
                'choices' => ''
            ),
            'traffica_blog_heading' => array(
                'id' => 'traffica_options[traffica_blog_heading]',
                'label' => __('Home Page Main Heading', 'traffica'),
                'description' => __('Enter your heading text for home page', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_blog_number' => array(
                'id' => 'traffica_options[traffica_blog_number]',
                'label' => __('Enter Number of Blogs', 'traffica'),
                'description' => __('Enter the number of blogs you want to display for home page', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_home_post_content' => array(
                'id' => 'traffica_options[traffica_home_post_content]',
                'label' => __('Home Page Blog post On/Off', 'traffica'),
                'description' => __('Turn on or off the home page blog post as per your requirement', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $home_page_blog_content
            ),
            'traffica_home_about_section' => array(
                'id' => 'traffica_options[traffica_home_about_section]',
                'label' => __('Home Page About Section On/Off', 'traffica'),
                'description' => __('Turn on or off the home page about section as per your requirement.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $home_page_about_section
            ),
            'traffica_home_about_testimonial_section' => array(
                'id' => 'traffica_options[traffica_home_about_testimonial_section]',
                'label' => __('Home Page About Testimonial Section On/Off', 'traffica'),
                'description' => __('Turn on or off the home page about Testimonial section as per your requirement.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $home_page_Accordin_section
            ),
            'traffica_home_accordian_on_off' => array(
                'id' => 'traffica_options[traffica_home_accordian_on_off]',
                'label' => __('Home Page About Accordin Section On/Off', 'traffica'),
                'description' => __('Turn on or off the home page about Accordin section as per your requirement.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $home_page_about_testimonial_section
            ),
            'traffica_accordian_selector' => array(
                'id' => 'traffica_options[traffica_accordian_selector]',
                'label' => __('Accordian Area Selector', 'traffica'),
                'description' => __('Select the number of Feature Areas to be displayed.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => '1',
                'choices' => array(
                    '1' => 'Display One Accordian Area',
                    '2' => 'Display Two Accordian Area',
                    '3' => 'Display Three Accordian Area',
                )
            ),
            'traffica_about_head' => array(
                'id' => 'traffica_options[traffica_about_head]',
                'label' => __('Bottom left Feature Heading', 'traffica'),
                'description' => __('Enter your heading for Bottom left Feature', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_about_desc' => array(
                'id' => 'traffica_options[traffica_about_desc]',
                'label' => __('Bottom left Feature Description', 'traffica'),
                'description' => __('Enter your text for Bottom left Feature description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_service_head' => array(
                'id' => 'traffica_options[traffica_service_head]',
                'label' => __('Home Page Accordian Feature Main Heading', 'traffica'),
                'description' => __('Enter your heading for Home Page Accordian Feature Main.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_head1' => array(
                'id' => 'traffica_options[traffica_acc_head1]',
                'label' => __('First Accordian Heading', 'traffica'),
                'description' => __('Enter your text for First Accordian Heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_desc1' => array(
                'id' => 'traffica_options[traffica_acc_desc1]',
                'label' => __('First Accordian Description', 'traffica'),
                'description' => __('Enter your text for First Accordian Description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_head2' => array(
                'id' => 'traffica_options[traffica_acc_head2]',
                'label' => __('Second Accordian Heading', 'traffica'),
                'description' => __('Enter your text for Second Accordian Heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_desc2' => array(
                'id' => 'traffica_options[traffica_acc_desc2]',
                'label' => __('Second Accordian Description', 'traffica'),
                'description' => __('Enter your text for Second Accordian Description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_head3' => array(
                'id' => 'traffica_options[traffica_acc_head3]',
                'label' => __('Third Accordian Heading', 'traffica'),
                'description' => __('Enter your text for Third Accordian Heading.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_acc_desc3' => array(
                'id' => 'traffica_options[traffica_acc_desc3]',
                'label' => __('Third Accordian Description', 'traffica'),
                'description' => __('Enter your text for Third Accordian Description.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_test_head' => array(
                'id' => 'traffica_options[traffica_test_head]',
                'label' => __('Testimonial Heading', 'traffica'),
                'description' => __('Mention the text for Testimonial heading that will say something about your business to the users.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_test_image' => array(
                'id' => 'traffica_options[traffica_test_image]',
                'label' => __('Testimonial Image', 'traffica'),
                'description' => __('Here you can upload the image for your business. Optimal size is 74px x 72px.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'image',
                'default' => ''
            ),
            'traffica_test_desc' => array(
                'id' => 'traffica_options[traffica_test_desc]',
                'label' => __('Testimonial Description', 'traffica'),
                'description' => __('Here you can describe about your business in short so that the users will understand the importance of your services.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_test_link_text' => array(
                'id' => 'traffica_options[traffica_test_link_text]',
                'label' => __('Testimonial Link Text', 'traffica'),
                'description' => __('Here you can mention the name of the person giving the Testimonial.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'text',
                'default' => ''
            ),
            'traffica_test_link' => array(
                'id' => 'traffica_options[traffica_test_link]',
                'label' => __('Testimonial Link', 'traffica'),
                'description' => __('Here you can link up your name to anywhere you want.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_test_website' => array(
                'id' => 'traffica_options[traffica_test_website]',
                'label' => __('Testimonial Website Name', 'traffica'),
                'description' => __('Here you can mention the Website of the person giving the Testimonial.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => ''
            ),
            'traffica_altstylesheet' => array(
                'id' => 'traffica_options[traffica_altstylesheet]',
                'label' => __('Theme Stylesheet', 'traffica'),
                'description' => __('Select the color of the theme from different available colors.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'choices' => $alt_stylesheets,
                'default' => ''
            ),
            'traffica_lanstylesheet' => array(
                'id' => 'traffica_options[traffica_lanstylesheet]',
                'label' => __('Theme Language', 'traffica'),
                'description' => __('By default the theme content displays from left to right which you can change to right to left i.e. switching it to RTL.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'choices' => $lan_stylesheets,
                'default' => ''
            ),
//            'traffica_footertext' => array(
//                'id' => 'traffica_options[traffica_footertext]',
//                'label' => __('Bottom Footer Text', 'traffica'),
////              'description' => __('Write the text here that will be displayed on the footer i.e. at the bottom of the Website.', 'traffica'),
//                'description' => __('The areas that you would like to configure is called Footer Widget areas that you can configure from Dashboard->Appearance->Widget.<br/> All you need to do is just place your desired Widgets under the Footer Widget Areas (First, Second, Third, etc.) they will appear on the footer of your site. <br/> If you want to hide the footer widget contents then, just place a Blank Text Widget under Footer Widget Areas. <br/> You can also refer this tutorial link: <a href="https://www.traffica.com/how-to-use-widgets-in-wordpress/">https://www.traffica.com/how-to-use-widgets-in-wordpress/</a>', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'textarea',
//                'default' => ''
//            ),
            'social_icon_1' => array(
                'id' => 'traffica_options[social_icon_1]',
                'label' => __('Social Icon 1', 'traffica'),
                'description' => __('Choose the Social Icon from below Dropdown List.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => 'facebook',
                'choices' => $social_icons
            ),
            'social_link_1' => array(
                'id' => 'traffica_options[social_link_1]',
                'label' => __('Social Link 1', 'traffica'),
                'description' => __('Quickly add link to your Social Link.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'social_icon_2' => array(
                'id' => 'traffica_options[social_icon_2]',
                'label' => __('Social Icon 2', 'traffica'),
                'description' => __('Choose the Social Icon from below Dropdown List.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => 'twitter',
                'choices' => $social_icons
            ),
            'social_link_2' => array(
                'id' => 'traffica_options[social_link_2]',
                'label' => __('Social Link 2', 'traffica'),
                'description' => __('Quickly add link to your Social Link.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'social_icon_3' => array(
                'id' => 'traffica_options[social_icon_3]',
                'label' => __('Social Icon 3', 'traffica'),
                'description' => __('Choose the Social Icon from below Dropdown List.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => 'google-plus',
                'choices' => $social_icons
            ),
            'social_link_3' => array(
                'id' => 'traffica_options[social_link_3]',
                'label' => __('Social Link 3', 'traffica'),
                'description' => __('Quickly add link to your Social Link.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
            'social_icon_4' => array(
                'id' => 'traffica_options[social_icon_4]',
                'label' => __('Social Icon 4', 'traffica'),
                'description' => __('Choose the Social Icon from below Dropdown List.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => 'pinterest',
                'choices' => $social_icons
            ),
            'social_link_4' => array(
                'id' => 'traffica_options[social_link_4]',
                'label' => __('Social Link 4', 'traffica'),
                'description' => __('Quickly add link to your Social Link.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'link',
                'default' => '#'
            ),
//
//            'traffica_pinterest' => array(
//                'id' => 'traffica_options[traffica_pinterest]',
//                'label' => __('Pinterest URL', 'traffica'),
//                'description' => __('Quickly add your Pinterest URL code to your theme by writing the code in this block.', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//
//            'traffica_youtube' => array(
//                'id' => 'traffica_options[traffica_youtube]',
//                'label' => __('Youtube URL', 'traffica'),
//                'description' => __('Quickly add your Youtube URL code to your theme by writing the code in this block.', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//
//            'traffica_linkedin' => array(
//                'id' => 'traffica_options[traffica_linkedin]',
//                'label' => __('Linked In URL', 'traffica'),
//                'description' => __('Enter your Linkedin URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//            'traffica_stumbleupon' => array(
//                'id' => 'traffica_options[traffica_stumbleupon]',
//                'label' => __('Stumble Upon URL', 'traffica'),
//                'description' => __('Enter your Stumble Upon URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//            'traffica_digg' => array(
//                'id' => 'traffica_options[traffica_digg]',
//                'label' => __('Digg URL', 'traffica'),
//                'description' => __('Enter your Stumble Upon URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//             'traffica_flickr' => array(
//                'id' => 'traffica_options[traffica_flickr]',
//                'label' => __('Flickr URL', 'traffica'),
//                'description' => __('Enter your Custom Flickr URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//
//            'traffica_instagram' => array(
//                'id' => 'traffica_options[traffica_instagram]',
//                'label' => __('Instagram URL', 'traffica'),
//                'description' => __('Enter your Custom Instagram URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
//            'traffica_tumblr' => array(
//                'id' => 'traffica_options[traffica_tumblr]',
//                'label' => __('Tumblr URL', 'traffica'),
//                'description' => __('Enter your Custom Tumblr URL if you have one', 'traffica'),
//                'type' => 'option',
//                'setting_type' => 'link',
//                'default' => '#'
//            ),
            'traffica_contact_map' => array(
                'id' => 'traffica_options[traffica_contact_map]',
                'label' => __('Contact Page Map', 'traffica'),
                'description' => __("Go to https://maps.google.com/ and generate the map for your location. Just copy only the iframe code i.e. code within &lt;iframe&gt; and &lt;/iframe&gt; and paste it here. This will show the map location of your business on the Website.", 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_keyword' => array(
                'id' => 'traffica_options[traffica_keyword]',
                'label' => __('Meta Keywords (comma separated)', 'traffica'),
                'description' => __('Meta keywords provide search engines with additional information about topics that appear on your site. This only applies to your home page. Keyword Limit Maximum 8.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_description' => array(
                'id' => 'traffica_options[traffica_description]',
                'label' => __('Meta Description', 'traffica'),
                'description' => __('You should use meta descriptions to provide search engines with additional information about topics that appear on your site. This only applies to your home page.Optimal Length for Search Engines, Roughly 155 Characters.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'traffica_author' => array(
                'id' => 'traffica_options[traffica_author]',
                'label' => __('Meta Author Name', 'traffica'),
                'description' => __('You should write the full name of the author here. This only applies to your home page.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
            'typography_logo_family' => array(
                'id' => 'traffica_options[typography_logo_family]',
                'label' => __('Logo Font Family', 'traffica'),
                'description' => __('Allows you to set Font Family For Logo.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => '',
                'choices' => $font_family
            ),
            'typography_nav_family' => array(
                'id' => 'traffica_options[typography_nav_family]',
                'label' => __('Body Font Family', 'traffica'),
                'description' => __('Allows you to set Font Family For Body.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'select',
                'default' => '',
                'choices' => $font_family
            ),
            'footer_widget_on_off' => array(
                'id' => 'traffica_options[footer_widget_on_off]',
                'label' => __('Footer Widget On/Off', 'traffica'),
                'description' => __('Check on for enabling or disabling Footer Widget Area.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => 'on',
                'choices' => $file_rename
            ),
            'footer_widget_area_select' => array(
                'id' => 'traffica_options[footer_widget_area_select]',
                'label' => __('Widget Area Selector', 'traffica'),
                'description' => __('Select the number of Widget Areas to be displayed.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'radio',
                'default' => '2',
                'choices' => array(
                    '1' => 'Display One widget Area',
                    '2' => 'Display Two widget Area',
                    '3' => 'Display Three widget Area',
                    '4' => 'Display Four widget Area'
                )
            ),
            'traffica_footertext' => array(
                'id' => 'traffica_options[traffica_footertext]',
                'label' => __('Footer Text', 'traffica'),
                'description' => __('Write the text here that will be displayed on the footer i.e. at the bottom of the Website.', 'traffica'),
                'type' => 'option',
                'setting_type' => 'textarea',
                'default' => ''
            ),
        );
        return $theme_settings;
    }

    public static function traffica_Controls($wp_customize) {


        $sections = self::traffica_Section_Content();
        $settings = self::traffica_Settings();

        /*
         * Begin: Calling Selective refresh function for each settings.
         */
        foreach ($settings as $k => $v) {
            $wp_customize->selective_refresh->add_partial($v['id'], array(
                'selector' => '.' . $k,
            ));
        }
        /*
         * End: Calling Selective refresh function for each settings.
         */

        foreach ($sections as $section_id => $section_content) {

            foreach ($section_content as $section_content_id) {

                switch ($settings[$section_content_id]['setting_type']) {
                    case 'image':
                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_url');
                        $wp_customize->add_control(new WP_Customize_Image_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id']
                                )
                        ));
                        break;

                    case 'text':
                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_text');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'text'
                                )
                        ));
                        break;

                    case 'textarea':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_textarea');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'textarea'
                                )
                        ));
                        break;

                    case 'link':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_url');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'text'
                                )
                        ));
                        break;

                    case 'color':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_color');

                        $wp_customize->add_control(new WP_Customize_Color_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id']
                                )
                        ));
                        break;

                    case 'number':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_number');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'text'
                                )
                        ));
                        break;

                    case 'select':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_select');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'select',
                            'choices' => $settings[$section_content_id]['choices']
                                )
                        ));
                        break;

                    case 'radio':

                        self::add_setting($wp_customize, $settings[$section_content_id]['id'], $settings[$section_content_id]['default'], $settings[$section_content_id]['type'], 'traffica_sanitize_radio');

                        $wp_customize->add_control(new WP_Customize_Control(
                                $wp_customize, $settings[$section_content_id]['id'], array(
                            'label' => $settings[$section_content_id]['label'],
                            'description' => $settings[$section_content_id]['description'],
                            'section' => $section_id,
                            'settings' => $settings[$section_content_id]['id'],
                            'type' => 'radio',
                            'choices' => $settings[$section_content_id]['choices']
                                )
                        ));
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public static function add_setting($wp_customize, $setting_id, $default, $type, $sanitize_callback) {
        $wp_customize->add_setting($setting_id, array(
            'default' => $default,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => array('traffica_Customizer', $sanitize_callback),
            'type' => $type
                )
        );
    }

    /**
     * adds sanitization callback funtion : textarea
     * @package traffica
     */
    public static function traffica_sanitize_textarea($value) {
        $value = esc_html($value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : url
     * @package traffica
     */
    public static function traffica_sanitize_url($value) {
        $value = esc_url($value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : text
     * @package traffica
     */
    public static function traffica_sanitize_text($value) {
        $value = sanitize_text_field($value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : email
     * @package traffica
     */
    public static function traffica_sanitize_email($value) {
        $value = sanitize_email($value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : number
     * @package traffica
     */
    public static function traffica_sanitize_number($value) {
        $value = preg_replace("/[^0-9+ ]/", "", $value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : number
     * @package traffica
     */
    public static function traffica_sanitize_color($value) {
        $value = sanitize_hex_color($value);
        return $value;
    }

    /**
     * adds sanitization callback funtion : select
     * @package traffica
     */
    public static function traffica_sanitize_select($value, $setting) {
        global $wp_customize;
        $control = $wp_customize->get_control($setting->id);
        if (array_key_exists($value, $control->choices)) {
            return $value;
        } else {
            return $setting->default;
        }
    }

    /**
     * adds sanitization callback funtion : radio
     * @package traffica
     */
    public static function traffica_sanitize_radio($value, $setting) {
        global $wp_customize;
        $control = $wp_customize->get_control($setting->id);
        if (array_key_exists($value, $control->choices)) {
            return $value;
        } else {
            return $setting->default;
        }
    }

}

// Setup the Theme Customizer settings and controls...
add_action('customize_register', array('traffica_Customizer', 'traffica_Register'));

function inkthemes_registers() {
    wp_register_script('inkthemes_jquery_ui', '//code.jquery.com/ui/1.11.0/jquery-ui.js', array("jquery"), true);
    wp_register_script('inkthemes_customizer_script1', get_template_directory_uri() . '/assets/js/inkthemes_customizer.js', array("jquery", "inkthemes_jquery_ui"), true);
    wp_enqueue_script('inkthemes_customizer_script1');
    wp_localize_script('inkthemes_customizer_script1', 'ink_advert', array(
        'pro' => __('View PRO version', 'traffica'),
        'url' => esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),
        'support_text' => __('Need Help!', 'traffica'),
        'support_url' => esc_url('https://www.inkthemes.com/contact-us/')
            )
    );
}

add_action('customize_controls_enqueue_scripts', 'inkthemes_registers');

/**
 * Enqueue script for custom customize control.
 */
function theme_slug_custom_customize_enqueue() {
    wp_enqueue_style('customizer-css', TRAFFICA_DIR_URI . 'assets/css/customizer.css');
}

add_action('customize_controls_enqueue_scripts', 'theme_slug_custom_customize_enqueue');

function traffica_customize_register($wp_customize) {
    
    
        class WP_Customize_Range_Control extends WP_Customize_Control {

        public $type = 'custom_range';

        public function enqueue() {
            wp_enqueue_script('inkthemes-range-control', get_template_directory_uri() . '/functions/js/range-control.js', array('jquery'));
        }

        public function render_content() {
            ?>
            <label>
                <?php if (!empty($this->label)) : ?>
                    <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                <?php endif; ?>
                <div class="cs-range-value"></div>
                <input data-input-type="range" type="range" <?php $this->input_attrs(); ?> value="<?php echo esc_attr($this->value()); ?>" <?php $this->link(); ?> step="0.1" min="1.0" max="5.0"/>
                <?php if (!empty($this->description)) : ?>
                    <span class="description customize-control-description"><?php echo esc_html($this->description); ?></span>
                <?php endif; ?>
            </label>
            <?php
        }

    }
    
    

    //blog name and description
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
// Add the selective part


    $wp_customize->selective_refresh->add_partial('blogname', array(
        'selector' => '.site-title a',
        'render_callback' => 'traffica_customize_partial_blogname',
    ));
    $wp_customize->selective_refresh->add_partial('blogdescription', array(
        'selector' => '.site-description',
        'render_callback' => 'traffica_customize_partial_blogdescription',
    ));

    //     blog layout and page layout
    // Load the radio image control class.
    require_once( trailingslashit(get_template_directory()) . 'includes/load.php' );
    // get_template_part('includes/load.php');
    // Register the radio image control class as a JS control type.
    $wp_customize->register_control_type('traffica_Customize_Control_Radio_Image');

    // Add a custom layout section.
    $wp_customize->add_section('Blog_and_Page_layout', array('title' => esc_html__('Blog & page Layout', 'traffica'), 'priority' => '2', 'description' => __("<h3>Check out the <a href='https://www.inkthemes.com/market/yoga-studio-wordpress-theme/' '%s' target='_blank'>PRO version </a>for full control over layout Options!</h3> Allows you to set layout Options for traffica Theme.", 'traffica'), esc_url('https://www.inkthemes.com/market/yoga-studio-wordpress-theme/'),));

    // Add the layout setting.
    $wp_customize->add_setting(
            'blog-layout', array(
        'type' => 'option',
        'default' => 'content-sidebar',
        'sanitize_callback' => 'sanitize_key',
            )
    );
    $wp_customize->add_setting(
            'page-layout', array(
        'type' => 'option',
        'default' => 'content-sidebar',
        'sanitize_callback' => 'sanitize_key',
            )
    );
    // Add the layout control.
    $wp_customize->add_control(
            new traffica_Customize_Control_Radio_Image(
            $wp_customize, 'blog-layout', array(
        'label' => esc_html__('Blog Sidebar Layout', 'traffica'),
        'section' => 'Blog_and_Page_layout',
        'choices' => array(
            'content-sidebar' => array(
                'label' => esc_html__('Left Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/left-sidebar.jpg'
            ),
            'sidebar-content' => array(
                'label' => esc_html__('Right Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/right-sidebar.jpg'
            ),
            'content' => array(
                'label' => esc_html__('Center Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/center.jpg'
            )
        )
            )
            )
    );
    // Add the layout control.
    $wp_customize->add_control(
            new traffica_Customize_Control_Radio_Image(
            $wp_customize, 'page-layout', array(
        'label' => esc_html__('Page Sidebar Layout', 'traffica'),
        'section' => 'Blog_and_Page_layout',
        'choices' => array(
            'content-sidebar' => array(
                'label' => esc_html__('Left Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/left-sidebar2.jpg'
            ),
            'sidebar-content' => array(
                'label' => esc_html__('Right Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/right-sidebar2.jpg'
            ),
            'content' => array(
                'label' => esc_html__('Center Layout', 'traffica'),
                'url' => get_template_directory_uri() . '/assets/images/center2.jpg'
            )
        )
            )
            )
    );
    
    
    

    $wp_customize->add_setting('header_v_pad', array(
        'default' => '200',
        'type' => 'option',
        'sanitize_callback' => 'inkthemes_sanitize_floating_value',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage'
            )
    );

    // Do stuff with $wp_customize, the WP_Customize_Manager object.
    $wp_customize->add_control(new WP_Customize_Range_Control($wp_customize, 'header_v_option', array(
        'label' => __('Header Vertical Padding', 'traffica'),
        'section' => 'header_layout_section',
        'settings' => 'header_v_pad',
        'description' => __('Measurement is in pixel.', 'traffica'),
        'priority' => '3',
        'input_attrs' => array(
            'min' => 0,
            'max' => 299.999,
        ),
            )
            )
    );

   
    
    
        /**
     * adds sanitization callback funtion : radio
     * @package inkthemes
     */
    function inkthemes_sanitize_floating_value($value) {
        $value = preg_replace("/[^0-9+ ]/", "", $value);
        $temp = substr($value, 1);
        if ($temp != '') {
            $value = $value / 10;
        }
        return $value;
    }
}

add_action('customize_register', 'traffica_customize_register');

//function traffica_panels_js() {
//
//           wp_enqueue_style('fontawesome', TRAFFICA_DIR_URI . "assets/css/font-awesome.css", '', '', 'all');
//}
//add_action( 'customize_controls_enqueue_scripts', 'traffica_panels_js' );



