<?php

/**
 * Lite Manager
 *
 * @package traffica
 */
/**
 * About page class
 */
require_once get_template_directory() . '/cw-notifications/cw-about-page/class-inkthemes-about-page.php';

$func = 'general_admin_notice';
/*
 * About page instance
 */
$config = array(
    // Menu name under Appearance.
    'menu_name' => apply_filters('traffica_about_page_filter', __('About Traffica', 'traffica'), 'menu_name'),
    // Page title.
    'page_name' => apply_filters('traffica_about_page_filter', __('About Traffica', 'traffica'), 'page_name'),
    // Main welcome title
    /* translators: s - theme name */
    'welcome_title' => apply_filters('traffica_about_page_filter', sprintf(__('Welcome to %s !', 'traffica'), 'Traffica'), 'welcome_title'),
    // Main welcome content
        'welcome_content' => apply_filters('traffica_about_page_filter', sprintf(__('Traffica is ultimate WordPress theme with lots of customization features and options. The theme can be used by all kind of businesses and it is also perfect for personal use.', 'traffica'), 'welcome_content')),
    /**
     * Tabs array.
     *
     * The key needs to be ONLY consisted from letters and underscores. If we want to define outside the class a function to render the tab,
     * the will be the name of the function which will be used to render the tab content.
     */
    'tabs' => array(
        'getting_started' => __('Advanced (Colorway)', 'traffica'),
        'getting_started_theme' => __('Getting Started (Traffica)', 'traffica'),
        'support' => __('Support', 'traffica'),
        'changelog' => __('Changelog', 'traffica'),
        'free_pro' => __('Free vs Paid', 'traffica'),
    ),
    // Support content tab.
    'support_content' => array(
        'first' => array(
            'title' => esc_html__('Contact Support', 'traffica'),
//			'icon'         => 'dashicons dashicons-sos',
            'text' => esc_html__('Our support staff is always dedicated to hold your website up and running without any glitch. If you need any kind help while creating a website with Traffica you can contact us via our conatct form.', 'traffica'),
            'button_label' => esc_html__('Contact Us', 'traffica'),
            'button_link' => esc_url('https://www.inkthemes.com/contact-us/'),
            'is_button' => true,
            'is_new_tab' => true,
        ),
        'second' => array(
            'title' => esc_html__('Support', 'traffica'),
//			'icon'         => 'dashicons dashicons-book-alt',
            'text' => esc_html__('Checkout our support forum for more help.', 'traffica'),
            'button_label' => esc_html__('Support Forum', 'traffica'),
            'button_link' => 'https://wordpress.org/support/theme/traffica',
            'is_button' => false,
            'is_new_tab' => true,
        ),
        'third' => array(
            'title' => esc_html__('Changelog', 'traffica'),
//			'icon'         => 'dashicons dashicons-portfolio',
            'text' => esc_html__('We keep a track and manitain all the records of our enhanced features or latest versions of theme in the Changelog. You can find all those changes and updated anytime in our Changelog.', 'traffica'),
            'button_label' => esc_html__('Changelog', 'traffica'),
            'button_link' => esc_url(admin_url('themes.php?page=traffica-welcome&tab=changelog&show=yes')),
            'is_button' => false,
            'is_new_tab' => false,
        ),
    ),
        // Getting started tab
    'getting_started' => array(
        'first' => array(
           // 'title' => esc_html__('', 'traffica'),
            'text' => $func()
        ),
    ),
    
    // Getting started theme tab
    'getting_started_theme' => array(
        'first' => array(
            'title' => esc_html__('Traffica Theme - Full Documentation', 'traffica'),
            'text' => esc_html__('Read full documentation of Traffica lite WordPress Theme. In case any issue, you can go to the community forum of InkThemes and get instant solution to your queries.', 'traffica'),
            'button_label' => esc_html__('View  Traffica lite Documentation', 'traffica'),
            'button_link' => 'https://www.inkthemes.com/how-to-use-traffica-wordpress-theme-on-your-website/',
            'is_button' => false,
            'recommended_actions' => false,
            'is_new_tab' => true,
        ),
        'second' => array(
            'title' => esc_html__('Upgrade to Traffica Wordpress Theme[Pro] ', 'traffica'),
            'text' => esc_html__('Make a move to Traffica pro. Get Advance CSS, Mobile friendliness and more when you switch from basic to advance', 'traffica'),
            'button_label' => esc_html__('View Pro Features', 'traffica'),
            'button_link' => 'https://www.inkthemes.com/market/yoga-studio-wordpress-theme/',
            'is_button' => false,
            'recommended_actions' => true,
            'is_new_tab' => false,
        ),
    ),
    // Free vs PRO array.
    'free_pro' => array(
        'free_theme_name' => 'Traffica',
        'pro_theme_name' => 'Traffica Pro',
        'pro_theme_link' => 'https://www.inkthemes.com/market/yoga-studio-wordpress-theme/',
        /* translators: s - theme name */
        'get_pro_theme_label' => sprintf(__('Get %s now!', 'traffica'), 'Traffica Pro'),
        'banner_link' => 'https://www.inkthemes.com/doc/traffica-wordpress-theme-documentation/',
        'banner_src' => get_template_directory_uri() . '/assets/images/feature-image.png',
        'features' => array(
            array(
                'title' => __('Translation Ready', 'traffica'),
                'description' => __('The theme is compatible with WPML plugin and you can display the contents in your desired language.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Live Customizer', 'traffica'),
                'description' => __('Visible Editing Shortcuts For HomePage Elements. Now, edit the content directly by clicking the edit icon(pencil) on homepage elements.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Mobile friendly', 'traffica'),
                'description' => __('Responsive layout. Works on every device.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Background Image', 'traffica'),
                'description' => __('You can use any background image you want.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
//            array(
//                'title' => __('Contact Form With Google reCaptcha', 'traffica'),
//                'description' => __('Get Leads directly to your Inbox.', 'traffica'),
//                'is_in_lite' => 'true',
//                'is_in_pro' => 'true',
//            ),
            array(
                'title' => __('Social Icons', 'traffica'),
                'description' => __('Add Social Icons for your Business website.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Home Page Blog Post Box On/Off', 'traffica'),
                'description' => __('You can Enable or Disable the Home page Blog Post Section.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Footer Widget Section On/Off', 'traffica'),
                'description' => __('You can Enable or Disable the Footer Section.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Home Page Slider Two Types', 'traffica'),
                'description' => __('Change Normal and Layered Slider Types As per Your Choice', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Home Page Slider Speed Control', 'traffica'),
                'description' => __('Change Speed of Slider As Required', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Six Slider In Home page', 'traffica'),
                'description' => __('Change Slider Settings through Customizer.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Home Page Feature Column Structure', 'traffica'),
                'description' => __('Choose whether you want 3-column structure or 4-column structure for feature box.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Home Page Display No Of Blogs', 'traffica'),
                'description' => __('Change the number of blogs you want to display for home page.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Footer Widget Area Column Option', 'traffica'),
                'description' => __('Change Footer Settings through Customizer.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Page and Blog Sidebar Layout', 'traffica'),
                'description' => __('Change Layout of Blog and Page through Customizer in three Different Ways.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Typography', 'traffica'),
                'description' => __('You can use any font family as you want.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Contact Map Page', 'traffica'),
                'description' => __('Setup Contact Map Page for Your Theme.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Add Unlimited Feature Box In Home Page', 'traffica'),
                'description' => __('Add Feature Box As Much As You Want.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Copyright Footer Text', 'traffica'),
                'description' => __('Change your Copyright Footer Text for your business website.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Styling Options', 'traffica'),
                'description' => __('Design your website with more colors and styles.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('Google Analytics Tracking Code', 'traffica'),
                'description' => __('Analyze and track the number of visitors on your website with Google Analytics.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('SEO Optimized', 'traffica'),
                'description' => __('Get a completely SEO optimized website for your business.', 'traffica'),
                'is_in_lite' => 'false',
                'is_in_pro' => 'true',
            ),
            array(
                'title' => __('24*7 Support', 'traffica'),
                'description' => __('Get instant solution to all your queries with our 24*7 support.', 'traffica'),
                'is_in_lite' => 'true',
                'is_in_pro' => 'true',
            ),
        ),
    ),
);
inkthemes_About_Page::init(apply_filters('traffica_about_page_array', $config));

function general_admin_notice() {
    $content = '<div class="warning-message">
       <img src="' . get_template_directory_uri() . '/assets/images/information-icon.png"/>
    <p>Switch to Colorway WordPress Theme which is packed with 35+ ready-made elementor templates & offers better customization features and website development elements. <a href="https://wordpress.org/themes/colorway/" target="_blank">Download Colorway</a> for Free !!</p>
</div>';
    $content .= '<div id="colorway-sites-on-active" data-repeat-notice-after="">
                    <div class="notice-container">
                        <div class="colorway-sites-wrap">
                        <div class="block1-container">
                        <div class="top-container">
                            <div class="notice-image">
                            <img src="' . get_template_directory_uri() . '/assets/images/thumb33.png" class="custom-logo" alt="Traffica">
                            </div> 
                            
                        <div class="notice-content">
                        <div class="colorway-sites-block">
                        <h4>Colorway - Advanced Elementor Based WordPress Theme</h4>
                        <p>Create stunning websites in a minute by using 35+ elementor based site templates and the Drag & Drop builder.</p>
                        </div>
	<div class="colorway-review-notice-container">';

    //Check if Colorway Theme Exists or not        
    $get_themes = array();
    $get_themes = wp_get_themes();
    
if (array_key_exists("colorway", $get_themes)) {
        $content .= '<a href="'.admin_url('theme-install.php?search=colorway').'" class="button button-primary button-hero">Try Colorway For Free</a>';
    } else {
        $content .= '<a href="'.admin_url('theme-install.php?search=colorway').'" class="button button-primary button-hero">Try to Colorway For Free</a>';
    }
    $content .= '<a target="_blank" href="' . esc_url(admin_url("customize.php")) . '" class="button cwy-sites-btn">Go to the Customizer</a></div>
       </div></div></div><div class="cw-sites-video">
 <div class="cwy-video-section embed-container">
<iframe width="100%" height="315px" src="https://www.youtube.com/embed/WLrfR4-HEfo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>                </div>
            
</div>
        </div>
        </div>
	</div>';
    return $content;
}
