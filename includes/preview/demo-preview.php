<?php
// Preview Check
function traffica_is_preview() {
	$theme = wp_get_theme();
	$theme_name = $theme->get( 'TextDomain' );
	$active_theme = traffica_get_raw_option( 'template' ); 
        //return apply_filters( 'traffica_is_preview', ( $active_theme == strtolower( $theme_name )));
        return apply_filters( 'traffica_is_preview', ( $active_theme != strtolower( $theme_name ) && !is_child_theme()));
}
// Get Raw Options
function traffica_get_raw_option( $opt_name ) {
	$alloptions = wp_cache_get( 'alloptions', 'options' );
	$alloptions = maybe_unserialize( $alloptions );
	return isset( $alloptions[ $opt_name ] ) ? maybe_unserialize( $alloptions[ $opt_name ] ) : false;
}

// Random Images
function traffica_get_preview_img_src( $i = 0 ) {
	// prevent infinite loop
	if ( 10 == $i ) {
		return '';
	}

	$path = get_template_directory() . '/assets/images/';

	// Build or re-build the global dem img array
	if ( ! isset( $GLOBALS['traffica_preview_images'] ) || empty( $GLOBALS['traffica_preview_images'] ) ) {
		$imgs = array( 'Blog-01.jpg', 'Blog-02.jpg', 'Blog-03.jpg', 'Blog-04.jpg', 'Blog-05.jpg', 'Blog-06.jpg', 'Blog-07.jpg', 'Blog-08.jpg', 'Blog-09.jpg', 'Blog-10.jpg' );
		$candidates = array();
		foreach ( $imgs as $img ) {
			$candidates[] = $img;
		}
		$GLOBALS['traffica_preview_images'] = $candidates;
	}
	$candidates = $GLOBALS['traffica_preview_images'];
	// get a random image name
	$rand_key = array_rand( $candidates );
	$img_name = $candidates[$rand_key];

	// if file does not exists, reset the global and recursively call it again
	if ( ! file_exists( $path . $img_name ) ) {
		unset( $GLOBALS['traffica_preview_images'] );
		$i++;
		return traffica_get_preview_img_src( $i );
	}

	// unset all sizes of the img found and update the global
	$new_candidates = $candidates;
	foreach ( $candidates as $_key => $_img ) {
		if ( substr( $_img, 0, strlen( "{$img_name}" ) ) === "{$img_name}" ) {
			unset($new_candidates[$_key]);
		}
	}
	$GLOBALS['traffica_preview_images'] = $new_candidates;
	return get_template_directory_uri() . '/assets/images/' . $img_name;
}

/**
 * Filter thumbnail image
 *
 * @param string $input Post thumbnail.
 */
function traffica_preview_thumbnail( $input ) {  
	if ( empty( $input ) && traffica_is_preview() ) {           
		$placeholder = traffica_get_preview_img_src();                    
		return '<img src="' . esc_url( $placeholder ) . '" class="aligncenter sb-thumb-img">';
	}
	return $input;
}
add_filter( 'post_thumbnail_html', 'traffica_preview_thumbnail' );
