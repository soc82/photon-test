“Traffica Theme" Documentation by InkThemes

Get Your Website Ready in Just 1 Click.

Thank you for purchasing the Traffica WordPress Theme. The theme is very simple and easy to use. If you have any questions that are beyond the scope of this readme file, please feel free to ask questions at InkThemes Support Forum. Follow the link given below:

http://www.inkthemes.com/community/

More about the theme usage:

1. Begining (Important)

A) Installing the theme

To be able to use the Traffica Wordpress Theme, you need to install WordPress on your server. If you don't know how to do it, then click on the link below that will guide you how to install WordPress.
 
http://www.inkthemes.com/12-simple-steps-to-install-wordpress-locally/01/

To install the Traffica WordPress theme, just place the theme folder “Trafficatheme”  in the themes directory under wp-content and activate it from the WordPress admin. As soon as you do that, the theme will be activated on your Website.

2. Creating Pages on Website

You would need to build new pages from "Add Page" menu and select the appropriate template for each page. For e.g.-

A) For “Blog Page” choose the “Blog Template” from Page Attributes section. Below the link is mentioned to set up a Blog Page in Traffica WordPress theme. Howevever, the process will remain the same for other themes also.

http://www.youtube.com/watch?v=C53zw1oIZCI

B) For “Fullwidth Page” choose the “Fullwidth Template” from Page Attributes section.

http://www.inkthemes.com/community/threads/full-width-by-default.2585/

C) For “Contact Page” choose the “Contact Us Template” from Page Attributes section. Follow the link given below that will guide you how to make a Contact Page.

http://www.youtube.com/watch?v=OIuy7dTQYM4

D) For “Gallery Page” choose the either “Default Template” or "Fullwidth Tempalte" from Page Attributes section. 

3. Configuring Homepage

A) Uploading Logo & Favicon
For uploading logo and favicon,
Dashboard->Appearance->Theme options->General Settings and upload the logo and favicon into the appropriate fields.

B) Sliders on Homepage

Go to Dashboard->InkThemes->Slider Settings 

(a) Choose whether you want to use normal slider or layered slider. The image size for normal slider is 444px X 238px and for layered slider is 1583px X 644px

(b) Upload the images for the Slider. You can display eight slider images. By default atleast 1 slide appear on the Home Page.

(C) Feature Box on Home Page

Go to Dashboard->InkThemes->Homepage Feature Area

(a) Select Feature column structure whether you want a 3-column structure or 4-column structure.

(b) By default there is an option of adding 4 feature boxes.

(c) You can add more feature boxes according to your need by writing shortcodes separated by comma.

Shortcode is [add_feature_box featureimg = 'PATH TO THE IMAGE' featureicon = 'ICON CLASS ex: fa-television' featuretitle = 'TITLE OF YOUR FEATURE' featuredesc = 'DESC OF YOUR FEATURE' featurelink = 'LINK OF YOUR FEATURE' featureback = 'BACK TEXT OF YOUR FEATURE']

(D) Blog Section on Home Page

Go to Dashboard->InkThemes->Homepage Bottom Feature

You can choose whether you want to display blogs on home page or not. 

4. Building Menus

Menu with Menu Manager
Traffica has a prebuilt feature of displaying all the pages and subpages in the menu. However, you can also build a custom menu using the "Menus" option under the “Appearance” Section.

5. Full Widht Home Page Feature

The Traffica theme has unique feature of full width slider in which the user can show any desired size of image. Moreover the blog post can be shown on the home page.

Once again, thank you so much for trying the Traffica WordPress Theme. As we said at the beginning, we will be glad to help you. If you have any questions related to this theme then do let us know & we will try our best to assist. If you have any general questions related to the themes on InkThemes, we would recommend you to visit the InkThemes Support Forum and ask your queries there.