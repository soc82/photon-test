<?php
/*
  Template Name: Fullwidth Page
 */
get_header();
?>
<div class="page_heading_container">
    <span class="crumb_shadow"></span>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_heading_content row">
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="col-md-6">
                        <?php traffica_breadcrumbs(); ?>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content">
                <div class="fullwidth">
                    <?php if (have_posts()) : the_post(); ?>
                        <?php
                        the_content();
                        wp_link_pages(array(
                            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'traffica') . '</span>',
                            'after' => '</div>',
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'traffica') . ' </span>%',
                            'separator' => '<span class="screen-reader-text">, </span>',
                        ));
                        ?>	
                    <?php endif; ?>	  
                </div> 
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>