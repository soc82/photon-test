<div class="sidebar">
    <?php if (!dynamic_sidebar('primary-widget-area')) : ?>
    		
        <?php
    endif; // end primary widget area 
// A second sidebar for widgets, just because.
    if (is_active_sidebar('secondary-widget-area')) :
        dynamic_sidebar('secondary-widget-area');
    endif;
    ?>      
</div>