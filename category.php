<?php
/**
 * The template for displaying Category pages.
 *
 */
get_header();
get_template_part("templates/breadcrumb");
?>  

<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8 col-sm-8">
                    <div class="content-bar">
                        <?php
                        if (have_posts()) :
                            $category_description = category_description();
                            if (!empty($category_description))
                                echo '' . esc_html($category_description) . '';
                            /* Run the loop for the category page to output the posts.
                             * If you want to overload this in a child theme then include a file
                             * called loop-category.php and that will be used instead.
                             */
                        endif; 
                            get_template_part('loop');
                            ?>
                            <div class="clear"></div>
                            <nav id="nav-single"> <span class="nav-previous">
                                    <?php next_posts_link(__('Newer posts &rarr;', 'traffica')); ?>
                                </span> <span class="nav-next">
                                    <?php previous_posts_link(__('&larr; Older posts', 'traffica')); ?>
                                </span> </nav>
                        
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>