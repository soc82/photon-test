<?php
ob_start();

/*
 * * Enqueue Google Fonts
 */

function menu_link_color_styles() {
   
    $header_v_pad = get_option('header_v_pad',200);


    if ($header_v_pad != '') {
        echo '<style type="text/css">.header, .traffica_footertext{ padding:0 ' . esc_attr($header_v_pad) . 'px;}</style>';
    }

}

add_action('wp_head', 'menu_link_color_styles');
