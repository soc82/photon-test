<?php
/*
  Template Name: Contact Page
 */
get_header();
$nameError = '';
$emailError = '';
$commentError = '';
if (isset($_POST['submitted'])) {
    if (trim($_POST['contactName']) === '') {
        $nameError = __('Please enter your name.', 'traffica');
        $hasError = true;
    } else {
        $name = trim($_POST['contactName']);
    }
    if (trim($_POST['email']) === '') {
        $emailError = __('Please enter your email address.', 'traffica');
        $hasError = true;
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $emailError = __('You entered an invalid email address.', 'traffica');
        $hasError = true;
    } else {
        $email = trim($_POST['email']);
    }
    if (trim($_POST['comments']) === '') {
        $commentError = __('Please enter a message.', 'traffica');
        $hasError = true;
    } else {
        if (function_exists('stripslashes')) {
            $comments = stripslashes(trim($_POST['comments']));
        } else {
            $comments = trim($_POST['comments']);
        }
    }
    if (!isset($hasError)) {
        $emailTo = get_option('tz_email');
        if (!isset($emailTo) || ($emailTo == '')) {
            $emailTo = get_option('admin_email');
        }
        $subject = __('[PHP Snippets] From ', 'traffica') . $name;
        $body = __('Name: ', 'traffica') . $name . __('<br/>Email: ', 'traffica') . $email . __('<br/>Comments: ', 'traffica') . $comments;
        $headers = __('From: ', 'traffica') . $name . ' <' . $emailTo . '>' . "\r\n" . __('Reply-To: ', 'traffica') . $email;
        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
    }
}
?>
<div class="page_heading_container">
    <span class="crumb_shadow"></span>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_heading_content row">
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="col-md-6">
                        <?php traffica_breadcrumbs(); ?>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8">
                    <div class="content-bar">
                        <div class="contact-page">
                            <?php
                            if (have_posts()) : the_post();
                            endif;
                            if (isset($emailSent) && $emailSent == true) {
                                ?>
                                <div class="thanks">
                                    <p><?php esc_html_e('Thanks, your email was sent successfully.', 'traffica'); ?></p>
                                </div>
                                <?php
                            } else {
                                ?>
                                <form action="<?php the_permalink(); ?>" id="contactForm" class="contactForm" method="post">  
                                    <h1 class="page-title"><a href="#"><?php the_title(); ?></a></h1>
                                    <?php the_content(); ?>	
                                    <input type="text" name="contactName" id="contactName" value="<?php
                                    if (isset($_POST['contactName']))
                                        echo esc_attr($_POST['contactName']);
                                    ?>" placeholder="<?php echo esc_attr(__('Name', 'traffica')); ?>" class="required requiredField" />
                                           <?php if ($nameError != '') { ?>
                                        <span class="error"> <?php echo $nameError; ?> </span>
                                    <?php } ?>              
                                    <input type="text" name="email" id="email" value="<?php
                                    if (isset($_POST['email']))
                                        echo esc_attr($_POST['email']);
                                    ?>" placeholder="<?php echo esc_attr(__('Email', 'traffica')); ?>" class="required requiredField email" />
                                           <?php if ($emailError != '') { ?>
                                        <span class="error"> <?php echo $emailError; ?> </span>
                                        <br/>
                                    <?php } ?>		               
                                    <textarea name="comments" id="commentsText" rows="20" cols="30" placeholder="<?php echo esc_attr(__('Message', 'traffica')); ?>"  class="required requiredField"><?php
                                        if (isset($_POST['comments'])) {
                                            if (function_exists('stripslashes')) {
                                                echo stripslashes($_POST['comments']);
                                            } else {
                                                echo $_POST['comments'];
                                            }
                                        }
                                        ?></textarea>

                                    <?php if ($commentError != '') { ?>
                                        <span class="error"> <?php echo $commentError; ?> </span>
                                        <br/>
                                    <?php } ?>	
                                    <input class="submit" type="submit" value="<?php echo esc_attr(__('Send Message', 'traffica')); ?>"/>
                                    <input type="hidden" name="submitted" id="submitted" value="<?php esc_html_e('true', 'traffica'); ?>" />
                                </form>
                            <?php } ?>	  
                            <div class="clear"></div>
                            <h1><?php esc_html_e('Location Map', 'traffica'); ?></h1>
                            <div style="width: 100%; overflow:hidden;" class="contact-map"><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo esc_url('https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Chuna+Bhatti,+Bhopal,+Madhya+Pradesh,+India&amp;aq=0&amp;oq=bh&amp;sll=37.0625,-95.677068&amp;sspn=56.506174,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=Chuna+Bhatti,+Bhopal,+Madhya+Pradesh,+India&amp;t=m&amp;ll=23.202617,77.413874&amp;spn=0.037866,0.054932&amp;z=14&amp;iwloc=A&amp;output=embed'); ?>"></iframe><br /></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>