<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <?php if (is_front_page()) { ?>
            <?php if (traffica_get_option('traffica_keyword') != '') { ?>
                <meta name="keywords" content="<?php ?>" />
                <?php
            }
            if (traffica_get_option('traffica_description') != '') {
                ?>
                <meta name="description" content="<?php ?>" />
                <?php
            }
            if (traffica_get_option('traffica_author') != '') {
                ?>
                <meta name="author" content="<?php ?>" />
                <?php
            }
        }
        ?>		
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="header_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="header">
                        <div class="col-md-3 col-sm-3">

                            <div class="logo">
                                <?php if (traffica_is_preview()) { ?>
                                    <a class="traffica_logo" href="<?php echo home_url(); ?>"><img src="<?php echo TRAFFICA_DIR_URI . 'assets/images/traffica_logo.png'; ?>" alt="<?php wp_kses_post(bloginfo('name')); ?>"/></a>
                                <?php } elseif (traffica_get_option('traffica_logo') != '') { ?>
                                    <a class="traffica_logo" href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url(traffica_get_option('traffica_logo')); ?>" alt="<?php wp_kses_post(bloginfo('name')); ?>"/></a>                              
                                <?php } else { ?>
                                    <hgroup>
                                        <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                                        <h5 class="site-description"><?php bloginfo('description'); ?></h5>
                                    </hgroup>
                                <?php } ?>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <div class="clear"></div>
                            <div id="MainNav traffica_nav">                                  
                                <?php if (traffica_get_option('traffica_nav') != '') { ?><a href="#" class="mobile_nav closed"><?php echo stripslashes(traffica_get_option('traffica_nav')); ?><span></span></a>
                                    <?php
                                }
                                traffica_nav();
                                ?> 
                            </div>  
                            
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>