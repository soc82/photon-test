<?php
if (post_password_required()) {
    ?>
    <p class="nocomments"><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'traffica'); ?></p>
    <?php
    return;
}
?>
<!-- You can start editing here. -->
<div id="commentsbox">
    <?php if (have_comments()) : 
        ?>
        <h3 id="comments">
            <?php comments_number(__('No Responses', 'traffica'), __('One Response', 'traffica'), __('% Responses', 'traffica')); ?>
            <?php esc_html_e('so far.', 'traffica'); ?></h3>
        <ol class="commentlist">
            <?php wp_list_comments(array('avatar_size' => 70)); ?>
        </ol>
        <div class="comment-nav">
            <div class="alignleft">
                <?php previous_comments_link() ?>
            </div>
            <div class="alignright">
                <?php next_comments_link() ?>
            </div>
        </div>
        <?php
    else : // this is displayed if there are no comments so far 
        if (comments_open()) :
//             If comments are open, but there are no comments. 
        else : // comments are closed  
            ?>
            <!--			 If comments are closed. -->
            <p class="nocomments"><?php esc_html_e('Comments are closed.', 'traffica'); ?></p>
        <?php
        endif;
    endif;
    if (comments_open()) :
        ?>
        <div class="commentform_wrapper">
            <div class="post-info"><?php esc_html_e('Leave a Comment', 'traffica'); ?></div>
            <div id="comment-form">
                <div id="respond" class="rounded">
                    <div class="cancel-comment-reply">
                        <small><?php cancel_comment_reply_link(); ?></small> 
                    </div>
                    <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
                        <p><?php esc_html_e('You must be', 'traffica'); ?> <a href="<?php echo esc_url(wp_login_url(get_permalink())); ?>"><?php esc_html_e('logged in', 'traffica'); ?></a><?php esc_html_e('to post a comment.', 'traffica'); ?></p>
                    <?php else : ?>
                        <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

                            <?php if ($user_ID) : ?>
                                <p class="comment_message" style="margin-bottom: 10px;"><?php esc_html_e('LOGIN', 'traffica'); ?> <a href="<?php echo esc_url(get_option('siteurl') . '/wp-admin/profile.php'); ?>"><?php echo $user_identity; ?></a>. <a href="<?php echo esc_url(wp_logout_url(get_permalink())); ?>" title="<?php esc_html__('Log out of this account', 'traffica'); ?>"><?php esc_attr_e('Logout', 'traffica'); ?></a></p>
                                <p class="clearfix">
                                    <textarea name="comment" id="comment" cols="50" rows="7" tabindex="1" placeholder="<?php echo__('Your Comment', 'traffica'); ?>"></textarea>
                                </p>
                            <?php else : ?>
                                <p class="clearfix">					
                                    <input type="text" name="author" id="author" tabindex="2" value="" placeholder="<?php echo esc_html__('Your Name', 'traffica'); ?>"/>
                                </p>
                                <p class="clearfix">					
                                    <input type="text" name="email" id="email" tabindex="3" value="" placeholder="<?php echo esc_html__('Your Email', 'traffica'); ?>"/>
                                </p>
                                <p class="clearfix">					
                                    <input type="text" name="url" id="url" tabindex="4" value="" placeholder="<?php echo esc_html__('Your Website', 'traffica'); ?>"/>
                                </p>
                                <p class="clearfix">
                                    <textarea name="comment" id="comment" cols="50" tabindex="5" rows="7"  placeholder="<?php esc_html__('Leave Your Comment Here', 'traffica'); ?>"></textarea>
                                </p>
                            <?php endif; ?>
                            <div class="submit">
                                <input name="submit" type="submit" id="submit" tabindex="6" value="<?php echo esc_html__('Sumbit Comment', 'traffica'); ?>" />
                                <p id="cancel-comment-reply">
                                    <?php cancel_comment_reply_link() ?>
                                </p>
                            </div>
                            <div>
                                <?php
                                comment_id_fields();
                                do_action('comment_form', $post->ID);
                                ?>
                            </div>
                        </form>
                    <?php endif; // If registration required and not logged in      ?>
                </div>
            </div>
        </div>
    <?php endif; // if you delete this the sky will fall on your head      ?>
</div>
