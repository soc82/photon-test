<ul class="slides">			
    <?php
    $i = '';
    $value_img = array('.jpg', '.png', '.jpeg', '.gif', '.bmp', '.tiff', '.tif');
    $arr_img = array( '<img src="' . get_template_directory_uri() . '/assets/images/Blog-01.jpg" alt="demo-img1"/>','<img src="' . get_template_directory_uri() . '/assets/images/Blog-02.jpg" alt="demo-img2"/>');
    for ($i = 1; $i <= 2; $i++) {
        if ($i == 1) {
            ?>
            <li>
                <?php
                //The strpos funtion is comparing the strings to allow uploading of the Videos & Images in the Slider
                $mystring = traffica_get_option('traffica_image' . $i);
                $check_img_ofset = 0;
                foreach ($value_img as $get_value) {
                    if (preg_match("/$get_value/", $mystring)) {
                        $check_img_ofset = 1;
                    }
                }
                // Note our use of ===.  Simply == would not work as expected
                // because the position of 'a' was the 0th (first) character.
                ?>
                <div class="imgmedia"> <?php if (traffica_get_option('traffica_image' . $i) != '') { ?>
                        <?php if ($check_img_ofset == 0) { ?>
                            <div class="video <?php echo traffica_image. $i?>"><?php echo traffica_get_option('traffica_image' . $i); ?></div>
                        <?php } else { ?>
                            <div class="imgpadding <?php echo traffica_image. $i?>"><img src="<?php echo traffica_get_option('traffica_image' . $i); ?>"  alt="" /><span class="slider-shadow"></span></div>
                            <?php
                        }
                    } elseif(traffica_is_preview()) {
                        ?>
                        <div class="imgpadding <?php echo traffica_image. $i?>"><?php echo $arr_img[$i]; ?><span class="slider-shadow"></span></div>
                    <?php } else{ ?>
                         <div class="imgpadding <?php echo traffica_image. $i?>"><img src="<?php echo esc_url(TRAFFICA_DIR_URI . 'assets/images/normal_slider_img.jpg'); ?>" alt="" /><span class="slider-shadow"></span></div>
                         <?php } ?>
                </div>
                <div class = "content <?php echo traffica_slider_heading. $i?>">
                    <?php if (traffica_get_option('traffica_slider_heading' . $i) != '') {
                        ?>
                        <h1>
                            <a href="<?php echo esc_url(traffica_get_option('traffica_slider_btnlink' . $i)); ?>">
                                <?php echo stripslashes(traffica_get_option('traffica_slider_heading' . $i)); ?>
                            </a>
                        </h1>
                    <?php } else {
                        ?>
                    <h1 class="<?php echo traffica_slider_heading. $i?>">
                            <a href="#"><?php _e('Time Has Proven That Yoga Works', 'traffica'); ?>
                            </a>
                        </h1>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php if (traffica_get_option('traffica_slider_des' . $i) != '') { ?>
                    <p class="<?php echo traffica_slider_des. $i?>"><?php echo stripslashes(traffica_get_option('traffica_slider_des' . $i)); ?></p>
                    <?php } else { ?>
                    <p class="<?php echo traffica_slider_des. $i?>"><?php _e('Yoga views the human system as mulcw-dimensional including the physical body, the breathing body, the personality, the intellect and emotions. All dimensions are strongly interconnected.', 'traffica'); ?></p>
                    <?php } ?>
                    <a class="slider-btn2 <?php echo traffica_slider_btnt. $i?>" href="<?php
                    if (traffica_get_option('traffica_slider_btnlink' . $i) != '') {
                        echo esc_url(stripslashes(traffica_get_option('traffica_slider_btnlink' . $i)));
                    } else {
                        echo "#";
                    }
                    ?>"><?php
                           if (traffica_get_option('traffica_slider_btnt' . $i) != '') {
                               echo traffica_get_option('traffica_slider_btnt' . $i);
                           } else {
                               _e('Get More Yoga Tips !', 'traffica');
                           }
                           ?></a>
                </div>
            </li>
            <?php
        } else {
            if (traffica_get_option('traffica_image' . $i) != '') {
                ?>
                <li>
                    <?php
                    //The strpos funtion is comparing the strings to allow uploading of the Videos & Images in the Slider
                    $mystring = traffica_get_option('traffica_image' . $i);
                    $check_img_ofset = 0;
                    foreach ($value_img as $get_value) {
                        if (preg_match("/$get_value/", $mystring)) {
                            $check_img_ofset = 1;
                        }
                    }
                    // Note our use of ===.  Simply == would not work as expected
                    // because the position of 'a' was the 0th (first) character.
                    ?>
                    <div class="imgmedia"> <?php if (traffica_get_option('traffica_image' . $i) != '') { ?>
                            <?php if ($check_img_ofset == 0) { ?>
                                <div class="video <?php echo traffica_image. $i?>"><?php echo traffica_get_option('traffica_image' . $i); ?></div>
                            <?php } else { ?>
                                <div class="imgpadding <?php echo traffica_image. $i?>"><img src="<?php echo esc_url(traffica_get_option('traffica_image' . $i)); ?>"  alt="" /><span class="slider-shadow"></span></div>
                                <?php
                            }
                       } elseif(traffica_is_preview()) {
                        ?>
                        <div class="imgpadding <?php echo traffica_image. $i?>"><?php echo $arr_img[$i]; ?><span class="slider-shadow"></span></div>
                    <?php } else{ ?>
                         <div class="imgpadding <?php echo traffica_image. $i?>"><img src="<?php echo esc_url(TRAFFICA_DIR_URI . 'assets/images/normal_slider_img.jpg'); ?>" alt="" /><span class="slider-shadow"></span></div>
                         <?php } ?>
                    </div>
                    <div class = "content <?php echo traffica_slider_heading. $i?>">
                        <?php if (traffica_get_option('traffica_slider_heading' . $i) != '') {
                            ?>
                            <h1>
                                <a href="<?php echo esc_url(traffica_get_option('traffica_slider_btnlink' . $i)); ?>">
                                    <?php echo stripslashes(traffica_get_option('traffica_slider_heading' . $i)); ?>
                                </a>
                            </h1>
                        <?php } else {
                            ?>
                        <h1 class="<?php echo traffica_slider_heading. $i?>">
                                <a href="#"><?php _e('Breathe Easy And Stress Less', 'traffica'); ?>
                                </a>
                            </h1>
                        <?php } ?>
                        <div class="clear"></div>
                        <?php if (traffica_get_option('traffica_slider_des' . $i) != '') { ?>
                        <p class="<?php echo traffica_slider_des . $i?>"><?php echo stripslashes(traffica_get_option('traffica_slider_des' . $i)); ?></p>
                        <?php } else { ?>
                            <p class="<?php echo traffica_slider_des . $i?>"><?php _e('Yoga is not a one-size-fits-all practice.Yoga views the human system the personality, the intellect and emotions. Yoga gives an individual relief from countless ailments at the physical level.', 'traffica'); ?></p>
                        <?php } ?>
                        <a class="slider-btn2 <?php echo traffica_slider_btnt . $i?>" href="<?php
                           if (traffica_get_option('traffica_slider_btnlink' . $i) != '') {
                               echo esc_url(stripslashes(traffica_get_option('traffica_slider_btnlink' . $i)));
                           } else {
                               echo "#";
                           }
                           ?>"><?php
                               if (traffica_get_option('traffica_slider_btnt' . $i) != '') {
                                   echo traffica_get_option('traffica_slider_btnt' . $i);
                               } else {
                                   _e('Get More Yoga Tips !', 'traffica');
                               }
                               ?></a>
                    </div>
                </li>
                <?php
            }
        }
    }
    ?>
</ul>