<div class="layered_slider">
    <div class="slider_container">
        
        <div id="myCarousel" class="carousel slide " data-ride="carousel" data-interval="<?php echo traffica_get_option( 'traffica_slide_speed' ) != '' ? traffica_get_option( 'traffica_slide_speed' ) : 300; ?>">
            <div class="carousel-inner">
                <?php
                $j = 1;
                for ( $i = 1; $i <= 6; $i++ ) {
                    if ( traffica_get_option( 'traffica_image' . $i ) != '' ) {
                          if ($i != 1) {
                           $j++;
                       } else {
                           $j = 7;
                       }
                        ?>
                            <?php if ( traffica_get_option( 'traffica_image' . $i ) != '' ) { ?>
                                <img src="<?php echo esc_url( traffica_get_option( 'traffica_image' . $i ) ); ?>" class="img-responsive" alt="<?php echo esc_attr( traffica_get_option( 'traffica_slider_heading' . $i ) ); ?>">
                            <?php } else { ?>
                                <img src="<?php echo esc_url( $ImageUrl1 ); ?>" class="img-responsive" alt="First slide">
                            <?php } ?>
                            <div class="container">
                                <div class="carousel-caption">
                                    <div class="carousel-text">

                                        <?php if ( traffica_get_option( 'traffica_slider_heading' . $i ) != '' ) {
                                            ?>
                                            <h1 class="animated <?php echo traffica_slider_heading . $i ?> <?php echo $j % 2 ? 'bounceInRight' : 'bounceInLeft'; ?>"><?php echo esc_attr( traffica_get_option( 'traffica_slider_heading' . $i ) ); ?></h1>
                                        <?php } else { ?>
                                            <h1 class="animated <?php echo traffica_slider_heading . $i ?> <?php echo $j % 2 ? 'bounceInRight' : 'bounceInLeft'; ?>"><?php esc_html_e( 'Premium WordPress Themes with Single Click Installation', 'traffica' ); ?></h1>
                                            <?php
                                        }

                                        if ( traffica_get_option( 'traffica_slider_des' . $i ) == '' ) {
                                            ?>
                                            <ul class="list-unstyled carousel-list <?php echo traffica_slider_des . $i?>">
                                                <li class="animated <?php echo $j % 2 ? 'bounceInLeft' : 'bounceInRight'; ?>"><?php esc_html_e( 'Lid est laborum dolo rumes fugats untras. Et harums ser quidem rerum facilis dolores nemis omnis fugiats vitaes nemo.', 'traffica' ); ?></li>
                                            </ul>
                                        <?php } else { ?>
                                            <ul class="list-unstyled carousel-list <?php echo traffica_slider_des . $i?>">
                                                <li class="animated <?php echo $j % 2 ? 'bounceInLeft' : 'bounceInRight'; ?>"><?php echo esc_attr( traffica_get_option( 'traffica_slider_des' . $i ) ); ?></li>
                                            </ul>

                                            <?php
                                        }
                                        ?>
                                        <a class="enigma_blog_read_btn animated bounceInUp slider-btn2" href="<?php
                                        if ( traffica_get_option( 'traffica_slider_btnlink' . $i ) != '' ) {
                                            echo esc_url( traffica_get_option( 'traffica_slider_btnlink' . $i ) );
                                        } else {
                                            echo "#";
                                        }
                                        ?>" role="button"><?php
                                               if ( traffica_get_option( 'traffica_slider_btnt' . $i ) != '' ) {
                                                   echo traffica_get_option( 'traffica_slider_btnt' . $i );
                                               } else {
                                                   esc_html_e( 'Get Started !', 'traffica' );
                                               }
                                               ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                if ( $j == 1 ) {
                    ?>
                    <div class = "item active">
                        <img src="<?php echo esc_url( TRAFFICA_DIR_URI . 'assets/images/layered_slider_img.jpg' ); ?>" class="img-responsive" alt="First slide">
                        <div class="container">
                            <div class="carousel-caption">
                                <div class="carousel-text">
                                    <h1 class="animated bounceInRight traffica_slider_heading1"><?php esc_html_e( 'Premium WordPress Themes with Single Click Installation', 'traffica' ); ?></h1>
                                    <ul class="list-unstyled carousel-list traffica_slider_des1">
                                        <li class="animated bounceInLeft "><?php esc_html_e( 'Lid est laborum dolo rumes fugats untras. Et harums ser quidem rerum facilis dolores nemis omnis fugiats vitaes nemo.', 'traffica' ); ?></li>
                                    </ul>
                                    <a class="enigma_blog_read_btn animated bounceInUp slider-btn2 traffica_slider_btnt1" href="#" role="button">
                                        <?php esc_html_e( 'Get Started !', 'traffica' ); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
            </div>
            <?php if ($j > 1 && $j != 7) { ?>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <?php } ?>
        </div><!-- /.carousel -->
    </div>
</div>
