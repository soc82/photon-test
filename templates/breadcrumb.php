<div class="page_heading_container">
    <span class="crumb_shadow"></span>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (is_404()) { ?>
                    <div class="page_heading_content">
                        <span class="bred-tip"></span>
                        <?php esc_html_e('This is somewhat embarrassing, isn&rsquo;t it?', 'traffica'); ?>
                    </div>
                <?php } else if (is_single()) { ?>
                    <div class="page_heading_content single">
                        <?php traffica_breadcrumbs(); ?>
                    </div>
                <?php } else if (is_attachment()) { ?>
                    <div class="page_heading_content single">
                        <h1><?php traffica_breadcrumbs(); ?></h1>
                    </div>
                <?php } else {
                    ?>
                    <div class="page_heading_content row">
                        <?php
                        switch (true) {
                            case(is_author()):
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <?php if (have_posts()) : the_post(); ?>
                                        <h1><?php printf(esc_attr__('Author Archives: %s', 'traffica'), "<span class='vcard'><a class='url fn n' href='" . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . "' title='" . esc_attr(get_the_author()) . "' rel='me'>" . get_the_author() . "</a></span>"); ?></h1>
                                    </div>
                                    <?php
                                endif;
                                break;
                            case(is_category()):
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <h1><?php printf(esc_attr__('Category Archives: %s', 'traffica'), '' . single_cat_title('', false) . ''); ?></h1>
                                </div>
                                <?php
                                break;
                            case(is_tag()):
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <h1><?php printf(esc_attr__('Tag Archives: %s', 'traffica'), '' . single_cat_title('', false) . ''); ?></h1>
                                </div>
                                <?php
                                break;
                            case(is_archive()):
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <h1 class="page_title single-heading">
                                        <?php
                                        if (is_day()) :
                                            printf(esc_attr__('Daily Archives: %s', 'traffica'), get_the_date());
                                        elseif (is_month()) :
                                            printf(esc_attr__('Monthly Archives: %s', 'traffica'), get_the_date('F Y'));
                                        elseif (is_year()) :
                                            printf(esc_attr__('Yearly Archives: %s', 'traffica'), get_the_date('Y'));
                                        else :
                                            esc_html_e('Blog Archives', 'traffica');
                                        endif;
                                        ?>
                                    </h1> 
                                </div>
                                <?php
                                break;
                            case(is_search()):
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <h1><?php printf(esc_attr__('Search Results for: %s', 'traffica'), '' . get_search_query() . ''); ?></h1>
                                </div>
                                <?php
                                break;
                            default:
                                ?>
                                <div class="col-md-6 col-sm-6">
                                    <h1><?php the_title(); ?></h1>
                                </div> <?php
                        }
                        ?>
                        <div class = "col-md-6 col-sm-6">
                            <?php traffica_breadcrumbs();
                            ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php }
                ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>