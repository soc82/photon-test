<div class="Portfolio traffica_home_post_content">  
    <div class="head-sep">
        <div class="hsep"></div>
        <?php if (traffica_get_option('traffica_blog_heading') != '') {
            ?>
            <h2 class="traffica_blog_heading"><span class="traff_blog_head"><?php echo stripslashes(traffica_get_option('traffica_blog_heading')); ?></span></h2>
        <?php } else { ?>
            <h2 class="traffica_blog_heading"><span class="traff_blog_head"><?php esc_html_e('Our Blog', 'traffica'); ?></span></h2>
            <?php
        }
        echo '</div>';
        $count = 1;
        $class = '';
        $check = false;
        $num_of_posts = 80;
        $args = array(
            'post_status' => 'publish',
            'posts_per_page' => $num_of_posts,
            'order' => 'DESC'
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
            ?>          
            <ul id="mycarousel" class="jcarousel-skin-tango">
                <?php
                while ($query->have_posts()) :$query->the_post();
                    global $post;
                    $content = $post->post_content;
                    $searchimages = '~<img [^>]* />~';
                    /* Run preg_match_all to grab all the images and save the results in $pics */
                    preg_match_all($searchimages, $content, $pics);
                    // Check to see if we have at least 1 image
                    $iNumberOfPics = count($pics[0]);
//                    if (($iNumberOfPics > 0) || (has_post_thumbnail())) {
                    ?>
                    <li class="list">
                        <div class="slider-item">
                            <?php
                            if (traffica_is_preview()) {
                                the_post_thumbnail(array(260, 200));
                            } elseif (($iNumberOfPics > 0) || (has_post_thumbnail())) {
                                traffica_get_thumbnail(260, 200);
                            } else {
                                traffica_get_image(260, 200);
                            }
                            ?>
                        </div>
                        <section>
                            <div class="carousel-post-title"><h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(esc_attr__('Permanent Link to %1$s', 'traffica'), get_the_title()); ?>"><?php the_title(); ?></a></h3></div>
                            <div class="carousel-post-desc"><p><?php the_excerpt() ?></p></div>
                            <div class="carousel-post-readmore"><a href="<?php the_permalink() ?>">Read More</a></div>
                        </section>

                    </li>
                    <?php
                endwhile;
                ?>
            </ul>
            <?php
        endif;
        wp_reset_query();
        ?>

        <!-- /End Carousel -->   
    </div>
</div>
