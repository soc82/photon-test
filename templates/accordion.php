<?php
for ($i = 1; $i <= 3; $i++) {
   
        ?>
        <div class="akordeon-item">
            <span class="ac_tip"></span>
            <div class="akordeon-item-head <?php $i == 3 ? 'collapsed' : ''; ?>">
                <div class="akordeon-item-head-container ">
                    <?php if (traffica_get_option('traffica_acc_head' . $i) != '') { ?>
                        <h5 class="akordeon-heading <?php echo traffica_acc_head . $i ?>"><?php echo stripslashes(traffica_get_option('traffica_acc_head' . $i)); ?></h5>
                    <?php } else { ?>
                        <h5 class="akordeon-heading <?php echo traffica_acc_head . $i ?>"><?php esc_html_e('Order to secure your website.', 'traffica'); ?></h5>
                    <?php } ?>
                </div>
            </div>
            <div class="akordeon-item-body">
                <div class="akordeon-item-content">
                    <?php
                    if (traffica_get_option('traffica_acc_desc' . $i) != '') {
                        echo stripslashes(traffica_get_option('traffica_acc_desc' . $i));
                    } else {
                        esc_html_e('Many of you have a WordPress administrator username set as Admin because when you install for the first time on your machine you stuck with this default username.', 'traffica');
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }

$x++;
?>

