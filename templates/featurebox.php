<?php
$feature_heading = array('traffica_firsthead', 'traffica_headline2', 'traffica_headline3', 'traffica_headline4');
$feature_desc = array('traffica_firstdesc', 'traffica_seconddesc', 'traffica_thirddesc', 'traffica_fourthdesc');
$feature_demo_head = array('Meditation Is A Part Of Yoga','Personal Workout Programs','Best Yoga Training Centre','Weight-loss basics Strategy');
$feature_demo_desc = array('Yoga has been used to help heal victims of torture or other trauma to recover fast','Regular physical activity should be part of any weight-loss plan. To lose weight.','Best Yoga teachers training center Best Combination of yoga & Ayurveda Methods.','Weight loss Help make your weight-loss goals a reality with these six strategies.');
$feature_demo_back = array('You will be surprised to know that the ancient practice of yoga can alleviate a multitude of your health woes!','If you thought yoga is just for those interested in slow and boring movements you are quite wrong.','Yoga gives you all that a gym can but in a peaceful, safe and more holistic way.','Yoga helps the body gently regain its flexibility by stretching out the muscles of various areas.');
$i = 1;
foreach ($feature_heading as $fhead) {?>
   
    <div class = "col-md-3 col-sm-6">
        <div class="flip-container" ontouchstart="this.classList.toggle('hover');" >
            <div class = "feature_inner_box">
                <div class="flipper <?php echo $fhead ?>">
                <div class="front">
                        <?php if (traffica_get_option('traffica_fimg' . $i) != '') {
                            ?>
                    <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>"><img src="<?php echo esc_url(traffica_get_option('traffica_fimg' . $i)); ?>" alt="<?php __('Feature image', 'traffica'); ?>" /></a>
                        <?php } else if (traffica_get_option('traffica_ficon' . $i) != '') { ?>
                    <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>">
                                <i class="<?php echo traffica_get_option('traffica_ficon' . $i); ?>"></i> 
                            </a>
                        <?php } else {
                            ?>
                            <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>"><i class="fa fa-television"></i></a>
                            <?php
                        }
                        if (traffica_get_option($fhead) != '') {
                            ?>
                            <h6 class="feature_title"><a href="<?php
                                $flink = 'traffica_feature_link' . $i;
                                if (traffica_get_option($flink) != '') {
                                    echo esc_url(traffica_get_option('traffica_feature_link' . $i));
                                }
                                ?>"><?php echo stripslashes(traffica_get_option($fhead)); ?></a></h6>
                            <?php } else { ?>
                            <h6><a href="#"><?php echo($feature_demo_head[$i-1]); ?></a></h6>
                            <?php
                        }
                        if (traffica_get_option($feature_desc[$i - 1]) != '') {
                            ?>
                            <p><?php echo stripslashes(traffica_get_option($feature_desc[$i - 1])); ?></p>
                        <?php } else { ?>
                            <p><?php echo($feature_demo_desc[$i-1]); ?></p>
                        <?php } ?>
                    </div>
                    <div class="back">
                        <?php if (traffica_get_option('traffica_fimg' . $i) != '') {
                            ?>
                            <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>"><img src="<?php echo esc_url(traffica_get_option('traffica_fimg' . $i)); ?>" alt="<?php __('Feature image', 'traffica'); ?>" /></a>
                        <?php } else if (traffica_get_option('traffica_ficon' . $i) != '') { ?>
                           
                            <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>">
                                <i class="<?php echo traffica_get_option('traffica_ficon' . $i); ?>"></i> 
                            </a>
                        <?php } else {
                            ?>
                            <a href="<?php echo esc_url(traffica_get_option('traffica_feature_link' . $i)); ?>"><i class="fa fa-television"></i></a>
                            <?php
                        }
                        if (traffica_get_option('traffica_feature_back' . $i) != '') {
                            echo '<p>' . traffica_get_option('traffica_feature_back' . $i) . '</p>';
                        } else {
                            ?>
                            <p><?php echo($feature_demo_back[$i-1]); ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <?php
    $i++;
}


?>