<!-- Start the Loop. -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<!--Start post-->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post_heading_wrapper">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="post_date">
                    <ul class="date">
                        <li class="day"><?php echo esc_html(get_the_time('d'))?></li>
                        <li class="month"><?php echo esc_html(get_the_time('M')) ?></li>
                        <li class="year"><?php echo esc_html(get_the_time('Y')) ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h1 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(esc_attr__('Permanent Link to %s', 'traffica'), the_title_attribute('echo=0')); ?>"><?php the_title(); ?></a></h1>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <ul class="post_meta">
        <li class="posted_by"><span><?php esc_html_e('Posted by', 'traffica'); ?></span><?php the_author_posts_link(); ?></li>
        <li class="post_category"><span><?php esc_html_e('in', 'traffica'); ?></span><?php the_category(', '); ?></li>
        <?php if (get_comments_number($post->ID) > 0 && !post_password_required()) {
            ?>
            <li class="post_comment"><span></span><?php comments_popup_link(__('No Comments.', 'traffica'), __('1 Comment.', 'traffica'), __('% Comments.', 'traffica')); ?></li>
        <?php } ?>
    </ul>
    <div class="post_content"><?php
    if(traffica_is_preview()){
        the_post_thumbnail(array(700,400));   
   } elseif ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>
  <?php 
       the_post_thumbnail('post_thumbnail', array('class' => 'postimg'));
   } else {
       traffica_get_image(700, 400);
   }
   the_excerpt();
        ?>
        <a class="read_more" href="<?php the_permalink() ?>"><?php esc_html_e('Continue Reading...', 'traffica'); ?></a>
	</div>
    <div class="clear"></div>
</div>
<!--End post-->
<div class="clear"></div>
   <!--End post-->        
    <?php endwhile; ?>
   
    <?php
else:
    ?>
    <div class="post">
        <p>
    <?php echo esc_html(SORRY_NO_POST_MATCHED); ?>
        </p>
    </div>
<?php endif;
?>
<div class="clear"></div>
<!--End post-->