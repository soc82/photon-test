<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header();

if (traffica_get_option('slider_select') == 'normal') {
    ?>
    <div class="slider_container">  
        <input type="hidden" id="txt_slidespeed" value="3000"/>  
        <span class="slidertop-shadow"></span>
        <div class="container">
            <div class="row">
                <div class="col-md-12">		
                    <div class="flexslider">
                        <?php get_template_part('templates/slider') ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div> 
    </div>
    <?php
} else {?>
 <div class="slider_container">  
        <input type="hidden" id="txt_slidespeed" value="3000"/>  
        <span class="slidertop-shadow"></span>
        <div class="container">
            <div class="row">
                <div class="col-md-12">		
                    <div class="flexslider">
                        <?php get_template_part('templates/slider') ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div> 
    </div>
<?php } ?>

<div class="home_container container">
    <div class="row">
        <div class="col-md-12">
            <div class="home-content">
                <div class="page_content">
                    <div class="page_info">
                        <?php if (traffica_get_option('traffica_page_main_heading') != '') { ?>
                            <h1 class="traffica_page_main_heading"><?php echo stripslashes(traffica_get_option('traffica_page_main_heading')); ?></h1>
                        <?php } else { ?>
                            <h1 class="traffica_page_main_heading"><?php esc_html_e('What Research Says About Yoga- The', 'traffica'); ?> <a href="#"><?php esc_html_e('Power', 'traffica'); ?></a> <?php esc_html_e('of Yoga Is Endless', 'traffica'); ?></h1>
                            <?php
                        }
                        if (traffica_get_option('traffica_page_sub_heading') != '') {
                            ?>
                            <p class="traffica_page_sub_heading"><?php echo stripslashes(traffica_get_option('traffica_page_sub_heading')); ?></p>
                        <?php } else { ?>
                            <p class="traffica_page_sub_heading"><?php esc_html_e('Yoga helps improve physical fitness, stress, general well being, controls mental clarity and greater self-understanding.', 'traffica'); ?> </p>
                        <?php } ?>
                    </div>
                    <div class="clear"></div>
                    <div class="feature_box row">
                        <?php get_template_part('templates/featurebox'); ?>
                    </div>
                    <div class="clear"></div>
                    <?php
                    $home_post_content = traffica_get_option('traffica_home_post_content');
                    $home_post_content_on = "on";
                    if ($home_post_content == $home_post_content_on || $home_post_content != 'off') {
                        ?>
                        <!--carousel slider	-->

                        <?php
                        get_template_part('templates/carousel');
                    }
                    ?>  


                    <div class="clear"></div>

                 

                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>