<?php
/**
 * The template for displaying attachments.
 *
 * @package WordPress
 */
get_header();
get_template_part("templates/breadcrumb");
?>
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="fullwidth">
                <p></p>
                <p>
                    <a href="<?php echo esc_url(get_permalink($post->post_parent)); ?>" title="<?php esc_attr(printf(esc_attr__('Return to %s', 'traffica'), get_the_title($post->post_parent))); ?>" rel="gallery">
                        <?php
                        /* translators: %s - title of parent post */
                        printf(/* translators: %s - title of parent post */__('<span>&larr;</span> %s', 'traffica'), get_the_title($post->post_parent));
                        ?>
                    </a>
                </p><p id="attachment-heading">
                    <?php
                    printf(/* translators: %s - Heading of parent post */ esc_attr__('By %2$s', 'traffica'), 'meta-prep meta-prep-author', sprintf('<a class="url fn n" href="%1$s" title="%2$s">%3$s</a>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), sprintf(esc_attr__('View All Post By', 'traffica'), get_the_author()), get_the_author()
                    ));
                    ?>
                    <span>|</span>
                    <?php
                    printf(/* translators: %s - Time of parent post */ esc_attr__('Published %2$s', 'traffica'), 'meta-prep meta-prep-entry-date', sprintf( '<abbr title="%1$s">%2$s</abbr>', esc_attr(get_the_time()), get_the_date()
                    ));
                    if (wp_attachment_is_image()) {
                        echo ' | ';
                        $metadata = wp_get_attachment_metadata();
                        printf(esc_attr__('Full size is %s pixels', 'traffica'), sprintf('<a href="%1$s" title="%2$s">%3$s &times; %4$s</a>', esc_url(wp_get_attachment_url()), esc_attr(LINK_TO_FULL_SIZE_IMAGE), $metadata['width'], $metadata['height']));
                    }
                    edit_post_link(__('Edit', 'traffica'), '', '');
                    ?>
                </p>
                <!-- .entry-meta --> 
                <?php
                if (wp_attachment_is_image()) {
                    $attachments = array_values(get_children(array('post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID')));
                    foreach ($attachments as $k => $attachment) {
                        if ($attachment->ID == $post->ID) {
                            break;
                        }
                    }
                    $k++;
                    // If there is more than 1 image attachment in a gallery
                    if (count($attachments) > 1) {
                        if (isset($attachments[$k])) { // get the URL of the next image attachment
                            $next_attachment_url = get_attachment_link($attachments[$k]->ID);
                        } else { // or get the URL of the first image attachment
                            $next_attachment_url = get_attachment_link($attachments[0]->ID);
                        }
                    } else {
                        // or, if there's only 1 image attachment, get the URL of the image
                        $next_attachment_url = wp_get_attachment_url();
                    }
                    ?>
                    <p><a href="<?php echo esc_url($next_attachment_url); ?>" title="<?php echo esc_attr(get_the_title()); ?>" rel="attachment">
                            <?php
                            $attachment_size = apply_filters('twentyten_attachment_size', 900);
                            echo wp_get_attachment_image($post->ID, array($attachment_size, 9999)); // filterable image width with, essentially, no limit for image height.
                            ?>
                        </a></p>
                    <nav id="nav-single">
                        <span class="nav-previous">
                            <?php previous_image_link(false, '<div class="previous-image">' . __('Previous Image', 'traffica') . '</div>'); ?></span>
                        <span class="nav-next">
                            <?php next_image_link(false, '<div class="next-image">' . __('Next Image', 'traffica') . '</div>'); ?></span>
                    </nav><!-- #nav-single -->
                <?php } else {
                    ?>
                    <a href="<?php echo esc_url(wp_get_attachment_url()); ?>" title="<?php echo esc_attr(get_the_title()); ?>" rel="attachment"><?php echo esc_url(basename(get_permalink())); ?></a>
                    <?php
                }
                if (!empty($post->post_excerpt)) {
                    echo "<div id='attachment-caption'>";
                    the_excerpt();
                    echo "</div>";
                }
                the_content(__('Continue reading &rarr;', 'traffica'));
                wp_link_pages(array('before' => '' . __('Pages:', 'traffica'), 'after' => ''));
                traffica_posted_in();
                edit_post_link(__('EDIT', 'traffica'), ' ', '');
                comments_template();
                ?>
            </div> 
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>