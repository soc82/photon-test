<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
get_header();
?>  
<!--Start Page Heading -->
<div class="page-heading-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-heading">
                    <h1 class="page-title"><?php the_title(); ?></h1>
                </div> 
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--End Page Heading -->
<!--Start Page Content -->
<div class="page-content-container">
    <div class="container">
        <div class="row">
            <div class="page-content">
                <div class="col-md-8">
                    <div class="content-bar">  
                        <?php
                        if (have_posts()) :
                            woocommerce_content();
                        endif;
                        ?>
                    </div>
                </div>    
                <div class="col-md-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</div>
<?php
get_footer();
