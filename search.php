<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * 
 */
get_header();
get_template_part("templates/breadcrumb");
?>  
<div class="page-container container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content row">
                <div class="col-md-8">
                    <div class="content-bar">
                        <?php if (have_posts()) : ?>               
                            <!--Start Post-->
                            <?php get_template_part('loop'); ?>
                            <!--End Post-->
                        <?php else : ?>
                            <article id="post-0" class="post no-results not-found">
                                <header class="entry-header">
                                    <h1 class="entry-title">
                                        <?php esc_html_e('Nothing Found here', 'traffica'); ?>
                                    </h1>
                                </header>
                                <!-- .entry-header -->
                                <div class="entry-content">
                                    <p>
                                        <?php esc_html_e('Sorry, but nothing matched your search criteria. Please try again with some diffrent keyword', 'traffica'); ?>
                                    </p>
                                    <?php get_search_form(); ?>
                                </div>
                                <!-- .entry-content -->
                            </article>
                        <?php endif; ?>
                        <div class="clear"></div>
                        <nav id="nav-single"> <span class="nav-previous">
                                <?php next_posts_link(__('Newer posts &rarr', 'traffica')); ?>
                            </span> <span class="nav-next">
                                <?php previous_posts_link(__('&larr; Older posts', 'traffica')); ?>
                            </span> </nav>
                    </div>
                </div>
                <div class="col-md-4">
                    <!--Start Sidebar-->
                    <?php get_sidebar(); ?>
                    <!--End Sidebar-->
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>